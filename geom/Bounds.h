#ifndef BOUNDS_H
#define BOUNDS_H
#include "NVX.h"
using namespace nvx;

template<typename T>
class Bounds {
public:
    typedef T Type;
    typedef typename ::nvx::base_value<T>::type Value;
    enum Types {
        Null = 0,
        Radius = 1,
        Square = 2
    };
    typedef Bounds<T> Self;

    inline Bounds() : type(0), prefType(Square) {

    }
    Bounds(const Self& orig) : type(orig.type), prefType(orig.prefType), center(orig.center), axes(orig.axes) {

    }
    virtual ~Bounds() {

    }

    unsigned char type, prefType;
    T center;
    T axes[2];

    inline virtual void setType(int t) {
        prefType = t;
        this->zero();
    }

    inline virtual void zero() {
        type = 0;
    }

    inline virtual bool isEmpty() const {
        return type == 0 || this->nearPoint() == this->farPoint();//axes[0].x == 0;
    }

    inline virtual Value area() const {
        switch(type) {
            default:
            case 0: return 0.f; break;
            case 1: {
                return Value(3.14159) * axes[0].x*axes[0].x;
            } break;
            case 2: {
                //V3 s = V3(center);
                //V3 e = V3(center + axes[0]);

                //return length((e-s)*V3_UP) + length((e-s)*V3_RIGHT) + length((e-s)*V3_FORWARD);
                return Value(1.f/2.f) * length2(axes[0]);
            } break;
        }
    }

    inline virtual void add(const T& pt) {
        switch(prefType) {
            default:
            case 0: break;
            case 1: {
                if(type == Null) {
                    center = pt;
                    axes[0] = Value(0.f);
                    axes[1].x = 1.f;
                }

                const T newCenter = center + (pt - static_cast<const T&>(center))/(axes[1].x + Value(1.f));
                const Value dist = distance(pt, newCenter);

                axes[0].x = max(axes[0].x + distance(center, newCenter), dist);
                center = newCenter;
                axes[0].y = axes[0].x*axes[0].x;
            } break;
            case 2: {
                if(type == Null) {
                    center = pt;
                    axes[0] = axes[1] = Value(0.f);
                }
                const T& newCenter = min(center, pt);
                axes[0] += (center - newCenter);
                center = newCenter;

                const T& del = pt - center;

                axes[0] = max(axes[0], del);
                axes[1] = normalize(axes[0]);
            } break;
        }
        type = prefType;
    }

    inline virtual void add(const Bounds<T>& o) {
        if(o.type == 0) return;
        else if(this->type == 0) {
            *this = o;
            return;
        }

        switch(o.type) {
            case 1: {
                switch(type) {
                    case 1: {
                        this->add(o.center - o.axes[0].x);
                        this->add(o.center + o.axes[0].x);
                    } break;
                    case 2: {
                        this->add(o.center - o.axes[0].x);
                        this->add(o.center + o.axes[0].x);
                    } break;
                }
            } break;
            case 2: {
                switch(type) {
                    case 1: {
                        this->add(o.center);
                        this->add(o.center + o.axes[0]);
                    } break;
                    case 2: {
                        this->add(o.center);
                        this->add(o.center + o.axes[0]);
                    } break;
                }
            } break;
        }
    }

    inline virtual T farPoint() const {
        switch (type) {
            case 1:
            {
                return center;
            }
            break;
            default:
            case 2:
            {
                return center + axes[0];
            }
            break;
        }
    }

    inline virtual T nearPoint() const {
        return center;
    }

    inline virtual bool contains(const T& pt) const {
        switch(type) {
            default:
            case 0: return false; break;
            case 1: {
                const Value dist2 = distance2(pt, center);
                return dist2 <= axes[0].y + Value(0.003f);
            } break;
            case 2: {
                /*const T del = pt - center;
                if(length2(del) < 0.003f) return true;

                const Value theta = dot(axes[1], normalize(del));
                if(theta <= 0.707) return false;

                if(distance(center, axes[0]) * theta > distance(center, pt) - 0.003f) return true;
                else return false;*/
                const T corner = center + axes[0];
                const T b1 = min(center, pt), b2 = max(corner, pt);
                if(b1 != center || b2 != corner) return false;
                else return true;
            } break;
        }
    }

    inline virtual bool intersects(const Self& o) const {
        const T far = this->farPoint(), oFar = o.farPoint();
        return center.x <= oFar.x && far.x >= o.center.x && far.y >= o.center.y && center.y <= oFar.y;
    }

    inline virtual bool fullyContains(const Self& o) const {
        const T far = this->farPoint(), oFar = o.farPoint();
        return o.center.x >= center.x && o.center.y >= center.y && oFar.x <= far.x && oFar.y <= far.y;
    }

    inline virtual T dimensions() const {
        switch(type) {
            default:
            case 0: return T(); break;
            case 1: {
                return T(axes[0].x * Value(2));
            } break;
            case 2: {
                return abs(axes[0]);
            } break;
        }
    }

    inline virtual void repair() {
        switch(type) {
            default:
            case 0: break;
            case 1: {
                axes[0].y = axes[0].x*axes[0].x;
            } break;
            case 2: {
                axes[1] = normalize(axes[0]);
            } break;
        }
    }

    inline virtual Value& radius() {
        return axes[0].x;
    }
    inline virtual Value radiusConst() const{
        return axes[0].x;
    }
private:

};

#endif /* BOUNDS_H */
