#include "Path.h"
#include "NPath.impl.h"

namespace nvx {
    template class NPath<genv_t, pt_t>;

    path_t::~path_t() {

    }

    template class NPath<genv2d_t, pt2d_t>;

    path2d_t::~path2d_t() {

    }
}
