#ifndef NLINE_H
#define NLINE_H
#include "NVX.h"
#include "NParametric.h"

//#define NVXL_DEBUG
#if defined(NVX_DEBUG) || defined(NVXL_DEBUG)
#define NVXL_DBG(...) __VA_ARGS__
#else
#define NVXL_DBG(...) 
#endif

namespace nvx {
    
struct NLine_T{};
struct NLineTypes {
    enum {
        OpenSegment = -2,//Fully open, includes neither start nor end
        PolySegment = -1,//Including start, not including end
        Segment = 0,//Closed line segment
        Ray = 1,//Closed on start, opens to infinity
        Line = 2//"Closed" over infinity
    };
};
    
template<typename VT, typename PT, char LTYPE>
class NLineBase : public NParametric, public NLine_T {
public:
    typedef NLineBase GenSelf;
    typedef NLineBase<VT, PT, LTYPE> Self;
    //typedef NLineBase<typename std::add_lvalue_reference<VT>::type, LTYPE> RefSelf;
    //typedef NLineBase<typename std::remove_reference<VT>::type, LTYPE> ObjSelf;
    typedef typename base_value<VT>::type Value;
    typedef typename Value::HighpRow HighpValue;
    constexpr static char LType = LTYPE;
    
    PT p1, p2;
    inline constexpr VT start() const { return static_cast<VT>(p1); };
    inline constexpr VT end() const {
        if(isRay()) return static_cast<VT>(p1) + static_cast<VT>(p2);
        else return static_cast<VT>(p2);
    }
    
    //template<class=void> inline NLineBase() : p1(VT::Nil()), p2(VT::Nil()) {};
    inline NLineBase(const PT& a, const PT& b) : p1(a), p2(b) {};
    inline NLineBase(const Self& orig) : p1(orig.p1), p2(orig.p2) {};
    
    inline constexpr bool isRay() const { return LTYPE == NLineTypes::Ray || LTYPE == NLineTypes::Line; };
    inline constexpr bool isNegRay() const { return LTYPE == NLineTypes::Line; };
     
    inline constexpr bool closedOnStart() const {
        return LType >= NLineTypes::PolySegment;
    };
    inline constexpr bool closedOnEnd() const {
        return LType >= NLineTypes::Segment;
    };
    
    inline bool valid() const {
        return !start().isNil() && !end().isNil();
    }
    inline explicit operator bool() const {
        return this->valid();
    }
    
    inline VT del() const { 
        if(isRay()) return static_cast<VT>(p2);
        else return $::del(start(), end()); 
    };
    inline auto length() const { return $::length(this->del()); };
    inline auto length2() const { return $::length2(this->del()); };
    
    inline auto norm() const { return $::normalize(this->del()); };
    
    inline auto rnorm() const { return -this->norm(); };
    inline auto rdel() const { return -this->del(); };
    
    inline VT evaluate(const HighpValue& t) const {
        if(isRay()) return $::lerp(start(), start()+end(), t);
        else return $::lerp(start(), end(), t);
    }
    inline HighpValue rawInverse(const VT& pt) const {
        return $::max($::highp($::del(this->start(), pt)) / this->del());
    }
    inline HighpValue inverse(const VT& pt) const {
        return this->rawInverse(this->project(pt));
    }
    
    inline VT rawProject(const VT& pt) const {
        //std::cout << "RAWP del+" << $::del(start(), pt) << ", norm=" << this->norm() << "\n";
        return $::project($::highp($::del(start(), pt)), this->norm());
    }
    inline VT project(const VT& pt) const {
        const VT proj = this->rawProject(pt);
        //std::cout << "RaWPROJECT gave " << proj << "\n";
        if(isRay()) {
            if(!isNegRay() && dot(proj, this->del()) < 0) return start();//Behind one-direction ray, so clamp to the beginning
            else return start() + proj;
        } else {
            if($::dot(proj, this->del()) < 0) return start();//before start()
            else if($::length2(proj) > this->length2()) return end();//beyond end()
            else return start() + proj;//somewhere on the line
        }
    }
    inline VT del(const VT& pt) const {
        const VT projPt = this->project(pt);
        return $::del(pt, projPt);
    }
    inline HighpValue distance(const VT& pt) const {
        return $::distance(pt, this->project(pt));
    }
    inline Value distance2(const VT& pt) const {
        return $::distance2(pt, this->project(pt));
    }
    inline bool within(const VT& pt, const HighpValue& epsilon) const {
        return this->distance2(pt) <= sq(epsilon);
    }
    
    inline VT intersect(const VT& pt) const {
        if(this->distance2(pt) != 0) return VT::Nil();
        
        if((!closedOnStart() && pt == start()) || (!closedOnEnd() && pt == end())) return VT::Nil();
        return pt;
    }
    template<typename LT>
    inline typename std::enable_if<std::is_base_of<nvx::NLine_T, LT>::value, VT>::type 
    intersect(const LT& o) const {
        //if(LT::LType > LType) return o.intersect(*this);//More admissible line, let them evaluate
        
        NVXL_DBG(std::cout << "intersect " << this->str() << " with " << o.str() << "\n");
        
        const HighpValue denom = start().x * (o.end().y - o.start().y) + 
                                 end().x * (o.start().y - o.end().y) +
                                 o.end().x * (end().y - start().y) +
                                 o.start().x * (start().y - end().y);
        
        NVXL_DBG(std::cout << "denom = "<<denom<<" = \n\t");
        NVXL_DBG(std::cout << start().x * (o.end().y - o.start().y) << " + \t\t" << start().x << " * " << (o.end().y - o.start().y) << "  \n\t" << 
                                 end().x * (o.start().y - o.end().y) << " + \t\t" << end().x<<" * "<<(o.start().y - o.end().y)<<"\n\t" << 
                                 o.end().x * (end().y - start().y) << " +\t\t" <<  o.end().x<<" * "<<(end().y - start().y) <<"\n\t" << 
                                 o.start().x * (start().y - end().y) << "\t\t" << o.start().x<<" * "<<(start().y - end().y) <<"\n");
        
        if(denom == 0) {//Parallel
            NVXL_DBG(std::cout << "PARALLEL CASE, \n");
            //if(!this->isRay() && !o.isRay()) {
                if(o.closedOnStart() && $::between(start(), end(), o.start())) return o.start();
                else if(o.closedOnEnd() && $::between(start(), end(), o.end())) return o.end();
                else if(this->closedOnStart() && $::between(o.start(), o.end(), start())) return start();
                else if(this->closedOnEnd() && $::between(o.start(), o.end(), end())) return end();
                //else return VT::Nil();
                else if(this->isRay() || o.isRay()) {
            //} else {
                //std::cout << "\tPARR DISTS = " << this->distance2(o.start()) << ", " << this->distance2(o.end()) << o.distance2(this->start()) << ", " << o.distance2(this->end()) << "\n";
                    if(this->intersect(o.start())) return o.start();
                    else if(this->intersect(o.end())) return o.end();
                    else return VT::Nil();
                    //if(o.closedOnStart() && this->(o.start()) == 0) return o.start();
                    //else if(o.closedOnEnd() && this->distance2(o.end()) == 0) return o.end();
                    //else if(this->closedOnStart() && o.distance2(this->start()) == 0) return this->start();
                    //else if(this->closedOnEnd() && o.distance2(this->end()) == 0) return this->end();
                } else return VT::Nil();
            //}
        }
        
        const HighpValue sNumer=start().x   * (o.end().y - o.start().y) + 
                                o.start().x * (start().y - o.end().y) +
                                o.end().x   * (o.start().y - start().y);
        if((sNumer == 0 && !closedOnStart()) || (sNumer == denom && !closedOnEnd() && !isRay())) return VT::Nil();//Improper intersection @ start()
        const HighpValue rawS = sNumer / denom;
        const HighpValue s = rawS;
        
        const HighpValue tNumer = -(start().x   * (o.start().y - end().y) + 
                                    end().x * (start().y - o.start().y) +
                                    o.start().x   * (end().y - start().y) );
        if((tNumer == 0 && !o.closedOnStart()) || (tNumer == denom && !o.closedOnEnd() && !isRay())) return VT::Nil();//Improper intersection @ end()
        const HighpValue rawT = tNumer / denom;
        const HighpValue t = rawT;
        
        NVXL_DBG(std::cout << "s = " << s << ", t = " << t << ", denom=" << denom << "\n");
        NVXL_DBG(std::cout << "line inter got this neg=" << isNegRay() << ", ray=" << isRay() << " and other neg=" << o.isNegRay() << ", ray=" << o.isRay() << "\n");
        NVXL_DBG(std::cout << "would give point " << (start() + rawS * (this->del())) << "\n");
        NVXL_DBG(std::cout << "would give point " << (o.start() + rawT * (o.del())) << "\n");
        const bool sValid = ((s >= 0.0 || isNegRay()) && (s <= 1.0 || isRay())) && 
                            ((s != 0 || closedOnStart()) && (s != 1 || closedOnEnd())),
                   tValid = ((t >= 0.0 || o.isNegRay()) && (t <= 1.0 || o.isRay())) &&
                            ((t != 0 || o.closedOnStart()) && (t != 1 || o.closedOnEnd()));
        const bool foundInter = sValid && tValid;
        
        if(!foundInter) return VT::Nil();
        
        NVXL_DBG(std::cout << "\t\tusing lines: "<<start() << ":" <<  (this->del()) << ", "<<o.start() << ":"<< (o.del()) <<" \n");
        NVXL_DBG(std::cout << "\tisRay=" << isRay() << ", s = " << s << ", t=" << t << " for pts sp=" <<(start() + s *  (this->del()))<< ", tp="<<(o.start() + t *  (o.del()))<<" \n");
        
        
        if(sValid) {
            return start() + rawS * (this->del());
        } else {
            return o.start() + rawT * (o.del());
        }
    }
    
    template<typename LT> inline typename std::enable_if<std::is_base_of<nvx::NLine_T, LT>::value, bool>::type 
    collinear(const LT& o) const {
            return (start().x * (o.end().y - o.start().y) +
                    end().x * (o.start().y - o.end().y) +
                    o.end().x * (end().y - start().y) +
                    o.start().x * (start().y - end().y)) == 0;
        }
    
#ifdef NVX_STR_CASTS
    inline std::string str() const {
        std::stringstream ss;
        if(LTYPE == NLineTypes::Line) ss << "Trueline(";
        else if(isRay()) ss << "Ray(";
        else ss << "Line(";
        if(start().isNil()) ss << "nil";
        else ss << start() << ", " << end();
        ss << ")";
        return ss.str();
    }
#endif
private:

};

template<typename VT, typename PT=VT>
using Line=NLineBase<VT, PT, NLineTypes::Segment>;
template<typename VT, typename PT=VT>
using Ray=NLineBase<VT, PT, NLineTypes::Ray>;
template<typename VT, typename PT=VT>
using TrueLine=NLineBase<VT, PT, NLineTypes::Line>;

template<typename VT, typename PT=VT>
using PolySegment=NLineBase<VT, PT, NLineTypes::PolySegment>;
template<typename VT, typename PT=VT>
using OpenLine=NLineBase<VT, PT, NLineTypes::OpenSegment>;

template<typename VT, typename PT=VT>
using LineRef=NLineBase<VT&, PT&, NLineTypes::Segment>;
template<typename VT, typename PT=VT>
using RayRef=NLineBase<VT&, PT&, NLineTypes::Ray>;
template<typename VT, typename PT=VT>
using TrueLineRef=NLineBase<VT&, PT&, NLineTypes::Line>;

template<typename VT, typename PT=VT>
using PolySegmentRef=NLineBase<VT&, PT&, NLineTypes::PolySegment>;
template<typename VT, typename PT=VT>
using OpenLineRef=NLineBase<VT&, PT&, NLineTypes::OpenSegment>;

};

#ifdef NVX_STR_CASTS
template<typename genType>
typename std::enable_if<std::is_base_of<nvx::NLine_T, genType>::value, std::ostream&>::type 
inline operator<<(std::ostream& out, const genType& g) {
    return out << g.str();
}
#endif

#endif /* NLINE_H */

