#ifndef NPOLYGON_IMPL_H
#define NPOLYGON_IMPL_H
#include "NPolygon.h"
#include "NLine.h"

//#include "NPolygon.clip.h"
//template struct ClipperLib<>;
namespace nvx {

    template<typename T, typename PT_T>
    inline int NPolygon<T, PT_T>::removeRange(int eraseStart, int eraseEnd, int dir) {
        if(this->size() == 0) return 0;
        if(dir < 0) std::swap(eraseStart, eraseEnd);

        eraseStart = arke_poly_nmod(eraseStart, this->size());
        eraseEnd = arke_poly_nmod(eraseEnd, this->size());

        unsigned int eraseMinFirst, eraseMaxFirst, eraseMinSec = 0, eraseMaxSec = 0;
        int newIdx = 0;
        if (eraseStart > eraseEnd) {
            eraseMinFirst = eraseStart;
            eraseMaxFirst = this->size();
            eraseMinSec = 0;
            eraseMaxSec = eraseEnd+1;
        } else {
            eraseMinFirst = eraseStart;
            eraseMaxFirst = eraseEnd+1;
        }

        unsigned int origSize = this->size();
        //Erase 0-max elements first, so as to not invalidate indices
        if (eraseMinSec != eraseMaxSec) {
            //std::cout << "\tSEC erasing from " << eraseMinSec << " to " << eraseMaxSec<< "\n";
            points.erase(points.begin() + eraseMinSec, points.begin() + eraseMaxSec);
            if(dir >= 0) newIdx = 0;
            else newIdx = -1;
            //std::cout << "\tSEC idx = " << newIdx << "\n";
        }

        //std::cout << "\terasing from " << eraseMinFirst << " to " << eraseMaxFirst<< " for size=" << this->size() << "\n";
        points.erase(points.begin() + eraseMinFirst, points.begin() + eraseMaxFirst);
        if(eraseMinSec == eraseMaxSec) {
            if(dir > 0) newIdx = arke_poly_nmod(eraseMinFirst, origSize);
            else newIdx = arke_poly_nmod( eraseMinFirst+dir, origSize);
            //std::cout << "\tFIRST idx = " << newIdx << "\n";
        }
        //std::cout << "ret " << arke_poly_nmod(newIdx, size()) << " from newIdx=" << newIdx << ", size=" << size() << "\n";
        return arke_poly_nmod(newIdx, size());
    }

    template<typename T, typename PT_T>
    inline int NPolygon<T, PT_T>::remove(int idx, int nextDir) {
        if(nextDir >= 0) nextDir = 0;
        else nextDir = -1;

        idx = arke_poly_nmod(idx, this->size());
        points.erase(points.begin()+idx);

        return arke_poly_nmod(idx + nextDir, this->size());
    }

    template<typename T, typename PT_T>
    inline int NPolygon<T, PT_T>::insert(int base, const PT_T& pt, int dir) {
        //const int origBase = base;
        base = arke_poly_nmod(base + ((dir < 0)? 0 : 1), this->size()+1);

        //auto it = points.insert(points.begin() + base, pt);
        //it->setParent(this);

        bounds.add(pt.position());
        accumulatedCenter += pt.position();

        return arke_poly_nmod(base, this->size());
    }

    template<typename T, typename PT_T>
    void NPolygon<T, PT_T>::removeList(const std::list<Idx>& toRemove) {
        if(toRemove.size() == 0) return;
        auto it = toRemove.begin();

        const unsigned int newSize = this->size() - toRemove.size();
        for(unsigned int i = 0, newI = 0; newI < newSize && i < size(); i++) {
            if(it != toRemove.end() && i == *it) {
                ++it;
                continue;//Skip
            } else {
                if(newI != i) points[newI] = points[i];
                newI++;
            }
        }
        points.resize(newSize);
    }

    template<typename T, typename PT_T>
    void NPolygon<T, PT_T>::decollinearize() {
        std::list<Idx> toRemove;
        for(unsigned int i = 0; i < this->size(); i++) {
            if(!this->closed() && (i == 0 || i == this->size()-1)) continue;
            const Segment& prevSeg = this->segments()[i-1];
            const Segment& nextSeg = this->segments()[i];

            if(abs(dot(prevSeg.norm(), nextSeg.norm())) == 1) toRemove.push_back(i);
        }
        this->removeList(toRemove);
    }

    template<typename T, typename PT_T>
    void NPolygon<T, PT_T>::smooth(HighpValue epsilon, bool recursive) {
        epsilon = $::sq(epsilon);
        std::list<Idx> toRemove;
        for(unsigned int i = 0; i < this->size(); i++) {
            if(!this->closed() && (i == 0 || i == this->size()-1)) continue;
            const Line<T> smoothedSeg(this->get(i-1).position(), this->get(i+1).position());
            const Coord smoothedDel = smoothedSeg.del(this->get(i).position());//Vector that smoothing would change the point by

            //If simplifying would give a point which the normal faces, changing its distance by no more than epsilon, we may smooth it out
            //Also, if we've generated a collinearity (len del = 0) at some point in the recursive call, remove it
            if( ($::dot(smoothedDel, this->normal(i)) > 0 && $::length2(smoothedDel) <= epsilon) ||
                $::length2(smoothedDel) == 0) toRemove.push_back(i);
        }
        this->removeList(toRemove);
        if(recursive && toRemove.size() > 0) this->smooth(epsilon, true);
    }

    template<typename T, typename PT_T>
    inline void NPolygon<T, PT_T>::fastSimplify(HighpValue epsilon, HighpValue relEpsilon) {
        //std::cout << "SIMPLIFY pOLY\n";
        if (points.size() <= 3) return;

        for (auto it = points.begin(); it != points.end(); it = std::next(it)) {
            if (points.size() <= 3 || this->index(&(*it)) >= this->size()) break;
            else if(!this->closed() && (this->index(&(*it)) == 0 || this->index(&(*it)) == this->size()-1)) continue;
            Point* pt = &(*it);
            Point* nxt = pt->right(this);
            //T delta = nxt->position() - pt->position();
            HighpValue deltaErr = 0.f;

            Point* prevFollows = nullptr;
            Point* follows = nxt->right(this);
            //std::cout << "check " << to_string(pt->position()) << " to " << to_string(nxt->position()) << "\n";
            if (pt == nxt || pt->position() == nxt->position()) break;

            while (follows != nullptr && follows != pt && follows != nxt) {
                //std::cout << "follows is #" << this->index(follows) << "\n";
                T newDelta = follows->position() - pt->position();
                HighpValue newDeltaErr = 0.f;
                if (length(newDelta) < epsilon / Value(10.f)) break;

                //std::cout << "\textending to " << to_string(follows->position()) << " giving delta=" << to_string(newDelta) << "\n";
                for (Point* trace = pt;; trace = trace->right(this)) {
                    //const T proj = (pt->position() - follows->position()) - dot(pt->position() - follows->position(), newDelta) * newDelta;
                    const T proj = pt->position() + dot((trace->position() - pt->position()), normalize(newDelta)) * normalize(newDelta);
                    const T deltaProj = trace->position() - proj;
                    const HighpValue lineDistSigned = length(deltaProj); // * ::sgn(dot(deltaProj, newDelta));
                    //std::cout << "\t\tpt err = " << lineDistSigned << " from " << to_string(proj) << " pt " << to_string(trace->position()) << " del " << to_string(deltaProj) << "\n";
                    newDeltaErr += lineDistSigned;
                    if (trace == follows) break;
                }
                //newDeltaErr /= length(newDelta);
                //std::cout << "\tgot dErr = " << newDeltaErr << " eps="<<(epsilon + relEpsilon * 1/length(newDelta)) << " from len=" << length(newDelta)<<"\n";
                if (fabs(fabs(deltaErr) - fabs(newDeltaErr)) > epsilon + relEpsilon * Value(1) / length(newDelta)) {
                    break;
                } else {
                    deltaErr = newDeltaErr;
                    prevFollows = follows;
                    follows = follows->right(this);
                }
            }

            Point* sectionEnd = prevFollows; //follows->left(this);//Last point satisfying simplification
            if (sectionEnd == nullptr || sectionEnd == nxt || sectionEnd == pt) continue; //Nothing could be simplified
            //Can be simplified, remove all points between pt and sectionEnd
            auto rmIter = std::next(it);
            if (rmIter == points.end()) rmIter = points.begin();

            int eraseStart = (this->index(pt) + 1) % this->size(), eraseEnd = this->index(sectionEnd) - 1;
            if (eraseEnd >= int(this->size())) continue;

            //if(eraseStart > eraseEnd) eraseEnd = std::min(this->index(pt) - 1, eraseEnd);
            this->removeRange(eraseStart, eraseEnd);

            /*unsigned int eraseMinFirst, eraseMaxFirst, eraseMinSec = 0, eraseMaxSec = 0;
             *        if (eraseStart > eraseEnd) {
             *            eraseMinFirst = eraseStart;
             *            eraseMaxFirst = this->size() - 1;
             *            eraseMinSec = 0;
             *            eraseMaxSec = std::min(this->index(pt) - 1, eraseEnd);
        } else {
            eraseMinFirst = eraseStart;
            eraseMaxFirst = eraseEnd;
        }
        //std::cout << "\terasing between " << to_string(pt->position()) << " and " << to_string(sectionEnd->position()) << " with err=" << deltaErr << "\n";
        //std::cout << "\terasing from " << eraseMinFirst << " to " << eraseMaxFirst<< " for size=" << this->size() << "\n";

        points.erase(points.begin() + eraseMinFirst, points.begin() + eraseMaxFirst);
        if (eraseMinSec != eraseMaxSec) {
            points.erase(points.begin() + eraseMinSec, points.begin() + eraseMaxSec);
            //std::cout << "SEC erasing from " << eraseMinSec << " to " << eraseMaxSec<< "\n";
        }*/

            if (points.size() <= 3) break;
            /*
             *        while(rmIter.position() != sectionEndPoint) {
             *            auto newIter = std::next(rmIter);
             *            if(newIter == points.end()) newIter = points.begin();
             *
             *            points.erase(rmIter);
             *            //std::cout << "\tERASE" << this->index(&(*rmIter)) << "\n";
             *            if(this->index(&(*rmIter))  > this->size()) sleep(20);
             *
             *            rmIter = newIter;
        }*/
        }
    }

    template<typename T, typename PT_T>
    inline bool NPolygon<T, PT_T>::contains(const T& pt, bool allowEdges, Contains_RData * rdata) {
        Contains_RData local;
        const TrueLine<T> ray(pt, $::normalize(T(1, 0)));
        NVXP_DBG(std::cout << "\tCasting wtih " << ray <<"\n");

        for(auto it = this->segments().begin(); it != this->segments().end(); ++it) {
            NVXP_DBG(std::cout << "testing segments on pts " << it.idx << " -> " << it.idx+1 << ", size=" << this->size() << " and end=" << this->segments().end().idx << "\n");
            const Segment& seg = *it;
            if(seg.intersect(pt)){
                NVXP_DBG(std::cout << "SEG INTERSECT " << pt << " on " << seg << "\n");
                if(allowEdges) return true;//Edges are always included (closed polygon)
                else return false;
            }

            const T inter = seg.intersect(ray);
            NVXP_DBG(std::cout << "TESTING SEG=" << seg << " ");
            if(!inter) {
                NVXP_DBG(std::cout << "\t\tno intersection with " << seg << "\n");
                continue;
            }

            NVXP_DBG(std::cout << "\t\tpoly crossing to " << pt << " at " << inter << "\n");
            //NVXP_DBG(std::cout << "\t\tray del=" << ray.del() << ", rayStart = " << ray.start(),)
            /*if(ray.collinear(seg)) {//This case is implicitly handled by PolySegments being open on their endpoints, and enforcing r%2 == l%2
             *            std::cout << "COLLINEAR\n";
        } else {*/
            if(inter == seg.start()) {//Hits the vertex, so check if the point's segments "cross" our ray, or stay on the same side
                const Coord rPerp = $::perp(ray.del());
                const Coord prevDel = -(this->segments()[it.idx-1].del());
                if($::sgn($::dot(rPerp, seg.del())) == $::sgn($::dot(rPerp, prevDel))) continue;
            }

            //Valid, so sum it up
            {
                if($::dot(ray.del(), $::del(ray.start(), inter)) > 0) local.rCross++;
                else local.lCross++;
            }
        }

        if(rdata != nullptr) {
            rdata->lCross += local.lCross;
            rdata->rCross += local.rCross;
        } else rdata = &local;

        if(this->next != nullptr) return next->contains(pt,allowEdges, rdata);
        NVXP_DBG(std::cout << "GOT RDATA r=" << rdata->rCross << ", " << rdata->lCross << "\n");

        return rdata->rCross % 2 == 1 && rdata->lCross % 2 == 1;
    }

    template<typename T, typename PT_T>
    inline void NPolygon<T, PT_T>::fixNormals(bool allowDeletion) {
        if(!Point::HasNormals()) return;

        std::list<Idx> redundentPoints;
        //Calculate known normals
        for(unsigned int i = 0; i < this->size(); i++) {
            PT_T& pt = this->get(i);
            PT_T *prevPt = pt.left(this), *nextPt = pt.right(this);
            if(prevPt == nullptr || nextPt == nullptr) {
                //pt.normal() = T(0.f);
                continue;
            }

            //Hints as to alignment
            T seedNorm = pt.normal();

            T calcNorm = normalize(pt.position() - prevPt->position()) + normalize(pt.position() - nextPt->position());
            if(length2(calcNorm) < Value(0.002f) && length2(seedNorm) >= Value(0.002f)) calcNorm = seedNorm;

            /* //Code to automatically repair normals based on sibling info, if needed one day. For now, deletion is beneficial in a number of ways.
             *        while(false && length2(calcNorm) < 0.002f) {
             *            //calcNorm = pt.normal();
             *            //They're colinear, flag for deletion
             *            //redundentPoints.push_back(i);
             *            //(pt.normal()) = T(0.f)
             *            PT_T *newPrev = prevPt->left(this);
             *            PT_T *newNext = nextPt->right(this);
             *            //We reached an edge or went full circle, give up.
             *            if(newPrev == nullptr || newNext == nullptr || newPrev == newNext || newPrev == nextPt || newNext == prevPt) break;
             *            prevPt = newPrev; nextPt = newNext;
             *
             *            if(length2(seedNorm) < 0.002f && length2(prevPt.normal()()) >= 0.002f) seedNorm = prevPt.normal()();
             *            else if(length2(seedNorm) < 0.002f && length2(nextPt.normal()()) >= 0.002f) seedNorm = nextPt.normal()();
             *
             *            calcNorm = normalize(pt.position() - prevPt->position()) + normalize(pt.position() - nextPt->position());
        }*/

            if(length2(calcNorm) < Value(0.002f)) {
                if(allowDeletion) redundentPoints.push_back(i);
                continue;
            }

            calcNorm = normalize(calcNorm);

            HighpValue normDot = sgn(dot(seedNorm, calcNorm));
            if (normDot == 0.f) normDot = 1.f;

            //Because we checked that the point type has normals (so this is well defined) and that more SFINAE is the last thing NVX needs, just cast away const
            const_cast<Coord&>(pt.normal()) = calcNorm * normDot;
            //std::cout << "\tNORM " << int(pt.position().x) << ", " << int(pt.position().y) << " = " << to_string(calcNorm*normDot) << "\n";
        }

        //Iterate over points to delete from greatest to least, to not invalidate idx's
        /*for(auto it = redundentPoints.rbegin(); it != redundentPoints.rend(); ++it) {
         *        this->remove(*it);
    }*/
        this->removeList(redundentPoints);
    }

    template<typename T, typename PT_T>
    inline void NPolygon<T, PT_T>::fixBounds() {
        bounds.zero();
        accumulatedCenter = T(0.f);
        for(PT_T& pt : points) {//Also fix parent refs if needed
            accumulatedCenter += pt.position();
            bounds.add(pt.position());
        }
    }

    template<typename T, typename PT_T>
    inline void NPolygon<T, PT_T>:: calculateArea() {
        if (this->size() < 3) {
            trueArea = 0.f;
            return;
        }

        Value raw = 0.f; T rawCenter = T(0.f);
        for (unsigned int i = 0; i < this->size(); i++) {
            //unsigned int ni = (i + 1) % this->size();
            const T &a = this->get(i  ).position();
            const T &b = this->get(i+1).position();

            Value cross = $::scalar_cross(a,b);//a.x * b.y - b.x * a.y;

            raw += cross;
            rawCenter += (a + b) * cross;
        }
        trueArea = Value(0.5f) * fabs(raw);
        NVXP_DBG(std::cout << "got raw center=" << rawCenter << ", div=" << (Value(6.f) * trueArea * sgn(raw)) << "\n");
        trueCenter = rawCenter / Value(Value(6.f) * trueArea * sgn(raw));
        NVXP_DBG(std::cout << "\t=" << trueCenter << ", per= " << rawCenter.x << " / " << (Value(6.f) * trueArea * sgn(raw)) << " = " << (rawCenter.x/(6.f * trueArea * sgn(raw))) << "\n");

        //Calculate radius
        HighpValue minRadSq = std::numeric_limits<HighpValue>::max(), maxRadSq = 0.f;
        for(unsigned int i = 0; i < this->size(); i++) {
            const HighpValue dSq = distance2(this->get(i).position(), trueCenter);
            if(dSq > maxRadSq) maxRadSq = dSq;
            if(dSq < minRadSq) minRadSq = dSq;
        }
        minRad = sqrt(minRadSq);
        maxRad = sqrt(maxRadSq);
    }

    template<typename T, typename PT_T>
    inline void NPolygon<T, PT_T>::repair(bool allowDelete) {
        this->fixBounds();
        this->fixNormals(allowDelete);
        this->calculateArea();
    }

    template<typename T, typename PT_T>
    inline int NPolygon<T, PT_T>::insert(int base, int baseDir, const NPolygon *const& o, int first, int last, int dir) {
        if(baseDir > 0) base = arke_poly_nmod(base + ::sgn(baseDir), this->size());
        last = arke_poly_nmod(last, o->size());
        if(dir < 0) std::swap(first, last);

        int curBase = base, curOther = first;
        while(true) {
            curBase = this->insert(curBase, o->get(curOther), baseDir);
            if(curOther == last) break;

            curOther = arke_poly_nmod(curOther + dir, o->size());
        }

        return curBase;
    }


    template<typename T, typename PT_T>
    inline typename NPolygon<T, PT_T>::Projection NPolygon<T, PT_T>::projection(const NPolygon<T, PT_T>::Viewpoint &view) {
        const T& dStart = view.start;
        const T& testAxisBase = view.end;
        const T axisMidNorm = normalize(testAxisBase - dStart);
        const T axisNormCross = T(axisMidNorm.y*-Value(1.f), axisMidNorm.x);
        const bool& ortho = view.ortho;

        T axisA, axisB;
        if(view.axes[0] == T(0.f)) {//Construct perpandicular to end-start
            axisA = axisNormCross;
        } else axisA = view.axes[0];

        if(view.axes[1] == axisA || view.axes[1] == T(0.f)) {
            axisB = -axisA;
        } else axisB = view.axes[1];

        const T axisANorm = normalize(axisA), axisBNorm = normalize(axisB);

        HighpValue axisADistSq = 0.f, axisBDistSq = 0.f;
        HighpValue minDistA = 10000.f, minDistB = 10000.f;
        HighpValue bestDotA = 2.f, bestDotB = 2.f;
        T axisABest = T(0.f), axisBBest = T(0.f);
        Point *edgeA = nullptr, *edgeB = nullptr;

        for (unsigned int i = 0; i < this->size(); i++) {
            Point *pt = &this->get(i);
            T ls = pt->position();

            //std::cout << "projecting " << VP(ls) << "\n";
            T lineStart, dLine;
            const T *selAxis, *otherAxis;
            if (ortho) {
                lineStart = ls - axisMidNorm;
                if (sgn(dot(axisNormCross, -axisBNorm)) == sgn(dot(lineStart - dStart, axisNormCross))) {
                    selAxis = &axisANorm; otherAxis = &axisBNorm;
                    dLine = -axisBNorm;
                } else {
                    selAxis = &axisBNorm; otherAxis = &axisANorm;
                    dLine = -axisANorm;
                }
            } else {
                lineStart = dStart;
                dLine = normalize(ls - lineStart);
                if (sgn(dot(axisNormCross, -axisBNorm)) == sgn(dot(dLine, axisNormCross))) {
                    selAxis = &axisANorm; otherAxis = &axisBNorm;
                } else {
                    selAxis = &axisBNorm; otherAxis = &axisANorm;
                }
            }

            //std::cout << "\tusing start="<<VP(lineStart)<<", dLine=" << VP(dLine) << "\n\t axis=" << VP(*selAxis) << ", base=" << VP(testAxisBase) << "\n";

            T edgePt;
            HighpValue edgeTime;
            if(ortho) {
                edgeTime = dot(*selAxis, ls - testAxisBase);
                edgePt = edgeTime * (*selAxis) + testAxisBase;
                HighpValue axisDist = distance2(edgePt, ls);

                //Handle bounded cases
                //bool axisPass = false;
                if(view.bounded) {
                    HighpValue edgeTime2 = dot(*otherAxis, ls - dStart);
                    T edgePt2 = edgeTime2 * (*otherAxis) + dStart;
                    HighpValue axisDist2 = distance2(edgePt2, ls);
                    if(axisDist2 < 0.1f) axisDist = -1.f;//Close enough to bounding axis, we can be constrained here
                }

                if (selAxis == &axisANorm) {
                    if ((view.admissable && edgeTime > axisADistSq) || (!view.admissable && axisDist+Value(0.1f) < minDistA)) {
                        edgeA = pt;
                        axisADistSq = edgeTime;
                        axisABest = edgePt;
                        minDistA = axisDist;
                    }
                } else {
                    if ((view.admissable && edgeTime > axisBDistSq) || (!view.admissable && axisDist+Value(0.1f) < minDistB)) {
                        edgeB = pt;
                        axisBDistSq = edgeTime;
                        axisBBest = edgePt;
                        minDistB = axisDist;
                    }
                }
            } else {
                //bool foundInter = this->intersectRays(lineStart, dLine, testAxisBase, *selAxis, edgePt, &edgeTime);
                const Ray<T> curRay(lineStart, dLine), axisRay(testAxisBase, *selAxis);
                edgePt = axisRay.intersect(curRay);
                const bool foundInter = bool(edgePt);

                if (foundInter) {
                    const HighpValue d2 = dot(*selAxis, edgePt);//dot(*selAxis, (edgePt - testAxisBase)); //distance2(lineStart, ls);
                    //std::cout << "\tgot inter pt=" << VP(edgePt) << ", t=" << edgeTime << ", d2=" << d2 << "\n";

                    const T ptNorm = normalize(edgePt - lineStart);
                    const HighpValue ptDot = dot(ptNorm, axisMidNorm);

                    if (selAxis == &axisANorm) {
                        if ((!ortho && ptDot < bestDotA) || (ortho && (d2 > axisADistSq))) {
                            edgeA = pt;
                            axisADistSq = d2;
                            axisABest = edgePt;
                            bestDotA = ptDot;
                        }
                    } else {
                        if ((!ortho && ptDot < bestDotB) || (ortho && (d2 > axisBDistSq))) {
                            edgeB = pt;
                            axisBDistSq = d2;
                            axisBBest = edgePt;
                            bestDotB = ptDot;
                        }
                    }

                }
            }
        }


        Projection ret;
        ret.intersections[0] = axisABest;
        ret.intersections[1] = axisBBest;
        ret.points[0] = edgeA;
        ret.points[1] = edgeB;

        //Projection failed, we're colinear
        if(edgeA == edgeB) edgeA = edgeB = nullptr;

        if (edgeA != nullptr && edgeB != nullptr) {
            if(false&&!view.admissable && (!view.bounded && view.ortho)) {//The hard way, sum all the norm dots and choose the most aligned with dStart
                HighpValue dotSumP = 0.f, dotSumN = 0.f;
                int dotCountP = 0, dotCountN = 0;
                for(Point* pt = edgeA->right(this); pt != edgeB; pt = pt->right(this)) {
                    dotSumP += dot(pt->normal(), axisMidNorm);
                    dotCountP++;
                }
                for(Point* pt = edgeA->left(this); pt != edgeB; pt = pt->left(this)) {
                    dotSumP += dot(pt->normal(), axisMidNorm);
                    dotCountN++;
                }

                if(dotCountP > 0) dotSumP /= HighpValue(dotCountP);
                if(dotCountN > 0) dotSumN /= HighpValue(dotCountN);

                if(dotSumP > dotSumN) ret.winding = 1;
                else ret.winding = -1;
            } else {
                /*T leftNorm = $::normalize(edgeA->left(this)->position() - edgeA->position());
                 *            T rightNorm = $::normalize(edgeA->right(this)->position() - edgeA->position());
                 *            const T toOrigin = $::normalize(dStart - edgeA->position());
                 *
                 *            const HighpValue lnDot = $::dot(toOrigin, leftNorm), rnDot = dot(toOrigin, rightNorm);
                 */
                const Idx edgeAIdx = this->index(edgeA);
                const Segment& prevSeg = this->segments()[edgeAIdx-1], &nextSeg = this->segments()[edgeAIdx];
                NVXP_DBG(std::cout << "projection got segments " << prevSeg << " -> " << nextSeg << "\n");

                const HighpValue prevDot = $::dot(axisMidNorm, -prevSeg.norm()), nextDot = $::dot(axisMidNorm, nextSeg.norm());
                NVXP_DBG(std::cout << "for dots " << prevDot << " and " << nextDot << "\n");

                if(prevDot > nextDot) ret.winding = -1;
                else if(prevDot < nextDot) ret.winding = 1;
                else {//Equal, so one must be closer (assuming no collinearities or redundencies
                    NVXP_DBG(std::cout << "cmd dist: " << distance2(prevSeg.start(), dStart)  << " and " << distance2(nextSeg.end(), dStart) << "\n");
                    if(distance2(prevSeg.start(), dStart) > distance2(nextSeg.end(), dStart)) ret.winding = -1;
                    else ret.winding = 1;
                }
                NVXP_DBG(std::cout << "winding ret " << ret.winding << "\n");

                //if(rnDot < lnDot) ret.winding = 1;
                //else ret.winding = -1;
                //std::cout << "projection got dots rn=" << rnDot << ", lnDot=" << lnDot << ", ret winding=" << ret.winding << "\n";
            }
        }
        return ret;
    }

    template<typename T>
    inline static T sgnmod(T k, T n) {
        return ((k %= n) < 0) ? k+n : k;
    }

    template<typename T, typename PT_T>
    inline void NPolygon<T, PT_T>::projectOnto(const NPolygon<T, PT_T>::Viewpoint &view) {
        if(this->size() <= 4) return;//No point, no verts can be eliminated safely

        const Projection proj = this->projection(view);
        if(!proj) return;

        NVXP_DBG(std::cout << "\tProjectOnto sz="<<size()<<" got start="<<this->index(proj.points[0]) << ":" << VP(proj.points[0]->position()) << ", end=" <<
        this->index(proj.points[1]) << ":" << VP(proj.points[1]->position()) << " with winding " << int(proj.winding) << "\n");

        int eraseInc = proj.winding;
        T ptA = proj.points[0]->position(), ptB = proj.points[1]->position();
        int eraseStart = this->index(proj.points[0]) + eraseInc,
        eraseEnd = this->index(proj.points[1]) - eraseInc;

        if(eraseStart + eraseInc == eraseEnd || eraseEnd - eraseInc == eraseStart) return;//Don't delete the entire polygon (or direct siblings)


        NVXP_DBG(std::cout << "\terasing from " << eraseStart << " to " << eraseEnd << "\n");
        for(int i = eraseStart; ; i = (i+eraseInc < 0)? size()-1 : (i+eraseInc)%size()) {
            NVXP_DBG(std::cout << "\t\tShould erase " << i << "\n");
            if( i == sgnmod(eraseEnd, int(this->size()))) break;
        }

        int newIdx = this->removeRange(eraseStart, eraseEnd, eraseInc);// + 2*eraseInc;
        NVXP_DBG(std::cout << "\tnewIdx = " << newIdx << "\n");
        NVXP_DBG(std::cout << "END PROJECT\n");


        //this->insert(newIdx, view.end, -1);
        if(eraseInc == 1) {
            if(distance2(proj.intersections[1], ptB) > 0.1f) this->insert(newIdx, proj.intersections[1], -1);
            this->insert(newIdx, view.end, -1);
            if(distance2(proj.intersections[0], ptA) > 0.1f) this->insert(newIdx, proj.intersections[0], -1);
        } else {
            newIdx = this->insert(newIdx, view.end, 1);
            if(distance2(proj.intersections[0], ptA) > 0.1f) this->insert(newIdx, proj.intersections[0], 1);
            if(distance2(proj.intersections[1], ptB) > 0.1f) this->insert(newIdx, proj.intersections[1], -1);
        }

        this->repair();
    }

//    namespace CLP = CLIPPER_T(N2, N2);

    template<typename T, typename PT_T>
    int NPolygon<T, PT_T>::op(int opType, NPolygon<T, PT_T>::Self*const a, const NPolygon<T, PT_T>::Self*const b) {
        Self* tmp = opnew(opType, a, b);
        std::swap(*a, *tmp);
        delete tmp;
        return 1;
    }

    template<typename T, typename PT_T>
    inline typename NPolygon<T, PT_T>::Self* NPolygon<T, PT_T>::opnew(int opType, const NPolygon<T, PT_T>::Self*const a, const NPolygon<T, PT_T>::Self*const b) {
        Self* ret = new Self();
        ((void)opType); ((void)a); ((void)b);
        /*if(a->size() < 3 || b->size() < 3) return ret;
         *
         *            unsigned char ply = 0;
         *            NI normalMult[2];
         *            bool inc[2];
         *            const NPolygon<T, PT_T>::Self* polys[2] = {a, b};
         *            const Idx idx[2] = {0,0};
         *            const Idx end[2] = {0,0};
         *
         *            CLP::ClipType clpType;
         *            switch(opType) {
         *                case csg::Add:
         *                    clpType = CLP::ctUnion;
         *                    normalMult[0] = 1;
         *                    normalMult[1] = 1;
         *                    inc[0] = true;
         *                    inc[1] = true;
         *                    break;
         *                case csg::Subtract:
         *                    clpType = CLP::ctDifference;
         *                    normalMult[0] = 1;
         *                    normalMult[1] = -1;
         *                    inc[0] = b->contains(a->get(0).position());
         *                    inc[1] = true;
         *                    break;
         *                case csg::Intersection:
         *                     clpType = CLP::ctIntersection;
         *                    break;
         *                case csg::Xor:
         *                    clpType = CLP::ctXor;
         *                    break;
    }

    do {
        const auto *const& s = polys[ply], o = polys[(ply+1)%2];
        const auto& seg = s->segments()[idx[ply]];
        const auto inter = o->intersect(seg);

        if(inter) {

    } else if(inc[ply]) {
        ret->add(seg->p1);
        ret->rbegin()->setNormal(ret->rbegin()->normal()*normalMult[ply]);
    } else idx[ply]++;
    } while(idx[ply] != end[ply]);
    */
        /*for(const auto& seg : a->segments()) {
         *                const auto inter = b->intersect(seg, true);
         *                if(!inter) {
         *                    ret.add(seg.p1);
         *                    continue;
    }


    }*/
        /*CLP::Clipper c;
         *
         *            CLP::PolyFillType windA = CLP::pftNonZero, windB = CLP::pftNonZero;
         *            c.AddPaths(a->asList(), CLP::ptSubject, true);
         *            c.AddPaths(b->asList(), CLP::ptClip, true);
         *            c.Execute(clpType, ret->asList(), windA, windB);
         */
        return ret;
    }
};

#endif /* NPOLYGON_IMPL_H */
