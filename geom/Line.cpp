#include "Line.h"

namespace nvx {
    template class NLineBase<genv_t, pt_t, NLineTypes::Segment>;
    template class NLineBase<genv_t, pt_t, NLineTypes::Ray>;
    template class NLineBase<genv_t, pt_t, NLineTypes::Line>;

    line_t::~line_t() {

    }
    ray_t::~ray_t() {

    }
    trueline_t::~trueline_t() {

    }

    template class NLineBase<genv2d_t, pt2d_t, NLineTypes::Segment>;
    template class NLineBase<genv2d_t, pt2d_t, NLineTypes::Ray>;
    template class NLineBase<genv2d_t, pt2d_t, NLineTypes::Line>;

    line2d_t::~line2d_t() {

    }
    ray2d_t::~ray2d_t() {

    }
    trueline2d_t::~trueline2d_t() {

    }
};
