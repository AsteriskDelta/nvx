#include "Point.h"

namespace nvx {
    template class NI2<40, 24, 0>;
    template class NVX2<3, geni_t>;

    template class NPolygonPtBase<genv_t, true>;
    template class NPolygonPt<genv_t, ptdata_t>;

    pt_t::~pt_t() {

    }

    template class NVX2<2, geni_t>;

    template class NPolygonPtBase<genv2d_t, true>;
    template class NPolygonPt<genv2d_t, ptdata2d_t>;

    pt2d_t::~pt2d_t() {

    }
}
