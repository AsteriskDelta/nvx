#ifndef NVX_LINE_H
#define NVX_LINE_H
#include "Point.h"
#include "NLine.h"

namespace nvx {
    class line_t : public Line<genv_t, pt_t> {
    public:
        typedef Line<genv_t, pt_t> Base;
        using Base::Base;
        virtual ~line_t();
    };
    class ray_t : public Ray<genv_t, pt_t> {
    public:
        typedef Ray<genv_t, pt_t> Base;
        using Base::Base;
        virtual ~ray_t();
    };
    class trueline_t : public TrueLine<genv_t, pt_t> {
    public:
        typedef TrueLine<genv_t, pt_t> Base;
        using Base::Base;
        virtual ~trueline_t();
    };

    //2D
    class line2d_t : public Line<genv2d_t, pt2d_t> {
    public:
        typedef Line<genv2d_t, pt2d_t> Base;
        using Base::Base;
        virtual ~line2d_t();
    };
    class ray2d_t : public Ray<genv2d_t, pt2d_t> {
    public:
        typedef Ray<genv2d_t, pt2d_t> Base;
        using Base::Base;
        virtual ~ray2d_t();
    };
    class trueline2d_t : public TrueLine<genv2d_t, pt2d_t> {
    public:
        typedef TrueLine<genv2d_t, pt2d_t> Base;
        using Base::Base;
        virtual ~trueline2d_t();
    };
};

#endif
