#ifndef NVX_POLYGON_H
#define NVX_POLYGON_H
#include "NPolygon.h"
#include "Point.h"

namespace nvx {
    class polygon_t : public NPolygon<genv_t, pt_t> {
    public:
        typedef NPolygon<genv_t, pt_t> Base;
        using Base::Base;
        virtual ~polygon_t();
    protected:
    };

    class polygon2d_t : public NPolygon<genv2d_t, pt2d_t> {
    public:
        typedef NPolygon<genv2d_t, pt2d_t> Base;
        using Base::Base;
        virtual ~polygon2d_t();
    protected:
    };
};

#endif
