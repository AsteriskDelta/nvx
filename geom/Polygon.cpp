#include "Polygon.h"
#include "NPolygon.impl.h"

namespace nvx {
    template class NPolygon<genv_t, pt_t>;

    polygon_t::~polygon_t() {

    }

    template class NPolygon<genv2d_t, pt2d_t>;

    polygon2d_t::~polygon2d_t() {

    }
}
