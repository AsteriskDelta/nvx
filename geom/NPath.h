#ifndef NVX_NPATH_H
#define NVX_NPATH_H
#include "NPolygon.h"

namespace nvx {
    template<typename V, typename PT>
    class NPath : public NPolygon<V,PT> {
    public:
        typedef NPolygon<V,PT> Base;
        typedef NPath<V,PT> Self;

        inline NPath() : Base() {
            this->bounds.setType(Bounds<V>::Square);
            this->isClosed = false;
        };

        inline NPath(const Self& orig, bool limited = false) : Base(orig, limited) {//Copy all polys on next
            this->isClosed = false;
        };

        inline NPath(const typename Base::Idx& sz) : NPath() {
            this->points.resize(sz);
            this->isClosed = false;
        }
        inline virtual ~NPath() {

        }
    };
};

#endif
