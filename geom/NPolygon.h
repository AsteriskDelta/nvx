#ifndef NPOLYGON_H
#define NPOLYGON_H
#include <vector>
#include <list>
#include "Bounds.h"
#include "NLine.h"

//#define NVXP_DEBUG
#if defined(NVX_DEBUG) || defined(NVXP_DEBUG)
#define NVXP_DBG(...) __VA_ARGS__
#else
#define NVXP_DBG(...)
#endif

namespace nvx {
    namespace csg {
        enum types {
            Add = 0, Union = 0,
            Sub = 1, Subtract = 1,
            Xor = 2, Difference = 2,
            And = 3, Intersection = 3
        };
    };

inline int arke_poly_nmod(int k, int n) {
    if(n == 0) return 1;
    else return ((k %= n) < 0) ? k+n : k;
}

class NPolygonProto {
public:
    //template<typename T>
    //T* sibling(const T *&pt, const int& delta);
};

template<typename V, typename PT> class NPolygon;
template<typename V, typename PT> class NPolygonList;
template<typename V, typename PT> class NPolygonSegments;

template<typename PT_V2, bool HAS_NORMALS> class NPolygonPtBaseData;
template<typename PT_V2> class NPolygonPtBaseData<PT_V2, true> : public PT_V2 {
public:
    typedef NPolygonPtBaseData<PT_V2, true> Self;

    PT_V2 norm;
    inline NPolygonPtBaseData<PT_V2, true>() : PT_V2(), norm(0) {};
    inline NPolygonPtBaseData<PT_V2, true>(const PT_V2& p, const PT_V2& n = PT_V2(0)) : PT_V2(p), norm(n) {};
    inline const PT_V2& normal() const {
        return norm;
    };
    inline PT_V2& normal() {
        return const_cast<PT_V2&> (const_cast<const Self*>(this)->normal());
    };
};
template<typename PT_V2> class NPolygonPtBaseData<PT_V2, false> : public PT_V2 {
public:
    inline NPolygonPtBaseData<PT_V2, false>() : PT_V2() {};
    inline NPolygonPtBaseData<PT_V2, false>(const PT_V2& p) : PT_V2(p) {};

    static const PT_V2 fakeNormal;
    inline const PT_V2& normal() const {
        return fakeNormal;
    };
};
template<typename PT_V2> const PT_V2 NPolygonPtBaseData<PT_V2, false>::fakeNormal = PT_V2(0);

template<typename PT_V2, bool HAS_NORMALS=false>
class NPolygonPtBase : public NPolygonPtBaseData<PT_V2, HAS_NORMALS> {
public:
    typedef NPolygonPtBaseData<PT_V2, HAS_NORMALS> Dat;
    typedef NPolygonPtBase<PT_V2, HAS_NORMALS> Self;

    inline NPolygonPtBase() : Dat() {};
    inline NPolygonPtBase(const PT_V2& p) : Dat(p) {};
    template<typename VF_T = PT_V2>
    inline NPolygonPtBase(const VF_T& p, const VF_T& n) : Dat(p, n) {
        static_assert(HAS_NORMALS, "PolygonPt constructed with normals, but type doesn't have them!");
    }

    inline const PT_V2& position() const {
        return *((const PT_V2*)this);
    };
    inline PT_V2& position() {
        return const_cast<PT_V2&>(const_cast<const Self*>(this)->position());
    };
    inline static constexpr bool HasNormals() { return HAS_NORMALS; };
};

struct NPolygonPt_Root {};

template<typename PT_V, typename BASE> class NPolygonPt : public BASE, public NPolygonPt_Root {
    public:
    typedef NPolygonPt<PT_V, BASE> Self;
    typedef BASE Base;

    //NPolygonProto* parentPtr;

    /*inline NPolygonProto* parent() const {
        return parentPtr;
    };
    inline void setParent(NPolygonProto* par) {
        parentPtr = par;
    };
*/
    template<typename S> inline Self* sibling(S *const parent, int delta) const {
        return parent->sibling(this, delta);
    }
    template<typename S> inline Self* right(S *const parent, int del = 1) const {
        return this->sibling<S>(parent, del);
    };
    template<typename S> inline Self* left(S *const parent, int del = 1) const {
        return this->sibling<S>(parent, -del);
    };

    inline NPolygonPt() : Base() {};
    inline NPolygonPt(const Self& o) : Base(o) {};
    inline NPolygonPt(const PT_V& p) : Base(p) {};
    inline NPolygonPt(const PT_V& p, const PT_V& n) : Base(p, n){};
    inline static constexpr bool HasNormals() { return Base::HasNormals(); };
};
//Fake list type for interop

template<typename V, typename PT>
class NPolygon : public NPolygonProto {
    friend class NPolygonSegments<V,PT>;
    friend class NPolygonList<V,PT>;
public:
    typedef V Coord;
    typedef PT Point;
    typedef typename ::nvx::base_value<V>::type Value;
    typedef typename Value::HighpRow HighpValue;

    typedef NPolygon<V , PT> Self;
    typedef typename std::vector<Point>::iterator iterator;
    typedef typename std::vector<Point>::reverse_iterator riterator;

    typedef NPolygonList<V,PT> List;
    typedef NPolygonList<typename V::Raw, PT> RawList;
    typedef NPolygonSegments<V,PT> Segments;
    typedef ::nvx::PolySegment<V,PT&> Segment;


    typedef unsigned int Idx;
    typedef Idx size_type;

    std::vector<Point> points;
    Self *prev, *next;
    Bounds<V > bounds;

    inline NPolygon() : points(), prev(nullptr), next(nullptr), bounds(),
    accumulatedCenter(0), trueCenter(0),
    trueArea(0.f), rawSegments(this), isClosed(true) {
        bounds.setType(Bounds<V>::Square);
    };

    inline NPolygon(const Self& orig, bool limited = false) : points(orig.points), prev(nullptr), next(nullptr), bounds(orig.bounds),
    accumulatedCenter(orig.accumulatedCenter), trueCenter(orig.trueCenter), trueArea(orig.trueArea), rawSegments(this), isClosed(true) {//Copy all polys on next
        //for (Point& pt : points) pt.setParent(this);
        if (!limited && orig.next != nullptr) next = new Self(*(orig.next));
    };

    inline NPolygon(const Idx& sz) : NPolygon() {
        points.resize(sz);
    }

    inline virtual ~NPolygon() {
        if (next != nullptr) delete next;
    }

    inline bool closed() const {
        return this->isClosed;
    }

    inline Point& get(const unsigned int& idx) {
        return const_cast<Point&>(((const Self*)this)->get(idx));
    };
    inline const Point& get(unsigned int idx) const {
        idx = arke_poly_nmod(idx, this->size());
        return points[idx];
    };
    inline V& at(const unsigned int& idx) {
        return const_cast<V&>(this->get(idx).position());
    };
    inline const V& at(unsigned int idx) const {
        return this->get(idx).position();
    };

    inline V& operator[](const unsigned int& idx) {
        return this->at(idx);
    }
    inline const V& operator[](const unsigned int& idx) const {
        return this->at(idx);
    }

    inline unsigned int index(const Point *pt) const {
        return ((unsigned int) (((const char*) pt) - ((const char*) (&points[0]))) / sizeof (Point));
    }

    inline unsigned int size() const {
        return points.size();
    };
    inline bool empty() const { return this->size() == 0; };
    inline void resize(unsigned int newSz) {
        if(newSz == points.size()) return;
        points.resize(newSz);
    }
    inline void reserve(unsigned int rsz) {
        points.reserve(rsz);
    }
    inline void clear() {
        *this = Self();
    }

    inline void add(const V & pt) {
        bounds.add(pt);
        points.emplace_back(Point(pt));
        accumulatedCenter += pt;
    };
    inline void add(const PT& pt) {
        points.push_back(pt);
        bounds.add(pt.position());
        accumulatedCenter += pt.position();
        //points.rbegin()->setParent(this);
    };
    inline void push_back(const V& pt) { return this->add(pt); };
    inline void push_back(const PT& pt) { return this->add(pt); };

    inline Point* sibling(const Point *pt, int delta) {
        const int idx = (int)this->index(pt);
        if(!this->closed() && (idx + delta < 0 || idx + delta >= int(this->size()))) return nullptr;
        else return const_cast<Point*> (&this->get(idx+delta));
    }

    inline Self* root() const {
        if (prev == nullptr) return const_cast<Self*> (this);
        else return prev->root();
    }
    inline Self* leaf() const {
        if (next == nullptr) return const_cast<Self*> (this);
        else return next->leaf();
    }
    inline Self* chained(const unsigned int& idx) const {
        if (next == nullptr || idx == 0) return const_cast<Self*> (this);
        else return next->chained(idx-1);
    }
    inline unsigned int rootDepth(const unsigned int& cur = 0) const {
        if (prev == nullptr) return cur;
        else return prev->rootDepth(cur+1);
    }
    inline unsigned int leafDepth(const unsigned int& cur = 0) const {
        if (next == nullptr) return cur;
        else return next->leafDepth(cur+1);
    }

    inline void chain(Self* obj) {
        if (next == nullptr) next = obj;
        else next->chain(obj);
    }
    inline void unchain(const bool& del = true) {//Deletes all chained (next) objects
        if(next != nullptr) next->unchain(del);
        if(del) delete next;
        next = nullptr;
    }

    inline Value closingDistance2() const {
        if(this->size() < 2) return 0;
        else return distance2(this->get(0).position(), this->get(-1).position());
    }
    inline Value closingDistance() const {
        return sqrt(closingDistance2());
    }

    inline Value area() const {
        return trueArea;
    }

    inline bool acute(const Idx& idx) const {
        const Coord actNorm = this->get(idx).normal();
        const Coord actDel = this->segments()[idx].del();
        if($::dot(actNorm, actDel) > 0) return true;
        else return false;
    }
    inline bool obtuse(const Idx& idx) const {
        return !this->acute(idx);
    }

    inline Coord normal(const Idx& idx) const {
        const Point& pt = this->get(idx);
        if(pt.normal() != Coord(0)) return pt.normal();
        else {
            const Coord nDel = this->segments()[idx].norm();
            return $::perp(nDel) * $::sgn($::dot(nDel, Coord(1,0)));
        }
    }

    //returns new index of min's -dir sibling
    inline int removeRange(int min, int max, int dir = 1);
    inline int remove(int idx, int nextDir = 1);//For consistency, nextDir is which sibling to return after deletion
    inline void removeLast(unsigned int cnt) {
        while (points.size() > 0 && cnt-- > 0) points.pop_back();
    }
    inline void pop_back() {
        return removeLast(1);
    }
    void removeList(const std::list<Idx>& ll);

    int insert(int base, const PT& pt, int dir = 1);
    template<typename FT=void>
    inline int insert(int base,const V & pt, int dir= 1) {
        return this->insert(base, Point(pt), dir);
    };

    inline iterator iter(int base) {return points.begin()+base;}
    inline iterator begin() { return points.begin();}
    inline iterator end() { return points.end(); }
    inline riterator riter(int base) {return points.rbegin()+(this->size() - base);}
    inline riterator rbegin() { return points.rbegin();}
    inline riterator rend() { return points.rend(); }

    inline const Segments& segments() const {
        if(rawSegments.poly != this) rawSegments.poly = const_cast<Self*>(this);
        return rawSegments;
    }
    inline List& asList() {
        if(rawList.root != this) rawList.root = this;
        return rawList;
    }
    inline const List& asList() const {
        if(rawList.root != this) rawList.root = const_cast<Self*>(this);
        return rawList;
    }
    inline RawList& asRawList() { return *reinterpret_cast<RawList*>(&(this->asList())); };
    inline const RawList& asRawList() const { return *reinterpret_cast<const RawList*>(&(this->asList())); };

    inline Self& csgSubtract(const Self *const o) {
        op(csg::Subtract, this, o);
        return *this;
    }
    inline Self& csgAdd(const Self *const o) {
        op(csg::Add, this, o);
        return *this;
    }
    inline Self& csgIntersect(const Self *const o) {
        op(csg::Intersection, this, o);
        return *this;
    }
    inline Self& csgXor(const Self *const o) {
        op(csg::Xor, this, o);
        return *this;
    }

    int insert(int base, int baseDir, const Self *const& o, int first, int last, int dir = 1);

    struct Contains_RData { short lCross = 0, rCross = 0; };
    bool contains(const V & pt, bool allowEdges = true, Contains_RData *rdata = nullptr);
    bool radialContains(const V & pt, const V & cen, bool soft = false);

    struct IntersectLine : public Line<V> {
        unsigned int nearIdx = 0, farIdx = 0;
        inline Segment near(const Self *const& p) { return p->segments()[nearIdx]; };
        inline Segment far(const Self *const& p) { return p->segments()[farIdx]; };
        inline IntersectLine() : Line<V>(V::Nil(), V::Nil()) {};
    };

    template<typename LINE_T>
    IntersectLine intersect(const LINE_T& line, const bool allowInternal = false);
    template<typename LINE_T> void intersect(const LINE_T& line, IntersectLine& ret);//For chained/recursive calls
    template<typename LINE_T>
    V intersection(const LINE_T& line);//Simplified call for points known to be outside (or on the edge of) this polygon

    void decollinearize();
    //Like simplification, but preserves internal subset (ie, only expands the polygon, never excluding previously internal points)
    void smooth(HighpValue epsilon, bool recursive = false);

    //WARNING: if deletion is not allowed, polygon normals are not promised to be well-formed
    inline void fastSimplify(HighpValue epsilon, HighpValue relEpsilon = 0.f);
    inline void fixNormals(bool allowDelete = true);
    inline void fixBounds();//Also fixes winding and parent refs
    inline void calculateArea();
    inline void repair(bool allowDelete = true);

    struct Viewpoint {
        V  start, end;
        V  axes[2] = {V (0.f), V (0.f)};
        bool ortho = false, admissable = false, bounded = false;
    };
    struct Projection {
        V  intersections[2];
        PT *points[2] = {nullptr, nullptr};
        char winding = 100;
        inline operator bool() const { return points[0] != nullptr && points[1] != nullptr; };
    };
    Projection projection(const Viewpoint& view);
    void projectOnto(const Viewpoint& view);

    inline V centroid() const {
        return trueCenter;
    }
    inline Value maxRadius() const {
        return maxRad;
    }
    inline Value minRadius() const {
        return minRad;
    }
    inline Value radius() const { return this->maxRadius(); };

    inline V accumulatedCentroid() const {
        if(this->size() == 0) return V ();
        else return this->accumulatedCenter / Value(this->size());
    }

    static int op(int opType, Self*const a, const Self *const b);

    static Self* opnew(int opType, const Self *const a, const Self *const b);
protected:
    V  accumulatedCenter, trueCenter;
    Value trueArea, minRad, maxRad;
    union {
    mutable Segments rawSegments;
    mutable List rawList;
    };

    bool isClosed;
};

template<typename V, typename PT>
class NPolygonSegments {
public:
    typedef NPolygon<V, PT> Poly;
    typedef NPolygonSegments<V,PT> Self;
    typedef typename Poly::Segment Segment;

    mutable Poly *poly;
    inline NPolygonSegments(Poly *p) : poly(const_cast<Poly*>(p)) {};

    inline const Segment operator[](const int& idx) const {
        //std::cout << "got parent = " << poly<< "\n";
        return Segment(poly->get(idx), poly->get(idx+1));
    }

    struct iterator {
        const Self& parent;
        int idx;
        inline iterator(const Self& par, const int& i = 0) : parent(par), idx(i) {};
        inline iterator& operator++() { idx++; return *this; };
        inline const Segment operator*() { return parent[idx]; };
        inline const Segment* operator->() { return &parent[idx]; };

        inline bool operator==(iterator o) { return idx == o.idx; };
        inline bool operator!=(iterator o) {
            //std::cout << "\t\titer ncmp with " << idx << " and " << o.idx << ", pars=" << parent.poly << ", " << o.parent.poly << "\n";
            return idx != o.idx;
        };
    };

    iterator begin() const {
        //std::cout << "returning (begin) segment iter on " << poly << " at idx=" << 0 << "\n";
        return iterator(*this, 0);
    }
    iterator end() const {
        //std::cout << "returning (end) segment iter on " << poly << " sz=" << poly->size() << " at idx=" << ::max(0, int(poly->size())) << "\n";
        if(!poly->closed()) return iterator(*this, ::std::max(0, int(poly->size()) - 1));
        else return iterator(*this, ::std::max(0, int(poly->size())));
    }
};

/*template<typename T, typename PT_T>
inline typename NPolygon<T, PT_T>::Segments NPolygon<T, PT_T>::segments() const {
    return NPolygonSegments<T,PT_T>(*const_cast<Self*>(this));
}*/

//Proxy class for libraries requiring std list/vector functionality
template<typename V, typename PT>
class NPolygonList {
public:
    typedef unsigned int Idx;
    typedef Idx size_type;
    typedef NPolygon<V,PT> Poly;
    typedef NPolygonList<V,PT> Self;

    Poly *root;

    inline NPolygonList() : root(nullptr) {};
    inline NPolygonList(Poly& pl) : root(&pl) {};
    /*inline NPolygonList(const Self& o) {

    };*/
    inline operator NPolygon<V,PT>&() {
        return *root;
    }
    inline Poly* poly() {
        return root;
    }
    inline const Poly* poly() const {
        return root;
    }

    inline void reserve(unsigned int sz) const {
        ((void)sz);
    }
    inline unsigned int size() const {
        if(root == nullptr) return 0;
        else return root->leafDepth() + 1;
    }
    inline void clear() {
        if(root == nullptr) return;
        else root->unchain();
    }
    inline void resize(const Idx& sz) {
        if(root == nullptr || sz >= this->size()) return;
        else if(sz == 0) return this->clear();
        else root->chained(sz-1)->unchain();
    }

    inline void push_back(const NPolygon<V,PT>& poly) {
        NPolygon<V,PT>* copiedPoly = new NPolygon<V,PT>(poly);
        if(root == nullptr) root = copiedPoly;
        else root->leaf()->chain(copiedPoly);
    }
    inline void push_back(NPolygon<V,PT> *const& poly) {
        if(root == nullptr) root = poly;
        else root->leaf()->chain(poly);
    }
    inline void pop_back() {
        if(root == nullptr || root->next == nullptr) return;
        else root->leaf()->prev->unchain();
    }
    inline void pop_front() {
        if(root == nullptr) return;

        Poly *const oldRoot = root;
        root = oldRoot->next;
        oldRoot->next = nullptr;
        delete oldRoot;
    }

    inline const NPolygon<V,PT>& operator[](const unsigned int& idx) const {
        return *root->chained(idx);
    }
    inline NPolygon<V,PT>& operator[](const unsigned int& idx) {
        return *root->chained(idx);
    }

};


template<typename T, typename PT_T>
template<typename LT>
inline void NPolygon<T, PT_T>::intersect(const LT& line, typename NPolygon<T, PT_T>::IntersectLine &ret) {
    NVXP_DBG(std::cout << "begin intersection on, this->size() = " << this->size() << " at addr=" << this << ":\n");
    NVXP_DBG(std::cout << "seg size=" << this->segments().poly->size() << " at seg addr=" << this->segments().poly << "\n");
    NVXP_DBG(std::cout << "\t"<<line<<"\n");
    //const auto lineDel = line.del();
    for(auto it = this->segments().begin(); it != this->segments().end(); ++it) {
        const Segment& seg = *it;
        //std::cout << "\twith seg " << seg << "\n";
        const auto inter = seg.intersect(line);
        //std::cout << "\t\tfor " << inter << "\n";
        NVXP_DBG(std::cout << "TESTING SEG=" << seg << ", nil would be " << decltype(inter)::Nil() << ", which (bool) is " << decltype(inter)::Nil().isNil() << "\n ");
        if(!inter) {
            NVXP_DBG(std::cout << "\t\tno intersection with " << seg << "\n");
            continue;
        }

        NVXP_DBG(std::cout << "\t\tpoly crossing to " << seg << " at " << inter << "\n");
        if(ret.p1.isNil() || line.rawInverse(ret.p1) > line.rawInverse(inter)) {//$::distance2(ret.p1, line.start()) > $::distance2(inter, line.start())) {
            ret.p1 = inter;
            ret.nearIdx = it.idx;
        }
        if(ret.p2.isNil() || line.rawInverse(ret.p2) < line.rawInverse(inter)) {
            ret.p2 = inter;
            ret.farIdx = it.idx;
        }
    }
}

template<typename T, typename PT_T>
template<typename LT>
inline typename NPolygon<T, PT_T>::IntersectLine NPolygon<T, PT_T>::intersect(const LT& line, const bool allowInternal) {
    NVXP_DBG(std::cout << "\nBEGIN INTERSECT with " << line << " on poly=" << this << "\n");
    IntersectLine ret;
    if(this->size() < 2) return ret;
    else if(!allowInternal && this->contains(line.start())) {
        ret.p1 = line.start(); ret.p2 = line.start();
        return ret;
    }
    this->intersect(line, ret);
    NVXP_DBG(std::cout << "END INTERSECT\n\n");
    return ret;
}

template<typename T, typename PT_T>
template<typename LT>
inline T NPolygon<T, PT_T>::intersection(const LT& line) {
    return this->intersect(line, false).p1;
}

};
/*
#include <iostream>
template<typename genType>
typename std::enable_if<std::is_base_of<nvx::NPolygonPt_Root, genType>::value, std::ostream&>::type
inline operator<<(std::ostream& out, const genType& g) {
    return out << "nPT(" << g.position() << ")";
}*/

#endif /* NPOLYGON_H */
