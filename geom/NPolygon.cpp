#include "NPolygon.h"
#include "NVX.h"
#include "NPolygon.impl.h"
/*
template class NPolygon<N2, 
        NPolygonPt<N2, 
        NPolygonPtBase<N2, true>
>>;

template class NPolygon<N2Meta, 
        NPolygonPt<N2Meta, 
        NPolygonPtBase<N2Meta, false>
>>;

#define CLIPPER_IMPL

#define CLIPPER_PT_T N2Meta
#define CLIPPER_POLY_T  N2Meta_Poly
#define CLIPPER_MULTIPOLY_T std::vector<CLIPPER_POLY_T>
 */

//#define CLIPPER_IMPL
//#include "NPolygon.clip.h"

/*
 Point* sibling(const Point *pt, int delta) {
        unsigned int idx = this->index(pt);
        while (delta != 0) {
            if (delta > 0) {
                if (pt->flags.connectRight) idx = (idx + 1) % this->size();
                else return nullptr;
                delta--;
            } else {
                if (pt->flags.connectLeft) idx = std::min(this->size() - 1, idx - 1);
                else return nullptr;

                delta++;
            }

            pt = this->get(idx);
        }

        return const_cast<Point*> (pt);
    }
 
 inline float closingDistance() const {
        if (points.size() < 2) return 0.f;

        return glm::distance(points.begin()->positionConst(), points.rbegin()->positionConst());
    }
 */