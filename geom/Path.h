#ifndef NVX_PATH_H
#define NVX_PATH_H
#include "NPath.h"
#include "Point.h"

namespace nvx {
    class path_t : public NPath<genv_t, pt_t> {
    public:
        typedef NPath<genv_t, pt_t> Base;
        using Base::Base;
        virtual ~path_t();
    protected:
    };

    class path2d_t : public NPath<genv2d_t, pt2d_t> {
    public:
        typedef NPath<genv2d_t, pt2d_t> Base;
        using Base::Base;
        virtual ~path2d_t();
    protected:
    };
};

#endif
