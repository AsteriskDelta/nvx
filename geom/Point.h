#ifndef NVX_POINT_H
#define NVX_POINT_H
#define NVX_STR_CASTS
#include "NVX.h"
#include "NPolygon.h"

 namespace nvx {
    typedef NI<22, 10, 0> geni_t;
    typedef NVX2<3, geni_t> genv_t;

    struct ptdata_t : public NPolygonPtBase<genv_t, true> {
        typedef NPolygonPtBase<genv_t, true> DataBaseClass;
        using DataBaseClass::DataBaseClass;
    };

    class pt_t : public NPolygonPt<genv_t, ptdata_t> {
    public:
        typedef NPolygonPt<genv_t, ptdata_t> Base;
        typedef pt_t Self;
        using Base::Base;
        virtual ~pt_t();

        inline operator const genv_t&() const {
            return Base::position();
        }
        inline operator const geni_t&() const {
            return Base::position()[0];
        }

        template<typename S> inline Self* sibling(S *const parent, int delta) const {
            return parent->sibling(this, delta);
        }
        template<typename S> inline Self* right(S *const parent, int del = 1) const {
            return this->sibling<S>(parent, del);
        };
        template<typename S> inline Self* left(S *const parent, int del = 1) const {
            return this->sibling<S>(parent, -del);
        };
    };

    //2D
    typedef NVX2<2, geni_t> genv2d_t;

    struct ptdata2d_t : public NPolygonPtBase<genv2d_t, true> {
        typedef NPolygonPtBase<genv2d_t, true> DataBaseClass;
        using DataBaseClass::DataBaseClass;
    };

    class pt2d_t : public NPolygonPt<genv2d_t, ptdata2d_t> {
    public:
        typedef NPolygonPt<genv2d_t, ptdata2d_t> Base;
        typedef pt2d_t Self;
        using Base::Base;
        virtual ~pt2d_t();

        inline operator const genv2d_t&() const {
            return Base::position();
        }
        inline operator const geni_t&() const {
            return Base::position()[0];
        }

        template<typename S> inline Self* sibling(S *const parent, int delta) const {
            return parent->sibling(this, delta);
        }
        template<typename S> inline Self* right(S *const parent, int del = 1) const {
            return this->sibling<S>(parent, del);
        };
        template<typename S> inline Self* left(S *const parent, int del = 1) const {
            return this->sibling<S>(parent, -del);
        };
    };
};

#endif
