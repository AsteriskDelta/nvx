#pragma once
#include <iostream>
#include <vector>
typedef __int128 cint;
typedef __uint128_t ucint;
typedef std::vector<cint> clist;
#define CLL(x) static_cast<__int128>(x)
namespace nvx {
//External code for printing 128-bit+ ints
inline std::ostream& operator<<( std::ostream& dest, __int128_t value ){
    std::ostream::sentry s( dest );
    if ( s ) {
        __uint128_t tmp = value < 0 ? -value : value;
        char buffer[ 128 ];
        char* d = std::end( buffer );
        do
        { 
            -- d;
            *d = "0123456789"[ tmp % 10 ];
            tmp /= 10;
        } while ( tmp != 0 );
        if ( value < 0 ) {
            -- d;
            *d = '-';
        }
        int len = std::end( buffer ) - d;
        if ( dest.rdbuf()->sputn( d, len ) != len ) {
            dest.setstate( std::ios_base::badbit );
        }
    }
    return dest;
}

inline std::ostream& operator<<( std::ostream& dest, __uint128_t value ){
    std::ostream::sentry s( dest );
    if ( s ) {
        __uint128_t tmp = value;
        char buffer[ 128 ];
        char* d = std::end( buffer );
        do
        { 
            -- d;
            *d = "0123456789"[ tmp % 10 ];
            tmp /= 10;
        } while ( tmp != 0 );
        int len = std::end( buffer ) - d;
        if ( dest.rdbuf()->sputn( d, len ) != len ) {
            dest.setstate( std::ios_base::badbit );
        }
    }
    return dest;
}

};
