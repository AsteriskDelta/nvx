#include <Spinster/Spinster.h>
#include "NVX.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <iomanip>

#include "NVX2.core.h"
#include "NVX2.str.h"

namespace arke{};
using namespace arke;
#include <ARKE/Timer.h>

typedef nvx::NI2<22,10,0> nvxi_t;
typedef nvx::NI2<32,16,0> bnvxi_t;
typedef nvx::NVX2<3, nvxi_t> N3;
typedef nvx::NVX2<2, nvxi_t> N2;

#include "spatial/Spatial.h"

NVX_DECLARE_SPATIAL(3, nvxi_t, 3D);
NVX_DECLARE_SPATIAL(2, nvxi_t, 2D);
NVX_INSTANTIATE_SPATIAL(3, nvxi_t, 3D);
NVX_INSTANTIATE_SPATIAL(2, nvxi_t, 2D);

using namespace nvx;

class Square : public Bounded2D {
public:
    Square() {
        
    }
};

int main(int argc, char** argv) {
    _unused(argc, argv);
    
    Spin::MemberInfo myInfo;
    myInfo.type = "nvx_test";
    
    Spin::Initialize(Spinster::MemberID(5, 13432), myInfo);
    
    N2 vars[6] = {N2(-1, -3), N2(4, -1), N2(5,7), N2(0, 3), N2(2, 2), N2(-4, 5)};

    Square squares[2];
    
    for(int i = 0; i < 4; i++) squares[0].add(vars[i], Bounded2D::World);
    for(int i = 2; i < 6; i++) squares[1].add(vars[i], Bounded2D::World);
    
    std::cout << "Square 0: from " << squares[0].minimum << " to " << squares[0].maximum << " centered at " << squares[0].center(Bounded2D::World) << "\n";
    std::cout << "Square 1: from " << squares[1].minimum << " to " << squares[1].maximum << " centered at " << squares[1].center(Bounded2D::World) << "\n";
    
    std::cout << "Contains:\n";
    for(int i = 0; i < 6; i++) {
        N2& pt = vars[i];
        std::cout << "\t" << i << "?\t" << vars[i] << "\t\t" << squares[0].contains(pt) << " : " << squares[1].contains(pt) << "\n";
    }
    
    std::cout << "squares intersect? " << squares[0].intersects(squares[1]) << "\n";
    std::cout << "squares 0 contains 1? " << squares[0].contains(squares[1]) << "\n";

    return 0;
}

