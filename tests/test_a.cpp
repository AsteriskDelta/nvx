#include <Spinster/Spinster.h>
#include "NVX.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <iomanip>

#include "NVX2.core.h"
#include "NVX2.str.h"
#include "arb/Arb.h"
using namespace nvx;

namespace arke{};
using namespace arke;
#include <ARKE/Timer.h>

int main(int argc, char** argv) {
    _unused(argc, argv);
    
    Spin::MemberInfo myInfo;
    myInfo.type = "nvx_test";
    
    Spin::Initialize(Spinster::MemberID(5, 13432), myInfo);
    
    //Test out the Arb type here
    
    return 0;
}

