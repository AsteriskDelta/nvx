#include <Spinster/Spinster.h>
#include "NVX.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>

#include "NVX2.core.h"
#include "NVX2.str.h"
#include "NVX2.vec.h"
using namespace nvx;

namespace arke{};
using namespace arke;
#include <ARKE/Timer.h>

typedef NI2<22,10,0> nvxi_t;
typedef NVX2<3, nvxi_t> nvxv_t;
template class NVX2<3, nvxi_t>;

int main(int argc, char** argv) {
    _unused(argc, argv);
    
    Spin::MemberInfo myInfo;
    myInfo.type = "nvx_test";
    
    Spin::Initialize(Spinster::MemberID(5, 13432), myInfo);
    
    nvxv_t a(1039, 22, 586.f);
    nvxv_t b(105.66654861, 666, -29.5);
    std::cout << a.str() << " / " << b.str() << " = " << (a/b).str() << "\n";
    std::cout << b.str() << " / " << a.str() << " = " << (b/a).str() << "\n";
    std::cout << a.str() << " * " << b.str() << " = " << (a*b).str() << "\n";
    std::cout << b.str() << " * " << a.str() << " = " << (b*a).str() << "\n";
    //return 0;
    
    std::cout << "up x right = " << cross(nvxv_t(0,1,0), nvxv_t(1,0,0)) << "\n";
    
#ifdef DONT_DO_THIS_2
    Spin::TaskSet tasks;
    DBG_EXCL(std::cout << "Begin task set\n";);
    
    for(int i = 0; i < 1; i++) {
        tasks += Spin::Local->invoke([]() -> int{
            Timer timer;
            const unsigned int benchmarkEvals = 1024;
            const unsigned int benchmarkRepeats = 1024*256;
            
            GEN_BENCHMARK(nvxi_t, float,
                          1039.6, 1.1,
                          (a+b), (a-b)
            );
            
            GEN_BENCHMARK(nvxi_t, float,
                          1039.6, 1.00568,
                          (a*b), (a - (a*b) + b)
            );
            
            GEN_BENCHMARK(nvxi_t, float,
                          1039.6, 1.10568,
                          (a/b), (a + a)
            );
            
            GEN_BENCHMARK(nvxi_t, float,
                          1039.6, 4.10568,
                          (a/b), (a*b)
            );
            return 1;
        });
    }
    
    Spin::Yield(tasks);
#endif
    
#ifdef DONT_DO_THIS
    NI2<32, 30, 0> a(1039);
    NI2<30, 31, 0> b(105.66654861);
    decltype(a) m(1.04);
    double ad = a, bd = b;
    
    
    std::cout << "evaluating NVX ops...\n";
    
    timer.start();
    for(unsigned int i = 0; i < benchmarkEvals; i++) {
        if(i%3 == 0) a = a - b;
        else a = a + b;
    }
    timer.stop();
    
    std::cout << " a = " << a.str() << "\n";
    std::cout << "NVX computation took "<<  timer.getElapsedMicro() << "uS, for " << (timer.getElapsedMicro()*1000.0/benchmarkEvals) << "nS per op\n";
    
    std::cout << "evaluating FPU/AVX ops...\n";
    
    timer.start();
    for(unsigned int i = 0; i < benchmarkEvals; i++) {
        if(i%3 == 0) ad = ad - bd;
        else ad = ad + bd;
    }
    timer.stop();
    
    std::streamsize ssp = std::cout.precision();
    std::cout << " a = "<< std::setprecision (15) << ad << std::setprecision(ssp)<< "\n";
    std::cout << "AVX computation took "<<  timer.getElapsedMicro() << "uS, for " << (timer.getElapsedMicro()*1000.0/benchmarkEvals) << "nS per op\n";
#endif
    /*NI2<400, -304, 0> c;//96 bits of data, 304 bits of "insignifiant" accuracy
     *    NI2<420, -330, 0> d;
     *    
     *    std::cout << "A's type: " << a.typeStr() << " = " << a.str() << "\n";
     *    std::cout << "B's type: " << b.typeStr() << " = " << b.str() << "\n";
     *    //std::cout << "C's type: " << c.typeStr() << " = " << c.str() << "\n";
     *    //std::cout << "D's type: " << d.typeStr() << " = " << d.str() <<  "\n\n";
     *    
     *    std::cout << "\tA+B's intermediate: " << (a|b).typeStr() << "\n";
     *    std::cout << "A+B's type: " << (a+b).typeStr() << "\n";
     *    std::cout << "A+B = " << (a+b).str() << "\n";
     *    std::cout << "A-B = " << (a-b).str() << "\n";
     *    
     *    std::cout << "\tA+C's intermediate: " << (a|c).typeStr() << "\n";
     *    std::cout << "A+C's type: " << (a+c).typeStr() << "\n";
     *    
     *    std::cout << "\tB+C's intermediate: " << (b|c).typeStr() << "\n";
     *    std::cout << "B+C's type: " << (b+c).typeStr() << "\n";
     *    
     *    std::cout << "\tC+D's intermediate: " << (c|d).typeStr() << "\n";
     *    std::cout << "C+D's type: " << (c+d).typeStr() << "\n";
     *    
     */
    
    return 0;
}

