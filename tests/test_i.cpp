#include <Spinster/Spinster.h>
#include "NVX.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <iomanip>

#include "NVX2.core.h"
#include "NVX2.str.h"
using namespace nvx;

namespace arke{};
using namespace arke;
#include <ARKE/Timer.h>

typedef NI2<22,10,0> nvxi_t;
typedef NI2<32,16,0> bnvxi_t;

#define GEN_BENCHMARK(NVT, FPT, VA, VB, EXP1, EXP2) {\
std::cout << "evaluating NVX ops...\n";\
    double ns1, ns2;\
    {\
        timer.start(); \
            NVT a(VA);\
            NVT b(VB);\
            const NVT aReset(a);\
            for(unsigned int r = 0; r < benchmarkRepeats; r++) {\
                a = aReset;\
                for(unsigned int j = 0; j < benchmarkEvals; j++) {\
                    if(j%2 == 0) a = EXP2;\
                    else a = EXP1;\
                }\
            }\
        timer.stop();\
        \
        std::cout << " a = " << a.str() << "\n";\
        const double nsPerOp = ns1 = (timer.getElapsedMicro()*1000.0/benchmarkEvals/benchmarkRepeats);\
        const double gOps = 1.0 / nsPerOp;\
        std::cout << "NVX computation took "<<  timer.getElapsedMicro() << "uS, for " << nsPerOp << "nS per op and " << gOps << " G-OPS\n";\
    }\
    std::cout << "evaluating FPU/AVX ops...\n";\
    {\
        timer.start(); \
            FPT a(VA);\
            FPT b(VB);\
            for(unsigned int r = 0; r < benchmarkRepeats; r++) {\
                a = FPT(VA);\
                for(unsigned int j = 0; j < benchmarkEvals; j++) {\
                    if(j%2 == 0) a = EXP2;\
                    else a = EXP1;\
                }\
            }\
        timer.stop();\
        \
        std::streamsize ssp = std::cout.precision();\
        std::cout << " a = "<< std::setprecision (15) << a << std::setprecision(ssp)<< "\n";\
        const double nsPerOp = ns2 = (timer.getElapsedMicro()*1000.0/benchmarkEvals/benchmarkRepeats);\
        const double gOps = 1.0 / nsPerOp;\
        std::cout << "AVX computation took "<<  timer.getElapsedMicro() << "uS, for " << nsPerOp << "nS per op and " << gOps << " G-OPS\n";\
    }\
    std::cout << "NVX vs. AVX[" << #FPT <<"] performance increase for " << #EXP1 << " and "<< #EXP2 << ": " << (ns2/ns1 * 100) << "%\n";\
}

#define TEST_EQ(EXPR) \
{\
    double ans, diff;\
    std::cout << std::setfill (' ') << std::setw (16) << #EXPR << "\t\t";\
    {\
        auto a = na, b = nb;\
        auto c = nc, d = nd;\
        _unused(a,b,c,d);\
        ans = double(EXPR);\
        std::cout << std::setfill (' ') << std::setw (10) << ans << " (" << a.handle() << "\t\t";\
    }\
    {\
        auto a = da, b = db, c = dc, d = dd;\
        _unused(a,b,c,d);\
        diff = (EXPR) - ans;\
        std::cout << std::setfill (' ') << std::setw (10) << (EXPR) << "\t\t";\
    }\
    std::cout << std::setfill (' ') << std::setw (10) << diff << "\t\t" << ((fabs(diff) < 0.01f)? "[ OK ]" : "[FAIL]") << "\n";\
};

int main(int argc, char** argv) {
    _unused(argc, argv);

    Spin::MemberInfo myInfo;
    myInfo.type = "nvx_test";

    Spin::Initialize(Spinster::MemberID(5, 13432), myInfo);

    double da = 1039.86, db = 105.66654861, dc = -234.7, dd = 500000;;
    nvxi_t na(da);
    nvxi_t nb(db);
    bnvxi_t nc(dc);
    bnvxi_t nd(dd);
    da = double(na); db = double(nb); dc = double(nc); dd = double(nd);//Reassign for nearest accuracy (fair comparison)

    std::cout << "a = " << na << ", b = " << nb << ", c = " << nc << ", d=" << nd << "\n";

    //std::cout << a.str() << " / " << b.str() << " = " << (a/b).str() << "\n";
    //std::cout << b.str() << " / " << a.str() << " = " << (b/a).str() << "\n";
    //std::cout << a.str() << " * " << b.str() << " = " << (a*b).str() << "\n";
    //std::cout << b.str() << " * " << a.str() << " = " << (b*a).str() << "\n";
    //return 0;
    TEST_EQ(a + b);
    TEST_EQ(a - b);
    TEST_EQ(b - a);
    TEST_EQ(a + c);
    TEST_EQ(b - c);

    TEST_EQ(a * b);
    TEST_EQ(a / b);
    TEST_EQ(b / a);
    TEST_EQ(a / c);

    TEST_EQ((b * a) / c);

    TEST_EQ((a + b) / a);
    TEST_EQ((a - b) * b);

    TEST_EQ(c / a);
    TEST_EQ((-c) / b);
    TEST_EQ((a) / c);
    TEST_EQ((a) / b);
    TEST_EQ((d) / a);

    //return 0;
    Spin::TaskSet tasks;
    DBG_EXCL(std::cout << "Begin task set\n";);

    for(int i = 0; i < 32; i++) {
        tasks += Spin::Local->invoke([]() -> int{
            Timer timer;
            const unsigned int benchmarkEvals = 1024;
            const unsigned int benchmarkRepeats = 1024*256;

            GEN_BENCHMARK(nvxi_t, float,
                1039.6, 1.1,
                (a+b), (a-b)
            );

            GEN_BENCHMARK(nvxi_t, float,
                1039.6, 1.00568,
                (a*b), (a - (a*b) + b)
            );

            GEN_BENCHMARK(nvxi_t, float,
                1039.6, 1.10568,
                (a/b), (a + a)
            );

            GEN_BENCHMARK(nvxi_t, float,
                1039.6, 4.10568,
                (a/b), (a*b)
            );
            return 1;
        });
    }

    Spin::Yield(tasks);

#ifdef DONT_DO_THIS
    NI2<32, 30, 0> a(1039);
    NI2<30, 31, 0> b(105.66654861);
    decltype(a) m(1.04);
    double ad = a, bd = b;


    std::cout << "evaluating NVX ops...\n";

    timer.start();
    for(unsigned int i = 0; i < benchmarkEvals; i++) {
        if(i%3 == 0) a = a - b;
        else a = a + b;
    }
    timer.stop();

    std::cout << " a = " << a.str() << "\n";
    std::cout << "NVX computation took "<<  timer.getElapsedMicro() << "uS, for " << (timer.getElapsedMicro()*1000.0/benchmarkEvals) << "nS per op\n";

    std::cout << "evaluating FPU/AVX ops...\n";

    timer.start();
    for(unsigned int i = 0; i < benchmarkEvals; i++) {
        if(i%3 == 0) ad = ad - bd;
        else ad = ad + bd;
    }
    timer.stop();

    std::streamsize ssp = std::cout.precision();
    std::cout << " a = "<< std::setprecision (15) << ad << std::setprecision(ssp)<< "\n";
    std::cout << "AVX computation took "<<  timer.getElapsedMicro() << "uS, for " << (timer.getElapsedMicro()*1000.0/benchmarkEvals) << "nS per op\n";
#endif
    /*NI2<400, -304, 0> c;//96 bits of data, 304 bits of "insignifiant" accuracy
    NI2<420, -330, 0> d;

    std::cout << "A's type: " << a.typeStr() << " = " << a.str() << "\n";
    std::cout << "B's type: " << b.typeStr() << " = " << b.str() << "\n";
    //std::cout << "C's type: " << c.typeStr() << " = " << c.str() << "\n";
    //std::cout << "D's type: " << d.typeStr() << " = " << d.str() <<  "\n\n";

    std::cout << "\tA+B's intermediate: " << (a|b).typeStr() << "\n";
    std::cout << "A+B's type: " << (a+b).typeStr() << "\n";
    std::cout << "A+B = " << (a+b).str() << "\n";
    std::cout << "A-B = " << (a-b).str() << "\n";

    std::cout << "\tA+C's intermediate: " << (a|c).typeStr() << "\n";
    std::cout << "A+C's type: " << (a+c).typeStr() << "\n";

    std::cout << "\tB+C's intermediate: " << (b|c).typeStr() << "\n";
    std::cout << "B+C's type: " << (b+c).typeStr() << "\n";

    std::cout << "\tC+D's intermediate: " << (c|d).typeStr() << "\n";
    std::cout << "C+D's type: " << (c+d).typeStr() << "\n";

    */

    return 0;
}
