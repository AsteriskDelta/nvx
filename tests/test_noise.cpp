#include <Spinster/Spinster.h>
#include "NVX.h"
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <iomanip>

#include "NVX.h"
#include "NVX2.str.h"
#include "extra/Noise.h"
#include "extra/Noise.impl.h"
using namespace nvx;

namespace arke{};
using namespace arke;
#include <ARKE/Timer.h>

typedef NI2<22,10,0> nvxi_t;

int main(int argc, char** argv) {
    _unused(argc, argv);

    Spin::MemberInfo myInfo;
    myInfo.type = "nvx_test";
    srand(time(NULL));
    //Spin::Initialize(Spinster::MemberID(5, 13432), myInfo);

    Noise::Perlin<NVX2<2, nvxi_t>> perlin;
    perlin.setRange(0.0, 64.0, 1.0);
    perlin.setSeed(rand());
    perlin.setOctaves(16);
    perlin.setScale(0.5);

    std::cout << "Begin grid:\n";
    unsigned int sz = 32;
    for(unsigned int y = 0; y < sz; y++) {
        for(unsigned int x = 0; x < sz; x++) {
            nvxi_t val = perlin(NVX2<2, nvxi_t>(x,y));
            char chr = char(double(val)/3) + 65;
            //std::cout << (round(val*nvxi_t(100))/nvxi_t(100.0)) << "\t";
            std::cout << chr << " ";
        }
        std::cout << "\n";
    }
    std::cout << "End grid\n";

    return 0;
}
