#ifndef NVX_DEFINES_H
#define NVX_DEFINES_H

#ifdef __GNUC__
#define _pure __attribute__((pure))
#define _unroll __attribute__((optimize("unroll-loops"))) 
#define _tls __thread
#define _likely(x) (__builtin_expect(!!(x), 1))
#define _unlikely(x) (__builtin_expect(!!(x), 0))

#define _allocAfter(x) __attribute__ ((init_priority (x)))
#else
#define pure [[pure]]
#endif

//Default, precompiled types
namespace nvx {
    
};

#define NVX_GLM_INTEROP
#define NVX_STR_CASTS

#ifdef NVX_DEBUG
#include <iostream>
#define NVX_DBG(...) __VA_ARGS__
#else
#define NVX_DBG(...) 
#endif

#ifndef _falias
#define _falias(G, F) \
template <typename... Args>\
auto G(Args&&... args) -> decltype(F(std::forward<Args>(args)...)) {\
    return F(std::forward<Args>(args)...);\
}
#endif

#define NVX_PLEVEL_MULT NVX_PRECISION_MULT

#ifdef NVX_STR_CASTS
#include <string>
#include <sstream>
#include <iomanip>
#endif

#ifdef NVX_GLM_INTEROP
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#ifndef GLM_ENABLE_EXPERIMENTAL
#define GLM_ENABLE_EXPERIMENTAL
#endif
#include <glm/vec4.hpp>
#include <glm/gtx/string_cast.hpp>
namespace nvx {
    class Arb;
    
    template<typename T, glm::precision P = glm::defaultp>
    struct NVX_FAKE_GLM_SUBT : public glm::tvec2<T,P>, public glm::tvec3<T,P>, public glm::tvec4<T,P> {
    };
    struct NVX_FAKE_GLM_T : public NVX_FAKE_GLM_SUBT<float> {};
};
#define NVX_MATCH_GLM(TT) (std::is_base_of<TT, ::nvx::NVX_FAKE_GLM_T>::value)
    #ifndef NVX_NO_GLM_STREAMS
template<typename genType>
typename std::enable_if<NVX_MATCH_GLM(genType), std::ostream&>::type 
inline operator<<(std::ostream& out, const genType& g) {
    return out << ::glm::to_string(g);
}
#endif
#else 
struct NVX_FAKE_GLM_T {};
#endif

namespace nvx {
    namespace $=::nvx;
    //For scaler and vector
    struct NVX_T {};
/*template<typename T>
struct NVX_IBase {//Default NVXI structure (may be overridden for metadata stored in the object's datafield/bitfield)
    typedef NVX_IBase Self;
    static constexpr bool HasMetadata = false;
    
    T val = 0;
    inline void cloneMetadata(const NVX_IBase& o) { ((void)o); };
    inline void makeNil() {
        val = std::numeric_limits<T>::max();
    }
    inline bool isNil() const {
        return val == std::numeric_limits<T>::max();
    }
};
template<typename T, T MULT, typename BASE = NVX_IBase<T>> struct NVX_IStruct;
typedef NVX_IStruct<NVX_Default_Int, NVX_Default_Precision> NVX_Default_Type;
*/
//Class member access macros
#define NVXV_RAW_COMP_ACCESS(AXIS, ID) \
inline constexpr typename V::Type r##AXIS##AXIS () const {\
    return this->operator[](ID).raw();\
}\
template<class=void>\
inline typename V::Type& r##AXIS##AXIS##ref () {\
    static_assert(!Dat::Bitfield, "Raw references to bitfield/packed types may not be taken!");\
    return this->operator[](ID).rawref();\
}
/*
template<typename T> using NVXMask = BitMask<T>;
#define NVX_MASK_NONE() \
    static constexpr typename V::Type MaskBytes = NVXMask<typename V::Type>::All;\
    static constexpr std::array<typename V::Type, size> Masks = make_array<size>(return_given_value<typename V::Type, MaskBytes>);\
    static constexpr bool IsMasked = false;
#define NVX_MASK_BYTES(B) \
    static constexpr typename V::Type MaskBytes = NVXMask<typename V::Type>::Bytes(B);\
    static constexpr std::array<typename V::Type, size> Masks = make_array<size>(return_given_value<typename V::Type, MaskBytes>);\
    static constexpr bool IsMasked = (B != sizeof(typename V::Type));
*/
//For fixed-point multipliers and reciprocals, all constexpr
/*
template<typename TA, typename TB>
inline constexpr typename std::enable_if<
    std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value
, typename TA::Type>::type
NVX_OpMult() {
    if (TA::Multiplier == 0 || TB::Multiplier == 0 || TA::Multiplier == TB::Multiplier) return 1; //Force invariance (for external library interface)
    else if(TA::Multiplier < TB::Multiplier && TB::Multiplier % TA::Multiplier == 0) return TB::Multiplier/TA::Multiplier;
    else if(TA::Multiplier > TB::Multiplier && TA::Multiplier % TB::Multiplier == 0) return 1;//Smaller but divisible, other one will be multiplied
    else return TB::Multiplier;//Their product must be a multiple, so use this (TODO: replace with euclid LCM code?)
}
template<typename TA, typename TB>
inline constexpr typename std::enable_if<
    std::is_base_of<NVX_IRoot, TA>::value && !std::is_base_of<NVX_IRoot, TB>::value
, typename TA::Type>::type
NVX_OpMult() {
    return 1;
}
template<typename TA, typename TB>
inline constexpr typename std::enable_if<
    !std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value
, typename TB::Type>::type
NVX_OpMult() {
    if (TB::Multiplier == 0) return 1;
    else return TB::Multiplier;
}

template<typename TA, typename TB>
inline constexpr typename std::enable_if<
    std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value
, typename TA::Type>::type 
NVX_OpRecip() {
    //std::cout << "\t\tRECIP(" << TA::Multiplier << ", " << TB::Multiplier << "), ta->tb = " << NVX_OpMult<TA, TB>() << " and tb->ta = " << NVX_OpMult<TB, TA>() <<  "\n";
    return NVX_OpMult<TB, TA>();
}
template<typename TA, typename TB>
inline constexpr typename std::enable_if<
    !std::is_base_of<NVX_IRoot, TA>::value || !std::is_base_of<NVX_IRoot, TB>::value
, typename TA::Type>::type 
NVX_OpRecip() {
    return 1;
}
template<typename TA, typename TB>
inline constexpr typename TA::Type NVX_OpRecipSq() {
    //std::cout << "\t\tRECIPSQ ta<-tb = " << NVX_OpRecip<TA,TB>() << " and tb<-ta = " << NVX_OpRecip<TB,TA>() << " : ta->ta_T = " << NVX_OpMult<typename TA::Type, TA>() << " and tb->tb_T = " << NVX_OpMult<typename TB::Type, TB>() << "\n"; 
    return NVX_OpRecip<TA,TB>() * NVX_OpMult<typename TA::Type, TA>();
}
*/
}
#endif /* NVX_DEFINES_H */

