#ifndef NVX_FUNC_H
#define NVX_FUNC_H
#include <iostream>
#include "NVX2.core.h"
#include <cmath>

//Use degrees by default (better for lowp)
#define NVX_ANG_FAC (360.0/M_PI/2.0)

#define NVXV_FN(RET_T, FN) \
template<typename TA, typename TB>\
inline typename std::enable_if<std::is_base_of<NVX_T, TA>::value && !std::is_base_of<NVX_T, TB>::value, RET_T>::type \
FN (const TA a, const TB &b) {\
    return FN(a, TA(b));\
}\
template<typename TA, typename TB>\
inline typename std::enable_if<std::is_base_of<NVX_T, TB>::value && !std::is_base_of<NVX_T, TA>::value, RET_T>::type \
FN (const TA a, const TB &b) {\
    return FN(TB(a), b);\
}\
template<typename TA, typename TB>\
inline _unroll typename std::enable_if<(std::is_base_of<NVX_T, TA>::value && std::is_base_of<NVX_T, TB>::value), RET_T>::type FN

#define NVXI_FN2(RET_T, FN) \
template<typename TA, typename TB>\
inline typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value && !std::is_base_of<NVX_IRoot, TB>::value, RET_T>::type \
FN (const TA a, const TB &b) {\
    return FN(a, TA(b));\
}\
template<typename TA, typename TB>\
inline typename std::enable_if<std::is_base_of<NVX_IRoot, TB>::value && !std::is_base_of<NVX_IRoot, TA>::value, RET_T>::type \
FN (const TA a, const TB &b) {\
    return FN(TB(a), b);\
}\
template<typename TA, typename TB>\
inline _unroll typename std::enable_if<\
    (std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value) ||\
    (std::is_arithmetic<TA>::value&&std::is_arithmetic<TB>::value),\
RET_T>::type FN

#define NVXI_FN1(RET_T, FN) \
template<typename TA>\
inline _unroll typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value || std::is_arithmetic<TA>::value, RET_T>::type FN
#define NVXV_FN1(RET_T, FN) \
template<typename TA>\
inline _unroll typename std::enable_if<std::is_base_of<NVX_T, TA>::value, RET_T>::type FN

#define NVXI_LUT_FN(RET_T, FN, IDX_T, MIN, MAX, GENFUNC, IN_SCALE, OUT_SCALE) NVXI_FN1(RET_T, FN) (const TA a) {\
    /*const IDX_T idx = IDX_T(a)*/ \
    try {\
    /*std::cout << " a = " << double(a) << ", f(a) = " << GENFUNC(double(a)) << ", ret=" << RET_T(GENFUNC(double(a)) * SCALE) << "\n";*/\
        double intermediate = GENFUNC(double(a) / double(IN_SCALE)) * OUT_SCALE;\
        RET_T ret(::round(intermediate));/* * (decltype(ret)::Multiplier)) / (decltype(ret)::Multiplier));*/\
        return ret;\
    } catch(...) { return RET_T(0); };\
}

#define NVXI_FN2_VARI(FN) template<typename TA, typename TB, typename... Args, typename std::enable_if< \
        std::is_base_of<NVX_IRoot, TA>::value || std::is_base_of<NVX_IRoot, TB>::value, const void*&>::type = nullptr>\
    inline auto FN (const TA a, const TB b, const Args& ... args) {\
      return FN ( FN (a,b), args...);\
    }\

/*#define NVXV_COMPONENT_FORWARD2A(FN, ...) NVXV_FN(typename TA::template MVec<decltype(FN(typename TA::Row(), typename TB::Row()))::Multiplier>, FN) ( __VA_ARGS__ ) {\
    typename TA::template MVec<decltype(FN(typename TA::Row(), typename TB::Row()))::Multiplier> ret;\
    for(typename decltype(ret)::Idx i = 0; i < decltype(ret)::Size; i++) ret[i] = FN(a[i], b[i]);\
    return ret;\
}*/
#define NVXV_COMPONENT_FORWARD1A(FN, ...) template<typename TA, typename std::enable_if<std::is_base_of<NVX_T, TA>::value, void*>::type = nullptr>\
inline _unroll auto FN ( __VA_ARGS__ ) {\
    typedef NVX2<TA::Size, typename TA::Value> RType;\
    RType ret;\
    for(typename decltype(ret)::Idx i = 0; i < decltype(ret)::Size; i++) ret[i] = FN(a[i]);\
    return ret;\
}
#define NVXV_COMPONENT_FORWARD2A(FN, ...) template<typename TA, typename TB, typename std::enable_if< \
std::is_base_of<NVX_T, TA>::value && !std::is_base_of<NVX_T, TB>::value, void*>::type = nullptr> \
inline auto FN ( __VA_ARGS__ ) {\
    return FN(a, TA(b));\
}\
template<typename TA, typename TB, typename std::enable_if< \
!std::is_base_of<NVX_T, TA>::value && std::is_base_of<NVX_T, TB>::value, void*>::type = nullptr> \
inline auto FN ( __VA_ARGS__ ) {\
    return FN(TB(a), b);\
}\
template<typename TA, typename TB, typename std::enable_if< \
std::is_base_of<NVX_T, TA>::value && std::is_base_of<NVX_T, TB>::value, void*>::type = nullptr> \
inline auto FN( __VA_ARGS__ ) {\
    typedef NVX2<TA::Size, decltype(NI2Intermediate(typename TA::Value(),typename TB::Value()))> RType;\
    RType ret;\
    for(typename decltype(ret)::Idx i = 0; i < decltype(ret)::Size; i++) ret[i] = FN(a[i], b[i]);\
    return ret;\
}

/*NVXV_FN1(\
typename TA::template MVec<typename std::result_of<typename FN(typename TA::Row)>::type::Multiplier> \
, FN) ( __VA_ARGS__ ) {\
    typename TA::template MVec<decltype(FN(typename TA::Row()))::Multiplier> ret;\
     for(typename decltype(ret)::Idx i = 0; i < decltype(ret)::Size; i++) ret[i] = FN(a[i]);\
    return ret;\
}*/

#define NVXV_COMPONENT_FORWARD2(FN) NVXV_COMPONENT_FORWARD2A(FN, const TA a, const TB b)

#define NVXV_COMPONENT_FORWARD1(FN) NVXV_COMPONENT_FORWARD1A(FN, const TA a)
#define NVXV_COMPONENT_FORWARD1_P2(FN) NVXV_COMPONENT_FORWARD1A(FN, const TA a, const TA b)

namespace nvx {
    NVXI_FN1(TA, abs) (const TA a) {
        if(a < 0) return -a;
        else return a;
    }
    NVXI_FN1(TA, fabs) (const TA a) {
        return abs(a);
    }
    NVXI_FN1(TA, sgn) (const TA a) {
        if(a < 0) return -1;
        else if(a > 0) return 1;
        else return 0;
    }
    NVXI_FN1(TA, frac) (const TA a) {
        return a % TA(1);
    }
    NVXI_FN1(typename TA::template MRow<1>, intp) (const TA a) {
        return a - frac(a);
    }
    NVXI_FN1(typename TA::HighpRow, highp) (const TA a) {
        return static_cast<typename TA::HighpRow>(a);
    }
    NVXI_FN1(typename TA::LowpRow, lowp) (const TA a) {
        return static_cast<typename TA::LowpRow>(a);
    }

    NVXI_FN1(TA, floor) (const TA a) {
        return a - frac(a);
    }
    NVXI_FN1(TA, ceil) (const TA a) {
        if(frac(a) > 0) return a + TA(1);
        else return a;
    }
    NVXI_FN1(TA, round) (const TA a) {
        if(frac(a) > TA(0.5)) return floor(a);//< typename TA::Raw(TA::Multiplier/ 2)) return floor(a);
        else return ceil(a);
    }

    NVXI_FN2(TA, del) (const TA a, const TB b) {
        return b - a;
    }

    NVXI_FN2(TA, pow) (const TA a, const TB exp) {
        return ::pow(double(a), double(exp));
    }
    NVXI_FN1(TA, sq) (const TA a) {
        return a * a;
    }
    NVXI_FN1(TA, sqrt) (const TA a) {
        return ::sqrt(double(a));
    }
    NVXI_FN1(TA, exp) (const TA a) {
        return ::exp(double(a));
    }

    NVXI_FN1(TA, log) (const TA a) {
        return ::log(double(a));
    }
    NVXI_FN1(TA, log10) (const TA a) {
        return ::log10(double(a));
    }
    NVXI_FN1(TA, log2) (const TA a) {
        return ::log2(double(a));
    }
    NVXI_FN2(TA, log) (const TA a, const TB base) {
        return log(a) / log(base);
    }

    NVXI_FN1(TA, atan) (const TA a) {
        return ::atan(double(a));
    }
    NVXI_LUT_FN(typename TA::HighpRow, cos, TA, -M_PI, M_PI, ::cos, NVX_ANG_FAC, 1.0);
    NVXI_LUT_FN(typename TA::HighpRow, sin, TA, -M_PI, M_PI, ::sin, NVX_ANG_FAC, 1.0);
    NVXI_LUT_FN(typename TA::HighpRow, tan, TA, -M_PI/2.0, M_PI/2.0, ::tan, NVX_ANG_FAC, 1.0);
    NVXI_LUT_FN(typename TA::HighpRow, acos, TA, -1.0, 1.0, ::acos, 1.0, NVX_ANG_FAC);
    NVXI_LUT_FN(typename TA::HighpRow, asin, TA, -1.0, 1.0, ::asin, 1.0, NVX_ANG_FAC);

    NVXI_FN1(TA, erf) (TA x) {
        return std::erf(double(x));
    }

    NVXI_FN1(TA, inverf) (TA x) {
        TA tt1, tt2, lnx, sg;
        sg = (x < 0) ? TA(-1.0) : TA(1.0);

        x = (1 - x)*(1 + x);        // x = 1 - x*x;
        lnx = log(x);

        tt1 = 2/(M_PI*0.147) + TA(0.5) * lnx;
        tt2 = 1/(0.147) * lnx;

        return(sg * sqrt(-tt1 + sqrt(tt1*tt1 - tt2)));
    }

    NVXI_FN2(TA, clamp) (const TA a, const TB min, const TB max) {
        if(a < min) return min;
        else if( a > max) return max;
        else return a;
    }
    NVXI_FN2(TA, edge) (const TA a, const TB min, const TB max, const TA edgeVal = TA(0)) {
        if(a < min || a > max) return edgeVal;
        else return a;
    }
    NVXI_FN2(TA, step) (const TB edge, const TA a) {
        if(a < edge) return 0;
        else return 1;
    }
    NVXI_FN2(TA, smoothstep) (const TB eStart, const TB eEnd, const TA a) {
        if(a < eStart) return 0;
        else if(a > eEnd) return 1;
        else return (a - eStart) / (eEnd - eStart);
    }
#define NVXI_CHOOSE_RET typename choose_nvxi<TA, TB>::type
    NVXI_FN2(NVXI_CHOOSE_RET, min) (const TA a, const TB b) {
        return ::std::min(static_cast<NVXI_CHOOSE_RET>(a), static_cast<NVXI_CHOOSE_RET>(b));
    }
    NVXI_FN2_VARI(min);//Fold/Chain values of base function
    NVXI_FN2(NVXI_CHOOSE_RET, max) (const TA a, const TB b) {
        return ::std::max(static_cast<NVXI_CHOOSE_RET>(a), static_cast<NVXI_CHOOSE_RET>(b));
    }
    NVXI_FN2_VARI(max);

    NVXI_FN1(TA, mix) (const TA a, const TA b, const typename TA::HighpRow& t) {
        return t * b + (typename TA::HighpRow(1)-t)*a;
    }
    NVXI_FN1(TA, lerp) (const TA a, const TA b, const typename TA::HighpRow& t) { return mix(a,b,t); };

    NVXV_COMPONENT_FORWARD1(abs);
    NVXV_COMPONENT_FORWARD1(sgn);
    NVXV_COMPONENT_FORWARD2(min);
    NVXV_COMPONENT_FORWARD2(max);
    NVXV_COMPONENT_FORWARD1(floor);
    NVXV_COMPONENT_FORWARD1(ceil);
    NVXV_COMPONENT_FORWARD1(round);

    NVXV_COMPONENT_FORWARD2(del);
    NVXV_COMPONENT_FORWARD1(frac);
    NVXV_COMPONENT_FORWARD1(intp);

    NVXV_COMPONENT_FORWARD2(pow);
    NVXV_COMPONENT_FORWARD1(log);
    NVXV_COMPONENT_FORWARD2(log);
    NVXV_COMPONENT_FORWARD1(log10);
    NVXV_COMPONENT_FORWARD1(log2);

    //NVXV_COMPONENT_FORWARD2A(lerp, const TA a, const TB b, const typename TA::HighpVec::Row);
    //NVXV_COMPONENT_FORWARD2A(mix, const TA a, const TB b, const typename TA::HighpVec::Row);
    NVXV_FN1(TA, mix) (const TA a, const TA b, const typename TA::HighpRow &t) {
        TA ret;
        for(typename decltype(ret)::Idx i = 0; i < decltype(ret)::Size; i++) ret[i] = mix(a[i], b[i], t);
        return ret;
    }
    NVXV_FN1(TA, lerp) (const TA a, const TA b, const typename TA::HighpRow &t) {
        return mix(a,b,t);
    }
    NVXV_FN(TA, clamp) (const TA val, const TB min, const TB max) {
        TA ret;
        for(typename decltype(ret)::Idx i = 0; i < decltype(ret)::Size; i++) ret[i] = clamp(val[i], min[i], max[i]);
        return ret;
    }

    NVXV_FN1(TA, slerp) (const TA a, TA b, const typename TA::Value t) {
        auto cosom = dot(a, b);
        b *= sgn(cosom);
        cosom *= sgn(cosom);

        typename TA::Value c0, c1;
        if(cosom < decltype(c1)::Epsilon) {
            c0 = 1.0 - t;
            c1 = t;
        } else {
            auto omega = acos(cosom);
            auto sinom = sin(omega);
            c0 = sin((1 - t) * omega) / sinom;
            c1 = sin(t * omega) / sinom;
        }

        return a*c0 + b*c1;
    }

    NVXV_FN1(typename TA::Row, max) (const TA a) {
        typename TA::Row ret = a[0];
        for(typename TA::Idx i = 1; i < TA::Size; i++) ret = max(ret, a[i]);
        return ret;
    }
    NVXV_FN1(typename TA::Row, min) (const TA a) {
        typename TA::Row ret = a[0];
        for(typename TA::Idx i = 1; i < TA::Size; i++) ret = min(ret, a[i]);
        return ret;
    }
    NVXV_FN1(typename TA::HighpVec, highp) (const TA a) {
        return static_cast<typename TA::HighpVec>(a);
    }
    NVXV_FN1(typename TA::LowpVec, lowp) (const TA a) {
        return static_cast<typename TA::LowpVec>(a);
    }

    NVXV_FN(typename TA::HighpRow, dot) (const TA a, const TB b) {
        typename TA::HighpRow ret = 0;
        //std::cout << "\tBEGIN dot\n";
        for(typename TA::Idx i = 0; i < TA::Size; i++) {
            //std::cout << "\t\tadd prod " << a[i] << " * " << b[i] << " = " << (a[i]*b[i]) << "\n";
            ret += a[i] * b[i];
        }
        //std::cout << "\tdot("<<a<<", "<<b<<") = " << ret << "\n";
        return ret;
    }
    NVXV_FN(typename TA::HighpRow, compn) (const TA a, const TB b) {
        return dot(b, a);
    }

    NVXV_FN(TA, cross) (const TA a, const TB b) {
        TA ret;
        if(TA::Size == 2) {
            ret[0] =  (a[0] * b[1]);
            ret[1] = -(b[0] * a[1]);
        } else if(TA::Size >= 3) {
            ret[0] = a[1] * b[2] - a[2] * b[1];
            ret[1] = a[2] * b[1] - a[0] * b[2];
            ret[2] = a[0] * b[0] - a[1] * b[0];
        } else throw std::logic_error("Dual-vector cross product invoked on non-2D/3D space!");
        return ret;
    }
    NVXV_FN1(TA, cross) (const TA a) {
        if(TA::Size != 2) throw std::logic_error("Single-vector cross product invoked on non-2D space!");
        return TA(-a.y, a.x);
    }

    NVXV_FN1(typename TA::Row, sum) (const TA a) {
        typename TA::Row ret = 0;
        for(typename TA::Idx i = 0; i < TA::Size; i++) ret += a[i];
        return ret;
    }
    NVXV_FN1(typename TA::Row, product) (const TA a) {
        typename TA::Row ret = 1;
        for(typename TA::Idx i = 0; i < TA::Size; i++) ret *= a[i];
        return ret;
    }

    NVXV_FN(typename TA::Row, scalar_cross) (const TA a, const TB b) {
        TA tmp = cross(a,b);
        return sum(tmp);
    }
    NVXV_FN1(TA, perp) (const TA a) {
        if(TA::Size != 2) throw std::logic_error("Single-vector perp() invoked on non-2D space!");
        return TA(-a.y, a.x);
    }
    NVXV_FN(TA, align) (const TA a, const TB b) {//Aligns a to the same normal as b
        if(dot(a, b) < 0) return -a;
        else return a;
    }

    NVXV_FN1(typename TA::Row, length2) (const TA a) {
        typename TA::Row ret = 0;
        for(typename TA::Idx i = 0; i < TA::Size; i++) ret += sq(a[i]);
        return ret;
    }
    NVXV_FN(typename TA::Row, distance2) (const TA a, const TB b) {
        return length2(del(a, b));
    }
    NVXV_FN1(typename TA::HighpRow, length) (const TA a) {
        return sqrt(highp(length2(a)));
    }
    NVXV_FN(typename TA::HighpRow, distance) (const TA a, const TB b) {
        return length(del(a, b));
    }
    NVXV_FN1(typename TA::HighpVec, normalize) (const TA a) {
        //std::cout << "normalize " << a << ", len=" << length(a) << ", result=" << (a/length(a)) << "\n";
        //std::cout << "comp div " << (a.x / length(a)) << ", " << (a.y/length(a))<<"\n";
        //std::cout << "intermediate: " << a / length(a) << "\n";
        return highp(a) / length(a);
    }
    NVXV_FN(typename TA::HighpVec, delnormalize) (const TA a, const TB b) {
        return normalize(del(a, b));
    }

    //a onto b
    NVXV_FN1(typename TA::HighpRow, comp) (const TA a, const TA b) {
        return dot(b, a) / length(b);
    }
    NVXV_FN1(TA, project) (const TA a, const TA b) {
        return comp(a, b) * b;
    }
    NVXV_FN1(TA, reflect) (const TA I, const TA N) {
        return I - dot(N, I)* 2 * N;
    }
    NVXV_FN1(TA, refract) (const TA I, const TA N, const typename TA::HighpRow& eta) {
        const typename TA::HighpRow k = 1 - sq(eta) * (1 - sq(dot(N, I)));
        if(k < 0) return TA(0);
        else return eta * I - (eta * dot(N,I) + sqrt(k)) * N;
    }

    NVXV_FN1(bool, collinear) (const TA a, const TA b, const TA c) {
        const typename TA::Value d1 = distance2(a, b), d2 = distance2(b, c), d3 = distance2(a, c);
        const typename TA::Value dMax = max(d1, d2, d3);
        return dMax == (d1 + d2 + d3) - dMax;
    }
    NVXV_FN(bool, between) (const TA start, const TA end, const TB pt) {
        const typename TA::Value len = distance2(start, end), dStart = distance2(start, pt), dEnd = distance2(end, pt);
        return len == dStart + dEnd;
    }

    NVXV_FN1(typename TA::HighpRow, anglen) (const TA a, const TA b) {
        //std::cout<<"\tanglen: dot=" << dot(a,b) << ", acos=" << acos(dot(a, b)) << ", perpDot="<<dot(a, perp(b))<<", sgnMult=" << sgn(dot(a, perp(b))) << "\n";
        return acos(dot(a, b)) * sgn(dot(a, perp(b)));
    }
    NVXV_FN1(typename TA::HighpRow, angle) (TA a, TA b) {
        a = normalize(a); b = normalize(b);
        //std::cout<<"\tangle: dot=" << dot(a,b) << ", acos=" << acos(dot(a, b)) << ", perpDot="<<dot(a, perp(b))<<", sgnMult=" << sgn(dot(a, perp(b))) << ", lenA=" << length(a) << ", lenB=" <<length(b)<<"\n";
        //std::cout << "\tangle numer=" << (acos(dot(a, b)) * sgn(dot(a, perp(b))) ) << ", div1=" << (acos(dot(a, b)) * sgn(dot(a, perp(b)))/length(a)) << ", div2=" <<(acos(dot(a, b)) * sgn(dot(a, perp(b))) / length(a) / length(b)) <<"\n";
        return acos(dot(a, b)) * int(sgn(dot(a, perp(b))));// / length(a) / length(b);
    }
    NVXV_FN1(TA, angled) (const TA a) {
        return a;// / TA(NVX_ANG_FAC);
    }
    NVXV_FN1(TA, rotate) (const TA v, const typename TA::HighpRow& theta) {
         const typename TA::HighpVec rVec(cos(theta), sin(theta));
         //std::cout << "rVec = " << rVec << "\n";
         return TA(rVec.x * highp(v.x) - rVec.y * highp(v.y),
                   rVec.y * highp(v.x) + rVec.x * highp(v.y));
    }

#ifdef NVX_STR_CASTS
    template<typename genType>
    typename std::enable_if<std::is_base_of<::nvx::NVX_T, genType>::value || std::is_base_of<::nvx::NVX_IRoot, genType>::value, std::string>::type
    inline to_string(const genType& g) {
        return g.str();
    }
    template<typename genType>
    typename std::enable_if<std::is_base_of<::nvx::NVX_T, genType>::value || std::is_base_of<::nvx::NVX_IRoot, genType>::value, std::ostream&>::type
    inline operator<<(std::ostream& out, const genType& g) {
        return out << g.str();
    }
#endif
};
#ifdef NVX_STR_CASTS
//namespace nvx {
/*template<typename genType>
typename std::enable_if<std::is_base_of<::nvx::NVX_T, genType>::value, std::ostream&>::type
inline operator<<(std::ostream& out, const genType& g) {
    return out << g.str();
}*/
/*
template<typename genType>
typename std::enable_if<std::is_base_of<::nvx::NVX_IRoot, genType>::value, std::ostream&>::type
inline operator<<(std::ostream& out, const genType& g) {
    return out << g.str();//"NI(" << ((signed long long)g) << ")";;
}*/
//};
#endif
#endif /* NVX_FUNC_H */
