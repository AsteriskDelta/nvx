#ifndef NVX2_VEC_H
#define NVX2_VEC_H
#include "NVX.defines.h"
#include "NVX2.core.h"
#include "NVX2.dat.h"

#define NVXV2_RET_T NVX2<TA::Size, decltype(NI2Result(typename TA::Value(),typename TB::Value()))>

#define NVXV2_BASE_OP(OP, RET_T, RET_IDX, RET_ASSIGN, RET_DEFAULT, RET_PRESTATE) \
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<std::is_base_of<NVX_T, TA>::value && !std::is_base_of<NVX_T, TB>::value, TA>::type \
operator OP(const TA a, const TB b) {\
    return a OP TA(b);\
}\
template<typename TA, typename TB>\
inline _unroll constexpr typename std::enable_if<!std::is_base_of<NVX_T, TA>::value && std::is_base_of<NVX_T, TB>::value, TB>::type \
operator OP(const TA a, const TB b) {\
    return TB(a) OP b;\
}\
template<typename TA, typename TB>\
inline _unroll constexpr typename std::enable_if<std::is_base_of<NVX_T, TA>::value && std::is_base_of<NVX_T, TB>::value, RET_T>::type \
operator OP(const TA a, const TB b) {\
    /*static_assert(TA::D == TB::D, "Operation on vectors of non-equal sizes requested");*/\
    NVX_DBG(std::cout << "OPVEC " << a << " OP " << b<< "\n");\
    RET_T ret RET_DEFAULT;\
    for(typename TA::Idx i = 0; i < TA::D; i++) ret RET_IDX RET_ASSIGN a[i] OP b[i];\
        RET_PRESTATE;\
        return ret;\
}

#define NVXV2_ARITH_OP(C_OP) NVXV2_BASE_OP(C_OP, NVXV2_RET_T , [i], =,,)
#define NVXI_BOOL_OP(OP) NVXI_BASE_OP(OP,NVXI_BOOL_INTER_T, bool,bool,bool,,,)

#define NVXV_BOOL_OP_ALL(OP) NVXV2_BASE_OP(OP, bool,,&=, = true,)
#define NVXV_BOOL_OP_ONE(OP) NVXV2_BASE_OP(OP, bool,,|=, = false,)

#define NVX_BOOL_OP_FRIEND(OP, BASE) template<typename TA, typename TB>\
inline constexpr friend typename std::enable_if<std::is_base_of<BASE, TA>::value && std::is_base_of<BASE, TB>::value, bool>::type \
operator OP(const TA a, const TB b);

namespace nvx {
    template<int D, typename V> class NVXDat;
    
    namespace NVXType {
        enum {
            vec2 = 2, V2 = vec2,
            vec3 = 3, V3 = vec3,
            vec4 = 4, V4 = vec4,
        };
    };
    
    template<int D_ID, typename T>
    class NVX2 : public NVX2Dat<D_ID, T> {
    public:
        typedef NVX2Dat<D_ID,T> Dat;
        typedef NVX2<D_ID,T> Self;
        typedef NVX2<D_ID,T> Raw;
        typedef NVX2 GenSelf;
        
        typedef unsigned char Idx;
        typedef typename Dat::Value Value;
        typedef Value type;
        
        static constexpr unsigned int N = Dat::Size;
        static constexpr unsigned int Size = Dat::Size;
        static constexpr int VecID = D_ID;
        
        static constexpr nvxbt DataBits = Value::DataBits;
        static constexpr nvxbt MetaBits = Value::MetaBits;
        static constexpr nvxbt PointBit = Value::PointBit;
        
        static constexpr unsigned char D = Dat::Size;
        
        typedef typename Dat::Row Row;
        typedef decltype(NI2Numerator(Value(),Value())) HighpRow;
        typedef decltype(NI2Denominator(Value(),Value())) LowpRow;
        typedef NVX2<D_ID, HighpRow> HighpVec;
        typedef NVX2<D_ID, LowpRow> LowpVec;
        
        typedef T V;//For legacy NVX functions
        
        
        template<typename TB> using BestVec = NVX2<D_ID, HighpRow>;
        template<typename TB> using WorstVec = NVX2<D_ID, LowpRow>;
        
        inline static constexpr Self Nil() {
            Self ret;
            for(Idx i = 0; i < D; i++) ret.rows[i].makeNil();
            return ret;
        }
        
        template<typename TA>
        inline constexpr void set(const Idx& idx, const TA& val) { 
            Dat::rows[idx] = val; 
        };
        
        //Initialize to zero as expected by external libraries
        #ifdef NVX_ZERO_INIT
        inline _unroll constexpr NVX2() : NVX2Dat<D_ID, T>() {};
        #else
        inline _unroll constexpr NVX2() {};
        #endif
        
        inline _unroll constexpr NVX2(const GenSelf& orig) {
            for(Idx i = 0; i < D; i++) Dat::rows[i] = orig.rows[i];
        };
        /*template<typename TA>
        inline _unroll constexpr NVX2(const TA& initial, typename std::enable_if<( std::is_base_of<NVX_T, T>::value || NVX_MATCH_GLM(T)), void*>::type unu = nullptr) {
            ((void)unu);
            for(Idx i = 0; i < D; i++) Dat::rows[i].template from<T>(initial[i]);
        }*/
        
        template<typename TA, typename = typename std::enable_if<( !std::is_base_of<NVX_T, T>::value  && !NVX_MATCH_GLM(T)), Self>::type>
        inline _unroll constexpr NVX2(const TA& initial) {
            for(Idx i = 0; i < D; i++) Dat::rows[i].template from<T>(initial);
        }
        
        template<typename TA> inline constexpr typename std::enable_if<!std::is_base_of<NVX_T, TA>::value && !NVX_MATCH_GLM(TA), Idx>::type 
        argUnzipStore(const Idx i, const TA& val) {
            //static_assert(i < D, "Too many arguments supplied to NVX vector constructor!");
            if(i >= D) return i+1;//throw std::logic_error( "Too many arguments supplied to NVX vector constructor!");
            NVX_DBG(std::cout << "argstore " << val << "\n");
            Dat::rows[i].template from(val);
            return i + 1;
        }
        template<typename TA>
        inline constexpr typename std::enable_if<NVX_MATCH_GLM(TA), Idx>::type argUnzipStore(const Idx i, const TA& val) {
            for(Idx oi = 0; oi < val.length(); oi++) argUnzipStore(i + oi, val[oi]);
            return i + val.length();
        }
        template<typename TA>
        inline constexpr typename std::enable_if<std::is_base_of<NVX_T, TA>::value, Idx>::type argUnzipStore(const Idx i, const TA& val) {
            for(typename TA::Idx oi = 0; oi < TA::D; oi++) argUnzipStore(i + oi, val[oi]);
            return i + TA::D;
        }
        template<typename TA>
        inline constexpr void argUnzip(const Idx i, const TA& val) {
            argUnzipStore(i, val);
        }
        template<typename TA, typename ...Args>
        inline constexpr void argUnzip(const Idx i, const TA& val, const Args&... args) {
            argUnzip(argUnzipStore(i, val), args...);
        }
        
        template< typename ...Args>
        inline constexpr _unroll NVX2( const Args& ...args) {
            this->argUnzip(0, args...);
        }
        
        template<typename ...Args>
        inline constexpr Self& operator=(const Args& ...args) {
            Self tmp(args...);
            std::swap(*this, tmp);
            return *this;
        }
        
        inline constexpr const Row& operator[](const Idx& idx) const {
            return const_cast<const Row&>(Dat::rows[idx]);
        }
        inline constexpr Row& operator[](const Idx& idx) {
            return const_cast<Row&>(const_cast<const Self*>(this)->operator[](idx));
        }
        
        template<typename TO_T> inline constexpr typename std::enable_if<!std::is_base_of<NVX_IRoot, TO_T>::value, TO_T>::type
        get(const Idx& idx) const {
            return this->operator[](idx).template to<TO_T>();
        }
        //Non-const and const
        template<typename TO_T> inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TO_T>::value, TO_T>::type
        get(const Idx& idx) {
            return this->operator[](idx);
        }
        template<typename TO_T> inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TO_T>::value, TO_T>::type
        get(const Idx& idx) const {
            return this->operator[](idx);
        }
        
        inline bool isNil() const {
            return Dat::rows[0].isNil();
        }
        inline explicit operator bool() const {
            return !this->isNil();
        }
        
#ifdef NVX_STR_CASTS
        inline std::string str(bool isRaw = false) const {
            std::stringstream ss; 
            if(D == D_ID) ss << "NV";
            else ss << "NV"<<D_ID<<":";
            ss << D;
            //if(Multiplier == 0) ss << "raw";
            ss << "(";
            
            if(this->isNil()) {
                ss << "nil";
            } else if(isRaw) {
                ss << (*this)[0].handle();;//this->get<const V&>(0).raw();
                for(Idx i = 1; i < D; i++) ss << ", " << (*this)[i].handle();//this->get(i);
            } else {
                ss << (*this)[0].str();
                for(Idx i = 1; i < D; i++) ss << ", " << (*this)[i].str();
            }
            //if(Dat::extDebug() != 0) ss << " : " << Dat::extDebug();
            ss << ")";
            return ss.str();
        }
        inline operator std::string() const {
            return str();
        }
        
        template<typename genType>
        typename std::enable_if<std::is_base_of<::nvx::NVX_IRoot, genType>::value, std::string>::type 
        inline to_string(const genType& g) {
            return g.str();
        }
#endif
        
        inline constexpr Self operator-() const {
            Self ret(*this);
            for(Idx i = 0; i < D; i++) ret[i] = -((*this)[i]);
            return ret;
        }
        
        NVX_OPEQ_OP(+);
        NVX_OPEQ_OP(-);
        NVX_OPEQ_OP(*);
        NVX_OPEQ_OP(/);
        NVX_OPEQ_OP(%);
        
        NVX_BOOL_OP_FRIEND(<,  NVX_T);
        NVX_BOOL_OP_FRIEND(>,  NVX_T);
        NVX_BOOL_OP_FRIEND(<=, NVX_T);
        NVX_BOOL_OP_FRIEND(>=, NVX_T);
        NVX_BOOL_OP_FRIEND(==, NVX_T);
        NVX_BOOL_OP_FRIEND(!=, NVX_T);
        
        #ifdef NVX_GLM_INTEROP
        inline _unroll constexpr operator typename Dat::FPVec() const {
            typename Dat::FPVec ret;
            for(Idx i = 0; i < D; i++) ret[i] = this->get<float>(i);
            return ret;
        }
        #endif
        
        //Swizzles
        //NVXV_SWIZZLES(2, x, y);
        //NVXV_SWIZZLES(3, x, y, z);
        //NVXV_SWIZZLES(4, x, y, z, w);
    protected:
    };
    
    NVXV2_ARITH_OP(+);
    NVXV2_ARITH_OP(-);
    NVXV2_ARITH_OP(*);
    NVXV2_ARITH_OP(/);
    NVXV2_ARITH_OP(%);
    
    NVXV_BOOL_OP_ALL(==);
    NVXV_BOOL_OP_ONE(!=);
    NVXV_BOOL_OP_ALL(<=);
    NVXV_BOOL_OP_ALL(>=);
    NVXV_BOOL_OP_ALL(<);
    NVXV_BOOL_OP_ALL(>);
    
    template<int D, typename T> 
    using NVX =NVX2<D, T>;
};

#endif
