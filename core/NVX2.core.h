#ifndef NVX2_CORE_H
#define NVX2_CORE_H
#define ARKEBASE_EXTERNAL
#include <typeinfo>
#include <string>
#include <cstdint>
#include <cstring>
#include "NVX.defines.h"
#include "fmnt/int128.h"

#define NVX2_RETURNS(XX) typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value, XX>::type
#define NVX2_CONV_RETURNS(XX) typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value || std::is_base_of<NVX_IRoot, TB>::value, XX>::type
#define NVX2_IDEN_RETURNS(XX) typename std::enable_if<std::is_base_of<NVX_IRoot, T>::value, XX>::type
#define NVX2_CONV_IDEN_RETURNS(XX) typename std::enable_if<!std::is_base_of<NVX_IRoot, T>::value, Self>::type

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshift-count-overflow"

#define NVX2_DBG(...)
#define NVX2_RDBG(...) __VA_ARGS__

#define NVX_PPCAT_NX(A, B) A ## B
#define NVX_OPEQ_OP(OP) template<typename TARG_T> inline constexpr Self& operator NVX_PPCAT_NX(OP,=)(const TARG_T& o) {\
    return ((*this) = (*this) OP o);\
}

template<typename T>
inline constexpr T _constexpr_lshift(const T val, const int cnt) {
    return (cnt == 0)? val*2 : _constexpr_lshift(val * 2, cnt - 1);
}
template<typename T>
inline constexpr T _constexpr_rshift(const T val, const int cnt) {
    return (cnt == 0)? val/2 : _constexpr_rshift(val / 2, cnt - 1);
}

template<typename T>
inline constexpr T _constexpr_exp2(const T exponent) {
    return _constexpr_lshift(static_cast<T>(1), exponent);
}

namespace nvx {
    struct NVX_IRoot {};

    typedef int16_t nvxbt;
    typedef uint16_t unvxbt;

    constexpr unvxbt _ct_log2(unvxbt n) {
        return ( (n<2) ? 1 : 1+::log2(n/2));
    }
    constexpr unvxbt _ct_alignment(unvxbt sz) {
        return std::min(static_cast<unvxbt>(16),
                        static_cast<unvxbt>(static_cast<unvxbt>(1) << (_ct_log2(sz == 0? 0 : sz - 1) + 1))
                );
    }

    template<nvxbt D, nvxbt M>
    struct Container : public NVX_IRoot {
        constexpr inline static nvxbt Bits = D + M;
        constexpr inline static nvxbt Bytes = ((Bits/8) *8) < Bits? (Bits/8 + 1) : (Bits/8);
        constexpr inline static nvxbt DataBits = D;
        constexpr inline static nvxbt MetaBits = M;// + (Bytes * 8) - Bits;
        constexpr inline static nvxbt UnusedBits = (Bytes * 8) - Bits;
        constexpr inline static unvxbt Alignment = _ct_alignment(Bytes);

        //In order to support link-time optimization, force padded allocation
        alignas(Alignment) uint8_t data[/*Bytes*/Alignment] = {};

        Container() {
            memset(data, 0x0, Alignment);
        }

        static constexpr bool HasMetadata = MetaBits > 0;

        inline void cloneMetadata(const Container& o) { ((void)o); };
        inline _unroll void makeNil() {
            for(nvxbt i = 0; i < Bytes; i++) data[i] = 0xFF;
        }
        inline _unroll bool isNil() const {
            bool ret = true;
            for(nvxbt i = 0; i < Bytes; i++) ret &= (data[i] == 0xFF);
            return ret;
        }
    };

    template<nvxbt D, nvxbt P, nvxbt M = 0> class NI2;

    template<nvxbt D1, nvxbt D2, nvxbt P1, nvxbt P2, nvxbt M1, nvxbt M2>
    inline auto NI2Result(const NI2<D1,P1,M1> &a, const NI2<D2,P2,M2> &b) {
        ((void)a); ((void)b);
        constexpr static nvxbt RD = std::max(D1, D2);
        constexpr static nvxbt RP = std::min(P1, P2);
        constexpr static nvxbt RM = std::max(M1, M2);
        return NI2<RD,RP,RM>();
    }

    template<nvxbt D1, nvxbt P1, nvxbt M1, typename OT>
    inline auto NI2Result(const NI2<D1,P1,M1> &a, const OT &b) {
        ((void)a);((void)b);
        return NI2<D1,P1,M1>();
    }

    template<nvxbt D1, nvxbt D2, nvxbt P1, nvxbt P2, nvxbt M1, nvxbt M2>
    inline auto NI2Intermediate(const NI2<D1,P1,M1> &a, const NI2<D2,P2,M2> &b) {
        ((void)a); ((void)b);
        constexpr static nvxbt RP = std::max(P1, P2);
        constexpr static nvxbt RD = std::max(D1, D2);//std::max(D1 + (RP - P1), D2 + (RP - P2));
        constexpr static nvxbt RM = std::max(M1, M2);
        return NI2<RD,RP,RM>();
    }
    template<nvxbt D1, nvxbt P1, nvxbt M1, typename OT>
    inline auto NI2Intermediate(const NI2<D1,P1,M1> &a, const OT &b) {
        ((void)a); ((void)b);
        return NI2<D1,P1,M1>();
    }
    template<typename OT, nvxbt D1, nvxbt P1, nvxbt M1>
    inline auto NI2Intermediate(const OT &a, const NI2<D1,P1,M1> &b) {
        ((void)a); ((void)b);
        return NI2<D1,P1,M1>();
    }

    template<nvxbt D1, nvxbt D2, nvxbt P1, nvxbt P2, nvxbt M1, nvxbt M2>
    inline auto NI2Denominator(const NI2<D1,P1,M1> &a, const NI2<D2,P2,M2> &b) {
        ((void)a); ((void)b);
        constexpr static nvxbt RP = P1 + P2;//std::max(P1, P2);
        constexpr static nvxbt RD = std::max(D1, D2);//D1 + D2;//std::max(D1+P1, D2+P2);
        constexpr static nvxbt RM = std::max(M1, M2);
        return NI2<RD,RP,RM>();
    }
    template<nvxbt D1, nvxbt D2, nvxbt P1, nvxbt P2, nvxbt M1, nvxbt M2>
    inline auto NI2Numerator(const NI2<D1,P1,M1> &a, const NI2<D2,P2,M2> &b) {
        ((void)a); ((void)b);
        constexpr static nvxbt RP = std::max(P1, P2);
        constexpr static nvxbt RD = D1 + D2;
        constexpr static nvxbt RM = std::max(M1, M2);
        return NI2<RD,RP,RM>();
    }
    template<typename OT, nvxbt D1, nvxbt P1, nvxbt M1>
    inline auto NI2Denominator(const NI2<D1,P1,M1> &a, const OT &b) {
        ((void)a); ((void)b);
        return NI2<D1,P1,M1>();
    }
    template<typename OT, nvxbt D1, nvxbt P1, nvxbt M1>
    inline auto NI2Numerator(const NI2<D1,P1,M1> &a, const OT &b) {
        ((void)a); ((void)b);
        return NI2<D1,P1,M1>();
    }

    template<typename TA, typename TB>
    inline constexpr NVX2_RETURNS(nvxbt) NI2Shift() {
        return TB::MinorBits - TA::MinorBits;
    }
    template<typename TA, typename TB>
    inline constexpr NVX2_RETURNS(nvxbt) NI2Excess() {
        return TA::MajorBits - TB::MajorBits;
    }

    template<unvxbt Bytes>
    inline constexpr auto NIPrimative() {
        if constexpr(Bytes > 8) return __int128_t(0);
        else if constexpr(Bytes > 4) return int64_t(0);
        else if constexpr(Bytes > 2) return int32_t(0);
        else if constexpr(Bytes > 1) return int16_t(0);
        else if constexpr(true) return int8_t(0);
    }
    //Because 128/256 bit integer make_unsigned is undefined ATM, we have to do this -_-
    template<unvxbt Bytes>
    inline constexpr auto NIUnsignedPrimative() {
        if constexpr(Bytes > 8) return __uint128_t(0);
        else if constexpr(Bytes > 4) return uint64_t(0);
        else if constexpr(Bytes > 2) return uint32_t(0);
        else if constexpr(Bytes > 1) return uint16_t(0);
        else if constexpr(true) return uint8_t(0);
    }

    template<typename TA, typename TB>
    struct choose_nvxi {
        typedef typename std::conditional<std::is_base_of<NVX_IRoot, TB>::value||std::is_base_of<NVX_IRoot, TA>::value,
        typename std::conditional<(std::is_base_of<NVX_IRoot, TB>::value), TB, TA>::type,
        TA>::type type;
        typedef type Type;
    };
    /*
    template<typename TA, typename TB>
    using best_vector = typename choose_nvxv<TA,TB>::type::template MVec<std::max(TA::Multiplier, TB::Multiplier)>;
    */
    template<typename TA, typename TB>
    using best_scalar = decltype(NI2Intermediate(TA(),TB()));//typename choose_nvxi<TA,TB>::type::template MRow<std::max(TA::Multiplier, TB::Multiplier)>;


    template<nvxbt D, nvxbt P, nvxbt M>
    class NI2 : public Container<D+P,M> {
    public:
        typedef Container<D+P,M> Cont;
        typedef NI2<D,P,M> Self;
        typedef NI2<D,P*2,M> HighpRow;
        typedef NI2<D,P/2,M> LowpRow;

        constexpr inline static nvxbt PointBit = P;
        constexpr inline static nvxbt MajorBits = Cont::DataBits - PointBit;
        constexpr inline static nvxbt MinorBits = Cont::DataBits - MajorBits;
        constexpr inline static nvxbt NonMajorBits = Cont::Bits - MajorBits;

        using Cont::MetaBits;
        using Cont::Bytes;
        using Cont::UnusedBits;
        using Cont::Alignment;
        using Cont::DataBits;
        using Cont::data;

        inline NI2() : Cont() {};

        typedef decltype(NIPrimative<Bytes>()) Primative;
        typedef decltype(NIUnsignedPrimative<Bytes>()) UPrimative;
        typedef Primative Type;
        typedef Primative type;
        typedef Primative Value;

        //Because c++ is freaking stupid in its support of bigints... (fake bitshift via 2's complement)
        constexpr inline static UPrimative MinorMask = _constexpr_lshift(static_cast<UPrimative>(0x1), MinorBits) - 1;
        constexpr inline static UPrimative MajorMask = (- MinorMask) + 1;

        inline bool superunital() const {
            return (this->handle() & MajorMask) != 0x0;
        }

       //constexpr inline static UPrimative MajorMask = std::numeric_limits<UPrimative>::max << MinorBits;
       //constexpr inline static UPrimative MinorMask = std::numeric_limits<UPrimative>::max >> (Cont::Bits - MinorBits);

        inline Primative& handle() {
            return (*reinterpret_cast<Primative*>(data));
        }
        inline Primative handle() const {
            return (*reinterpret_cast<const Primative*>(data));
        }

        typedef decltype(NIPrimative<((MajorBits + MinorBits) * 2)>()) PrimativeP;
        typedef decltype(NIUnsignedPrimative<((MajorBits + MinorBits) * 2)>()) UPrimativeP;
        inline auto handleP() const {
            return static_cast<PrimativeP>(this->handle());
        }

        inline static auto Numerator() {
            return decltype(NI2Numerator(Self(),Self()))();
        }
        inline static auto Denominator() {
            return decltype(NI2Denominator(Self(),Self()))();
        }

        _falias(raw, handle);
        _falias(rawref, handle);

        template<typename INTEGER>
        inline NI2(const INTEGER in, const typename std::enable_if<std::is_integral<INTEGER>::value,INTEGER*>::type nll = nullptr) : Cont() {
            ((void)nll);
            this->handle() = in;
            if constexpr(PointBit != 0) this->handle() <<= PointBit;
        }
        template<typename FLOAT>
        inline NI2(FLOAT in, typename std::enable_if<std::is_floating_point<FLOAT>::value,FLOAT*>::type nll = nullptr) : Cont() {
            ((void)nll);
            this->handle() = static_cast<long double>(in) * exp2(static_cast<long double>(MinorBits));
            NVX2_DBG(std::cout << "\nCONSTRUCT FROM " << in << " * " << exp2(static_cast<long double>(MinorBits)) << " = " << this->handle() << "\n";)
        }

        inline NI2(const Self& in) {//Trivial copy of homogenous data
            this->handle() = in.handle();
        }

        template<typename VT>
        inline NI2(VT in, typename std::enable_if<std::is_base_of<NVX_T, VT>::value,void*>::type nll = nullptr) : NI2() {
            ((void)in); ((void)nll);
        }

        template<typename OTHER>
        inline NI2(OTHER in, typename std::enable_if<std::is_base_of<NVX_IRoot, OTHER>::value, OTHER*>::type nll = nullptr) : Cont() {//Non-homogenous conversion copy
            ((void)nll);
            typedef decltype(NI2Intermediate(OTHER(), Self())) IT;
            IT intermediate = IT::FromInferior(in);
            NVX2_DBG(std::cout << "\n\nCONV" << intermediate.handle() << "\n";)
            *this = FromSuperior(intermediate);
        }

        inline explicit operator double() const {
            return this->handle() / exp2(static_cast<long double>(MinorBits));
                /*( (this->handle() << MetaBits) >> (PointBit + MetaBits)) + //Integer part
                (this->handle() - ((this->handle() >> PointBit) << PointBit)) //Fractional reciprocal
                / exp2(static_cast<long double>(MinorBits)); // Denominator
                */
        }

        inline explicit operator Primative() const {
            return ( (this->handle() << MetaBits) >> (PointBit + MetaBits));
        }

#ifdef NVX_STR_CASTS
        inline std::string typeStr() const {
            std::stringstream ss;
            ss << "NI2<" << MajorBits << "." << MinorBits << "+"<<MetaBits << "&" << UnusedBits << ":" << Bytes<< "B_" << Alignment <<">";
            return ss.str();
        }
        inline std::string str(bool isRaw = false) const {
            ((void)isRaw);
            static constexpr const unvxbt base10Sigs = ::floor(::log10(double(static_cast<Primative>(0x1) << MinorBits))) + 1;

            std::stringstream ss;
            ss << std::setprecision(base10Sigs) << ( (this->handle() - ((this->handle() >> PointBit) << PointBit)) / static_cast<long double>(Primative(1) << MinorBits) );
            std::string decString = ss.str();

            ss.str("");
            ss << /*"RAW("<<this->handle() << ") " << */( (this->handle() << MetaBits) >> (PointBit + MetaBits)) << "." <<
            (decString.size() > 2? decString.substr(2, decString.find_last_not_of('0') -1) : "");
            return ss.str();
        }
#endif

        static inline Self FromPrimative(const Primative raw) {
            //Force return ellision
            Self ret;
            ret.handle() = raw;
            //std::cout << "(set primative handle to " << ret.handle() << ")";
            return ret;
        }

        template<typename T>
        static inline NVX2_IDEN_RETURNS(Self) FromInferior(const T in) {
            Self ret;
            ret.handle() = in.handle();

            NVX2_DBG(std::cout << "\nInferior conversion from " << in.typeStr() << " to " << Self().typeStr() << "\n";)

            //Shift for decimal-bit alignment
            constexpr static const nvxbt alignmentShift = NI2Shift<T,Self>();
            static_assert(alignmentShift >= 0, "Inferior type passed with superior alignment");
            if constexpr(alignmentShift > 0) {
                ret.handle() <<= alignmentShift;
            }

            //If needed, mask and copy in metadata
            /*if constexpr(MetaBits > 0 && T::MetaBits > 0) {
                //Shift out (null) our own MetaBits, then isolate in's metabits, cast to our primative type, and realign as our type expects, then OR into the final value
                ret.handle() = ((ret.handle() << MetaBits) >> MetaBits) |
                                ( Primative(in.handle() >> T::DataBits) << (DataBits));
            }*/
            return ret;
        }
        template<typename T, typename V = void*>
        static inline NVX2_CONV_IDEN_RETURNS(Self) FromInferior(const T in, const V unu = nullptr) {
            ((void)unu);
            return FromInferior(Self(in));
        }

        template<typename T>
        static inline NVX2_IDEN_RETURNS(Self) FromSuperior(T in) {
            Self ret;
            //Shift for decimal-bit alignment
            constexpr static const nvxbt alignmentShift = NI2Shift<T,Self>();// - T::MinorBits;
            NVX2_DBG(std::cout << "\nSuperior conversion from " << in.typeStr() << " " << in << " to " << Self().typeStr() << ", shift= " << alignmentShift << "\n");

            static_assert(alignmentShift <= 0, "Superior type passed with inferior alignment");
            if constexpr(alignmentShift != 0) {
                in.handle() >>= -alignmentShift;
            }

            ret.handle() = in.handle();

            //If needed, mask and copy in metadata
            /*if constexpr(MetaBits > 0 && T::MetaBits > 0) {
                //Shift out (null) our own MetaBits, then isolate in's metabits, cast to our primative type, and realign as our type expects, then OR into the final value
                ret.handle() = ((ret.handle() << MetaBits) >> MetaBits) |
                                ( Primative(in.handle() >> T::DataBits) << (DataBits));
            }*/
            return ret;
        }
        template<typename T, typename V = void*>
        static inline NVX2_CONV_IDEN_RETURNS(Self) FromSuperior(const T in, const V unu = nullptr) {
            ((void)unu);
            return FromSuperior(Self(in));
        }


        inline static constexpr Self Nil() {
            Self ret; ret.makeNil();
            return ret;
        }

        inline constexpr Self operator-() const {
            Self ret(*this);
            ret.handle() = -this->handle();
            return ret;
        }
        inline constexpr Self& operator++() {
            this->handle() += 1;
            return *this;
        }
        inline constexpr  Self& operator++(int unu) {
            (void(unu));
            this->handle() += 1;
            return *this;
        }
        inline constexpr Self& operator--() {
            this->handle() -= 1;
            return *this;
        }
        inline constexpr Self& operator--(int unu) {
            (void(unu));
            this->handle() -= 1;
            return *this;
        }

        template<typename T>
        inline constexpr T to() const {
            return static_cast<T>(*this);
        }
        template<typename T>
        inline constexpr Self& from(const T& val) {
            (*this) = val;
            return *this;
        }

        template<typename TARG_T>
        inline explicit constexpr operator TARG_T() const {
            return to<TARG_T>();
        }

        inline explicit operator bool() const {
            return !this->isNil();
        }

        inline constexpr Self operator~() const {
            Self ret(*this);
            ret.handle() = ~ret.handle();
            return ret;
        }

        NVX_OPEQ_OP(+);
        NVX_OPEQ_OP(-);
        NVX_OPEQ_OP(*);
        NVX_OPEQ_OP(/);
        NVX_OPEQ_OP(%);
    };

    //Addition
    template<typename T>
    inline NVX2_IDEN_RETURNS(typename T::Self) operator+(const T& a, const T& b) {
        static_assert(T::MetaBits == 0, "Meta bits not yet implemented");
        if constexpr(T::MetaBits > 0) {//Preserve meta bits and detect overflow
            return T::FromPrimative(0);
        } else {
            //std::cout << "(add handles " << a.handle() << " + " << b.handle() << ")";
            return T::FromPrimative(a.handle() + b.handle());//No witchcraft needed and already aligned, just add
        }
    }

    template<typename TA, typename TB>
    inline NVX2_RETURNS( decltype(NI2Result(TA(),TB())) )
    operator+(const TA& a, const TB& b) {
        typedef decltype(NI2Result(a,b)) RT;
        typedef decltype(NI2Intermediate(a,b)) IT;

        const IT ia = IT::FromInferior(a), ib = IT::FromInferior(b);
        //std::cout << "\trequired shifts: {TA"<< NI2Shift<TA,IT>() <<"} {TB"<< NI2Shift<TB,IT>() <<"}\n";
        return RT::FromSuperior( ia + ib );//Resolves to single-template overload
    }

    //Subtraction
    template<typename T>
    inline NVX2_IDEN_RETURNS(typename T::Self) operator-(const T& a, const T& b) {
        if constexpr(T::MetaBits > 0) {//Preserve meta bits and detect overflow
            return T::FromPrimative(0);
        } else {
            return T::FromPrimative(a.handle() - b.handle());
        }
    }

    template<typename TA, typename TB>
    inline NVX2_RETURNS( decltype(NI2Result(TA(),TB())) )
    operator-(const TA& a, const TB& b) {
        typedef decltype(NI2Result(a,b)) RT;
        typedef decltype(NI2Intermediate(a,b)) IT;

        const IT ia = IT::FromInferior(a), ib = IT::FromInferior(b);
        return RT::FromSuperior( ia - ib );//Resolves to single-template overload
    }

    //Modulus
    template<typename T>
    inline NVX2_IDEN_RETURNS(typename T::Self) operator%(const T& a, const T& b) {
        if constexpr(T::MetaBits > 0) {//Preserve meta bits and detect overflow
            return T::FromPrimative(0);
        } else {
            return T::FromPrimative(a.handle() % b.handle());
        }
    }

    template<typename TA, typename TB>
    inline NVX2_RETURNS( decltype(NI2Result(TA(),TB())) )
    operator%(const TA& a, const TB& b) {
        typedef decltype(NI2Result(a,b)) RT;
        typedef decltype(NI2Intermediate(a,b)) IT;

        const IT ia = IT::FromInferior(a), ib = IT::FromInferior(b);
        return RT::FromSuperior( ia % ib );//Resolves to single-template overload
    }

    //Multiplication
    template<typename T>
    inline NVX2_IDEN_RETURNS(typename T::Self) operator*(const T a, const T b) {
        if constexpr(T::MetaBits > 0) {//Preserve meta bits and detect overflow
            return T::FromPrimative(0);
        } else {
            const auto product = (a.handleP() * b.handleP());
            const auto redacted = product >> (T::MinorBits);

            NVX2_DBG(std::cout << " \n\t"<<a.handleP()<<" * "<<b.handleP() << " = " << product << " -> " << redacted/* << " r " << ((product - (redacted << T::MinorBits)) >> 1) << " + " << sgn((product - (redacted << T::MinorBits)) >> 1) */<< "\n";);

            return T::FromPrimative(redacted/* + sgn((product - (redacted << T::MinorBits)) >> 1)*/);
        }
    }

    template<typename TA, typename TB>
    inline NVX2_RETURNS( decltype(NI2Result(TA(),TB())) )
    operator*(const TA& a, const TB& b) {
        typedef decltype(NI2Result(a,b)) RT;
        typedef decltype(RT::Numerator()) NT;
        typedef decltype(RT::Denominator()) DT;

        if(a.superunital() && b.superunital()) {//Getting bigger, so favor the integer
            return RT::FromSuperior(NT(a) * NT(b));
        } else {//Getting smaller, so favor the decimal
            return RT::FromSuperior(DT(a) * DT(b));
        }

        //const RT ia = RT::FromInferior(a), ib = RT::FromInferior(b);
        //return RT::FromSuperior(ia * ib);
        //typename IT::UPrimative product = (ia.handleP() * b.handleP());// >> (IT::MinorBits);
        //std::cout << " \n\t" << ia.handleP() << " * " << b.handleP() << " = " << (ia.handleP() * b.handleP()) << " -> " << IT::FromPrimative(product) << "\n";
        //std::cout << "\n\t"<<product<<"\t\t" << IT::MinorMask << "\t\t" << IT::MajorMask << "\n";
        //std::cout << "\t\t" << (product & MinorMask) << "\n";
        //product = /*(product & MajorMask) | */((product & MinorMask) );

        //return RT::FromSuperior( IT::FromPrimative(product) );//Resolves to single-template overload
    }

    //Division
    template<typename TA, typename TB>
    inline NVX2_RETURNS( decltype(NI2Result(TA(),TB())) )
    operator/(TA a, TB b) {
        typedef decltype(NI2Result(a,b)) RT;
        typedef decltype(RT::Numerator()) NT;
        typedef decltype(RT::Denominator()) DT;

        bool negate = false;
        if(a.handle() < 0) {
            a.handle() = -a.handle();
            negate = !negate;
        }
        if(b.handle() < 0) {
            b.handle() = -b.handle();
            negate = !negate;
        }

        if(a > b) {//Getting bigger, so favor the integer
            constexpr const auto shift = NT::DataBits - RT::DataBits;
            constexpr const auto shiftAdjust = shift - RT::MinorBits;//(shift/2 + (shift/2)%2);
            auto quotient = ((NT(a).handleP() << shift) / NT(b).handle()) >> shiftAdjust;
            /*(constexpr const auto shift = -NI2Excess<RT,NT>() +  NI2Shift<RT,NT>();// + RT::MinorBits;
            constexpr const auto shiftAdjust = -NI2Excess<RT,NT>()/2 + NI2Shift<RT,NT>() + (shift/2)%2;
            auto quotient = ((NT(a).handleP() << shift) / b.handle()) >> (2*shiftAdjust - (negate?NT::MinorBits : 0));*/
            //(std::cout << "\n\tNUMER "<<(NT(a).handleP() << shift)<<" / "<<NT(b).handle() << " = " << quotient << " << " << shift << " >> " << shiftAdjust << "\n");
            //std::cout << "\tRT: " << RT::MajorBits << ", NT: " << NT::MajorBits << "\n";
            //return RT::FromSuperior(NT::FromPrimative(negate? -quotient : quotient));//Superior(NT::FromPrimative(quotient));
            return RT::FromPrimative(negate? -quotient : quotient);
        } else {//Getting smaller, so favor the decimal
            /*constexpr const auto shift = NI2Shift<RT,DT>();
            auto quotient = (DT(a).handleP() << shift) / b.handle();
            (std::cout << "\n\tDENOM " << a.handle() << " -> "<< DT(a) << " -> "<<(DT(a).handleP() << shift)<<" / "<<b.handle() << " = " << quotient << " <> " << NI2Shift<RT,NT>() << "\n");
            return RT::FromSuperior(DT::FromPrimative(negate? -quotient : quotient));*/
            //constexpr const auto shift = DT::DataBits - RT::DataBits;
            //auto quotient = ((DT(a).handleP() << shift) / DT(b).handle());// >> (shift/2 + (shift/2)%2);
            constexpr const auto shift = DT::DataBits - RT::DataBits;
            constexpr const auto shiftAdjust = shift - RT::MinorBits;//(shift/2 + (shift/2)%2);
            auto quotient = ((DT(a).handleP() << shift) / DT(b).handle()) >> shiftAdjust;

            return RT::FromPrimative(negate? -quotient : quotient);
        }
        /*typedef decltype(NI2Result(a,b)) RT;
        typedef decltype(NI2Denominator(a,b)) IT;

        const IT ia = IT::FromInferior(a);//, ib = IT::FromInferior(b);
        constexpr const nvxbt numeratorBits = IT::MajorBits - RT::MajorBits;
        //std::cout << "\n\tdiv shift by" << numeratorBits << " -> " << (ia.handle() << numeratorBits) << " / " << ib.handle() << "\n";
        const IT quotient = IT::FromPrimative(((ia.handle() << numeratorBits) / b.handle()));
        return RT::FromSuperior(quotient);*/
    }
    /*
    template<typename TA, typename TB>
    inline NVX2_RETURNS( decltype(NI2Intermediate(TA(),TB())) )
    operator|(const TA a, const TB b) {
        decltype(NI2Intermediate(a,b)) ret;
        //std::cout << "\tintermediate shifts: {TA"<< NI2Shift<TA,decltype(ret)>() <<"} {TB"<< NI2Shift<TB,decltype(ret)>() <<"}\n";
        return ret;
    }*/

    //Boolean operations
#define NVX2_BOOL_OP(OP) \
    template<typename T>\
    inline NVX2_IDEN_RETURNS(bool) operator OP(const T a, const T b) {\
        return a.handle() OP b.handle();\
    }\
    template<typename TA, typename TB>\
    inline NVX2_CONV_RETURNS( bool ) operator OP(const TA a, const TB b) {\
        typedef decltype(NI2Intermediate(a,b)) IT;\
        \
        const IT ia = IT::FromInferior(a), ib = IT::FromInferior(b);\
        return ia OP ib;\
    }

    NVX2_BOOL_OP(==);
    NVX2_BOOL_OP(!=);
    NVX2_BOOL_OP(>=);
    NVX2_BOOL_OP(<=);
    NVX2_BOOL_OP(<);
    NVX2_BOOL_OP(>);

#define NVX2_BITWISE_OP(OP) \
    template<typename T>\
    inline NVX2_IDEN_RETURNS(typename T::Self) operator OP(const T a, const T b) {\
        if constexpr(T::MetaBits > 0) {\
            return T::FromPrimative(0);\
        } else {\
            return T::FromPrimative(a.handle() OP b.handle());\
        }\
    }\
    \
    template<typename TA, typename TB>\
    inline NVX2_CONV_RETURNS( decltype(NI2Result(TA(),TB())) )\
    operator OP(const TA a, const TB b) {\
        typedef decltype(NI2Result(a,b)) RT;\
        typedef decltype(NI2Intermediate(a,b)) IT;\
        \
        const IT ia = IT::FromInferior(a), ib = IT::FromInferior(b);\
        return RT::FromSuperior( ia OP ib );\
    }

    NVX2_BITWISE_OP(&);
    NVX2_BITWISE_OP(|);
    NVX2_BITWISE_OP(^);

    /*template<nvxbt W, nvxbt M>
    struct Container : public NVX_IRoot<W,M> {

    };*/
#ifdef NVX_STR_CASTS
    template<typename genType>
    typename std::enable_if<std::is_base_of<::nvx::NVX_IRoot, genType>::value, std::string>::type
    inline to_string(const genType& g) {
        return g.str();
    }
#endif


    template<nvxbt D, nvxbt P, nvxbt M>
    using NI =NI2<D, P, M>;
};



#pragma GCC diagnostic pop

#endif
