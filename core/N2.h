#ifndef N2_H
#define N2_H
#include "NVX2.vec.h"
#ifdef NVX_GLM_INTEROP
#include <glm/vec2.hpp>
#endif

namespace nvx {
template<typename V>
class NVXDat<NVXType::V2, V> : public NVX_T {
    public:
        static constexpr unsigned int size = 2;
        static constexpr bool HasBitfield = false;
        typedef NVXDat<NVXType::V2, V> Self;
        inline  NVXDat<NVXType::V2, V>() : x(0), y(0) {};
        
        typedef V RefRow;
        typedef RefRow Value;
        union {
            struct {
                RefRow x, y;
            };
            struct {
                RefRow X, Y;
            };
            V raw[size];
            RefRow rows[size];
        };
        
        inline typename V::Type extDebug() const { return 0; };
#ifdef NVX_GLM_INTEROP
        typedef glm::vec2 FPVec;
#endif
};

template<typename T> 
using N2T =NVX<2, T>;

};

#endif /* N2_H */

