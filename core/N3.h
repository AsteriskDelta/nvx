#ifndef N3_H
#define N3_H
#include "NVX2.vec.h"
#ifdef NVX_GLM_INTEROP
#include <glm/vec3.hpp>
#endif
namespace nvx {
template<typename V>
class NVXDat<NVXType::V3, V> : public NVX_T  {
    public:
        static constexpr unsigned int size = 3;
        static constexpr bool HasBitfield = false;

        typedef V RefRow;
        typedef RefRow Value;
        
        typedef NVXDat<NVXType::V3, V> Self;
        inline  NVXDat<NVXType::V3, V>() : x(0), y(0), z(0) {};
        
        union {
            struct {
                RefRow x, y, z;
            };
            struct {
                RefRow X, Y, Z;
            };
            V raw[size];
            RefRow rows[size];
        };
        inline typename V::Type extDebug() const { return 0; };
        
#ifdef NVX_GLM_INTEROP
        typedef glm::vec3 FPVec;
#endif
};

template<typename T> 
using N3T =NVX<3, T>;
}

#endif /* N3_H */

