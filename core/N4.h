#ifndef N4_H
#define N4_H
#include "NVX2.vec.h"
#ifdef NVX_GLM_INTEROP
#include <glm/vec4.hpp>
#endif

namespace nvx {
template<typename V>
class NVXDat<NVXType::V4, V> : public NVX_T  {
    public:
        static constexpr unsigned int size = 4;
        static constexpr bool HasBitfield = false;
        
        typedef V RefRow;
        typedef RefRow Value;
        
        typedef NVXDat<NVXType::V4, V> Self;
        inline  NVXDat<NVXType::V4, V>() : x(0), y(0), z(0), w(0) {};
        
        union {
            struct {
                RefRow x, y, z, w;
            };
            struct {
                RefRow X, Y, Z, W;
            };
            V raw[size];
            RefRow rows[size];
        };
        inline typename V::Type  extDebug() const { return 0; };
#ifdef NVX_GLM_INTEROP
        typedef glm::vec4 FPVec;
#endif
};

template<typename T> 
using N4T =NVX<4, T>;
};

#endif /* N4_H */

