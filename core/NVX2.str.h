#ifndef NVX2_STR_H
#define NVX2_STR_H
#ifdef NVX_STR_CASTS
#include "NVX2.core.h"
#include <sstream>
#include "fmnt/int128.h"
#include <iomanip>
/*
namespace nvx {
    template<nvxbt D, nvxbt P, nvxbt M>
    inline std::string NI2<D,P,M>::typeStr() const {
        std::stringstream ss;
        ss << "NI2<" << MajorBits << "." << MinorBits << "+"<<MetaBits << "&" << UnusedBits << ":" << Bytes<< "B_" << Alignment <<">";
        return ss.str();
    }
    template<nvxbt D, nvxbt P, nvxbt M>
    inline std::string NI2<D,P,M>::str(bool isRaw) const {
        _unused(isRaw);
        static constexpr const unvxbt base10Sigs = ::floor(::log10(double(static_cast<Primative>(0x1) << MinorBits)));
        
        std::stringstream ss;
        ss << std::setprecision(base10Sigs) << ( (this->handle() - ((this->handle() >> PointBit) << PointBit)) / static_cast<long double>(Primative(1) << MinorBits) );
        std::string decString = ss.str();
        
        ss.str("");
        ss << "RAW("<<this->handle() << ") " << ( (this->handle() << MetaBits) >> (PointBit + MetaBits)) << "." << 
        (decString.size() > 2? decString.substr(2, decString.find_last_not_of('0') -1) : "");
        return ss.str();
    }
};*/
#endif

#endif
