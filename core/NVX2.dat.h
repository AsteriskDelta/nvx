#ifndef NVX2_DAT_H
#define NVX2_DAT_H
#include "NVX.defines.h"
#include "NVX2.core.h"

namespace nvx {
    
#ifdef NVX_GLM_INTEROP
    template<unsigned char D>
    struct GLM_T;
    template<> struct GLM_T<3> {
        typedef ::glm::vec3 type;
    };
    template<> struct GLM_T<2> {
        typedef ::glm::vec3 type;
    };
    template<> struct GLM_T<4> {
        typedef ::glm::vec3 type;
    };
    
#endif
    
    template<unsigned char D, typename Row> struct NVX2Holder;
    
    template<typename Row> struct NVX2Holder<3, Row> {
        inline NVX2Holder() : x(),y(),z() {};
        
        union {
            Row rows[3];
            struct {
                Row x,y,z;
            };
        };
    };
    template<typename Row> struct NVX2Holder<2, Row> {
        inline NVX2Holder() : x(),y() {};
        
        union {
            Row rows[2];
            struct {
                Row x,y;
            };
        };
    };
    template<typename Row> struct NVX2Holder<4, Row> {
        inline NVX2Holder() : x(),y(),z(),q() {};
        
        union {
            Row rows[4];
            struct {
                Row x,y,z,q;
            };
        };
    };
    
    
    template<int D_ID, typename T>
    struct NVX2Dat : NVX_T, NVX2Holder<::abs(D_ID), T> {
        typedef T Row;
        typedef T& RefRow;
        typedef Row Value;
        
        static constexpr unsigned char Size = ::abs(D_ID);
        
        #ifdef NVX_GLM_INTEROP
        typedef typename GLM_T<Size>::type FPVec;  
        #endif
    };
    
    
    template<typename T> 
    struct base_value {
        typedef typename std::conditional<(std::is_base_of<NVX_T, T>::value || std::is_base_of<NVX_IRoot, T>::value), typename T::Value, T>::type type;
        typedef type Type;
    };
    template<typename TA, typename TB> 
    struct choose_nvxv {
        typedef typename std::conditional<(std::is_base_of<NVX_T, TB>::value), TB, TA>::type type;
        typedef type Type;
    };
};

#endif
