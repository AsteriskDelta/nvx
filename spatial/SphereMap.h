#ifndef NVX_NSPHEREMAP_H
#define NVX_NSPHEREMAP_H
#include "NVX.h"
#include <vector>

namespace nvx {
    template <typename FROM, typename TO>
    class SphereMap {
    public:
        typedef FROM From;
        typedef TO To;
        typedef typename FROM::Value Value;

        struct Entry {
            From from;
            To to;
            Value weight;
        };
        struct ResultPair {
            Value weight = 0;
            const To *value = nullptr;
            const Entry *entry = nullptr;
        };

        std::vector<Entry> entries;

        inline SphereMap(){

        }
        inline SphereMap(const SphereMap& o) {
            entries = o.entries;
        }
        inline virtual ~SphereMap() {

        }

        inline void add(const From& f, const To& t, const Value& w) {
            entries.push_back(Entry{f,t,w});
            /*Entry *entry = &(*entries.rbegin());

            entry->from = f;
            entry->to = t;
            entry->weight = w;*/
        }

        inline void clear() {
            entries.clear();
        }

        inline std::vector<ResultPair> evalList(const From& from, Value *const appWeightPtr = nullptr) const {
            Value appliedWeight = 0;
            std::vector<ResultPair> result;
            result.reserve(entries.size());

            for(auto it = entries.begin(); it != entries.end(); ++it) {
                Value weight = dot(from, it->from) * it->weight;
                weight *= weight;

                result.push_back(ResultPair{weight, &it->to, &(*it)});
                appliedWeight += weight;
            }

            for(auto it = result.begin(); it != result.end(); ++it) {
                it->weight /= appliedWeight;
            }

            if(appWeightPtr != nullptr) (*appWeightPtr) = appliedWeight;

            return result;
        }

        //Prevent instantiation for potentially incompatable types
        template<typename UNU=void*>
        inline To eval(const From& from) const {
            Value appliedWeight = 0;
            auto results = this->evalList(from, &appliedWeight);

            To ret;
            for(auto it = results.begin(); it != results.end(); ++it) {
                ret += (*it->value) * (it->weight / appliedWeight);
            }

            return ret;
        }

        inline std::vector<ResultPair> operator()(const From& from) const {
            return this->evalList(from);
        }
        inline To operator[](const From& from) const {
            return this->eval(from);
        }
    protected:
    };
};

#endif
