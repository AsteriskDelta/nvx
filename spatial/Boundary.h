#ifndef NVX_BOUNDRY_H
#define NVX_BOUNDRY_H
#include "NVX.defines.h"
#include "NVX.func.h"
#include "Transform.h"

namespace nvx {
    template<typename TRANSFORM>
    class Boundary_t : public TRANSFORM {
    public:
        typedef typename TRANSFORM::Vector Vector;
        typedef typename Vector::type Value;
        typedef TRANSFORM Transform_t;
        typedef Boundary_t<TRANSFORM> Self;
        
        enum Subspace : uint8_t {
            Local, World
        };
        
        struct Extant {
            inline constexpr static const auto Size = _constexpr_exp2(Vector::Size);
            Vector points[Size];
        };
        
        Vector maximum, minimum;
        
        inline bool contains(const Vector& pt, const Subspace s = World) const {
            if (s == Subspace::World) {
                return this->contains(Transform_t::toLocal(pt), Subspace::Local);
            } else {
                return pt >= minimum && pt <= maximum;
            }
        }
        
        inline void clear() {
            minimum = maximum = Vector(0);
        }
        
        inline bool empty() const {
            for(int i = 0; i < int(Vector::Size); i++) if(minimum[i] != static_cast<Value>(0) && maximum[i] != static_cast<Value>(0)) return false;
            return true;
        }
        
        inline Vector dimensions() const {
            return maximum - minimum;
        }
        
        inline Vector center(const Subspace s) const {
            if(s == Local) return this->dimensions()/2;
            else return Transform_t::toWorld(this->center(Local));
        }
        
        Value volume() const {
            return product(this->dimensions());
        }
        //Alias for 2D
        inline Value area() const {
            return this->volume();
        }
        
        inline _unroll Extant extant(const Subspace s) const {
            const bool worldSpace = (s == Subspace::World);
            Extant dims;
            
            //Unrolled at compile-time
            for(auto i = 0; i < int(Extant::Size); i++) {
                Vector pt;
                for(auto j = 0; j < int(Vector::Size); j++) {
                    if (i % _constexpr_exp2(j) == 0) pt[j] = minimum[j];
                    else pt[j] = maximum[j];
                }
                
                if (worldSpace) {
                    dims.points[i] = Transform_t::toWorld(pt);
                } else {
                    dims.points[i] = pt;
                }
            }
            return dims;
        }
        
        void add(const Vector& pt, const Subspace s) {
            if (s == Subspace::World) {
                minimum = min(pt, minimum);
                maximum = max(pt, maximum);
            } else {
                return this->add(Transform_t::toLocal(pt), Subspace::Local);
            }
        }
        
        void add(const Self& o) {
            const Extant ext = o.extant(Subspace::World);
            for(const Vector pt : ext.points) this->add(pt, Subspace::World);
        }
        
        Value radius() const {
            return length(this->dimensions()/Vector(2));
        }
        
        //Projected along axis
        /*T farPoint(const Vector& direction) const {
            
        }
        
        T nearPoint(const Vector& direction) const {
            return center;
        }*/
        
        bool intersects(const Self& o, const Subspace s = Subspace::World) const {
            Extant ea = this->extant(s), eb = o.extant(s);
            for(uint8_t j = 0; j < Extant::Size; j++) {
                if(this->contains(eb.points[j], s) || o.contains(ea.points[j], s)) return true;
            }
            return false;
        }
        
        bool contains(const Self& o, const Subspace s = Subspace::World) const {
            Extant eb = o.extant(s);
            for(uint8_t j = 0; j < Extant::Size; j++) {
                if(!this->contains(eb.points[j], s)) return false;
            }
            return true;
        }
    };
    
    template<typename V>
    using AlignedBoundary = Boundary_t<AlignedTransform<V>>;
    
    template<typename V>
    using Boundary = Boundary_t<Transform<V>>;
};

#endif


