#ifndef NVX_QUATERNION_H
#define NVX_QUATERNION_H
#include "NVX.defines.h"
#include "NVX2.vec.h"

#define QUATERNION_COMPONENT(LETTER, IDX)\
inline const Value& LETTER() const {\
    return Root::operator[](IDX);\
}\
inline Value& LETTER() {\
    return Root::operator[](IDX);\
}

namespace nvx {
    struct QuaternionRoot_ {};
    template<typename T>
    using QuaternionRoot = NVX2<T::Size+1, typename T::type>;
    
    template<typename V>
    class Quaternion : public QuaternionRoot<V>, public QuaternionRoot_ {
    public:
        typedef typename V::Value Value;
        typedef V Vector;
        typedef Quaternion<V> Self;
        typedef QuaternionRoot<V> Root;
        
        using Root::Root;
        inline Quaternion() : Root(1) {}
        inline Quaternion(const Root&& r) : Root(r) {};
        
        
        inline _unroll Self conjugate() const {
            Self ret;
            for(unsigned int i = 0; i < Vector::Size; i++) ret[i] = this->operator[](i);
            ret.q() = -this->q();
            return ret;
        }
        
        inline _unroll Value norm() const {
            Value ret = 0;
            for(unsigned int i = 0; i < Root::Size; i++) ret += this->operator[](i)*this->operator[](i);
            return ret;
        }
        
        Self inverse() const {
            return Self(static_cast<Root&&>(this->conjugate()) / this->norm());
        }
        
        QUATERNION_COMPONENT(r, 0);
        QUATERNION_COMPONENT(i, 1);
        QUATERNION_COMPONENT(j, 2);
        QUATERNION_COMPONENT(k, 3);
        
        inline const Value& q() const { return this->operator[](Root::Size-1); };
        inline Value& q() { return this->operator[](Root::Size-1); };
        
        inline _unroll Vector vec() const {
            Vector ret;
            for(unsigned int i = 0; i < Vector::Size; i++) ret[i] = this->operator[](i);
            return ret;
        }
    };
    
    template<typename T>
    inline constexpr typename std::enable_if<std::is_base_of<QuaternionRoot_, T>::value, T>::type 
    operator *(const T& a, const T &b) {
        //typedef typename T::Vector Vec_t;
        typedef T Quat_t;
        constexpr const unsigned int QOff = T::size - 1;
        
        Quat_t ret = cross(a,b);
        ret += a.q() * b;
        ret += b.a() * a;
        
        ret.q() = a.q() * b.q() - dot(a.vec(), b.vec());
        return ret;
    }
}

#endif

