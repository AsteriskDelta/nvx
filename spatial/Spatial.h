#pragma once

#include "Quaternion.h"
#include "Transform.h"
#include "Boundary.h"
#include "Bounded.h"

#define NVX_INSTANTIATE_SPATIAL(D, T, SUFFIX) \
namespace nvx {\
    template class AlignedTransform<NVX2<D,T>>;\
    template class Transform<NVX2<D,T>>;\
    template class Boundary_t<AlignedTransform<NVX2<D,T>>>;\
    template class Boundary_t<Transform<NVX2<D,T>>>;\
    template class Bounded_t<AlignedBoundary<NVX2<D,T>>>;\
    template class Bounded_t<Boundary<NVX2<D,T>>>;\
    template class Quaternion<NVX2<D,T>>;\
};

#define NVX_DECLARE_SPATIAL(D, T, SUFFIX) \
namespace nvx {\
    typedef AlignedTransform<NVX2<D,T>> AlignedTransform##SUFFIX;\
    typedef Transform<NVX2<D,T>> Transform##SUFFIX;\
    typedef Boundary_t<AlignedTransform<NVX2<D,T>>> AlignedBoundry##SUFFIX;\
    typedef Boundary_t<Transform<NVX2<D,T>>> Boundry##SUFFIX;\
    typedef Bounded_t<AlignedBoundary<NVX2<D,T>>> AlignedBounded##SUFFIX;\
    typedef Bounded_t<Boundary<NVX2<D,T>>> Bounded##SUFFIX;\
    typedef Quaternion<NVX2<D,T>> Quaternion##SUFFIX;\
};

#define NVX_DECLARE_SPATIAL_LC(D, T, SUFFIX) \
namespace nvx {\
    typedef AlignedTransform<NVX2<D,T>> alignedTransform##SUFFIX;\
    typedef Transform<NVX2<D,T>> transform##SUFFIX;\
    typedef Boundary_t<AlignedTransform<NVX2<D,T>>> alignedBoundry##SUFFIX;\
    typedef Boundary_t<Transform<NVX2<D,T>>> boundry##SUFFIX;\
    typedef Bounded_t<AlignedBoundary<NVX2<D,T>>> alignedBounded##SUFFIX;\
    typedef Bounded_t<Boundary<NVX2<D,T>>> bounded##SUFFIX;\
    typedef Quaternion<NVX2<D,T>> quaternion##SUFFIX;\
};

#include "Point.h"
NVX_DECLARE_SPATIAL_LC(3, geni_t, _t);
NVX_DECLARE_SPATIAL_LC(2, geni_t, 2d_t);
