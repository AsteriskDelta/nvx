#ifndef NVX_TRANSFORM_H
#define NVX_TRANSFORM_H
#include "NVX.defines.h"
#include "NVX2.vec.h"
#include "Quaternion.h"

#ifdef NVX_TRANSFORM_SCALE
#define SCALE_VARIABLE Vector scale;
#define SCALE_APPLY * scale
#define SCALE_INVERSE / scale
#else
#define SCALE_VARIABLE //inline const Vector scale = Vector(1);
#define SCALE_APPLY 
#define SCALE_INVERSE 
#endif

namespace nvx {
    template<typename V>
    class AlignedTransform {
    public:
        typedef V Vector;
        
        Vector position;
        
        Vector toWorld(const Vector& relPt) const {
            return position + relPt;
        }
        Vector toLocal(const Vector& worldPt) const {
            return worldPt - position;
        }
    };
    
    template<typename V>
    class Transform : public AlignedTransform<V> {
    public:
        using Vector = typename AlignedTransform<V>::Vector;
        typedef Quaternion<V> Quaternion;
        
        Quaternion rotation;
        SCALE_VARIABLE;
        
        Vector toWorld(const Vector& relPt) const {
            return AlignedTransform<V>::toWorld(relPt * rotation SCALE_APPLY);
        }
        Vector toLocal(const Vector& worldPt) const {
            return AlignedTransform<V>::toLocal(worldPt) / rotation SCALE_INVERSE;
        }
    };
};

#endif
