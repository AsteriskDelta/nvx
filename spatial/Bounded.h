#ifndef NVX_BOUNDED_H
#define NVX_BOUNDED_H
#include "NVX.defines.h"
#include "Transform.h"
#include "Boundary.h"

namespace nvx {
    template<typename BOUNDRY>
    class Bounded_t : public BOUNDRY {
    public:
        typedef typename BOUNDRY::Vector Vector;
        typedef BOUNDRY BoundaryInstance_t;
        BOUNDRY bounds;
        
        /*inline bool mayContainRel(const Vector& relPt) const {
            return bounds.containsRel(relPt);
        }
        inline bool mayContain(const Vector& pt) const {
            return bounds.contains(pt);
        }*/
        
        inline constexpr bool bounded() const {
            return true;
        };
        
        void constructBounds() = delete;
        inline void clearBounds() {
            bounds.clear();
        }
    };
    
    template<typename V>
    using AlignedBounded = Bounded_t<AlignedBoundary<V>>;
    
    template<typename V>
    using Bounded = Bounded_t<Boundary<V>>;
};

#endif

