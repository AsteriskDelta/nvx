#ifndef N2META_H
#define N2META_H
#include "NVX2.core.h"
#include <ARKE/Pseudo.h>
#ifdef NVX_GLM_INTEROP
#include <glm/vec2.hpp>
#endif
namespace nvx {
template<typename T>
struct N2Meta_RefRowBase {
    typedef N2Meta_RefRowBase<T> Self;
    static constexpr bool HasMetadata = true;
    
    T val : (sizeof (T) - 1)*8;
    unsigned char meta : 8;
    inline N2Meta_RefRowBase() : val(0), meta(0) {};

    inline void cloneMetadata(const Self& o) {
        meta = o.meta;
    }
    inline void makeNil() {
        (*reinterpret_cast<T*>(this)) = std::numeric_limits<T>::max();
    }
    inline bool isNil() const {
        return (*reinterpret_cast<const T*>(this)) == std::numeric_limits<T>::max();
    }
};

template<typename V, NVX_Mult_T MULT>
class NVXDat<NVXType::V2SoftMeta, V, MULT> : public NVX_T  {
    public:
        static constexpr unsigned int size = 2;
        static constexpr bool HasBitfield = true;
        typedef typename V::Type T;
        //NVX_MASK_BYTES(sizeof(T)-1);
        NVX_MASK_NONE();
        
        typedef NVXDat<NVXType::V2SoftMeta, V, MULT> Self;
        inline  NVXDat<NVXType::V2SoftMeta, V, MULT>() : raw{0,0} {};
        //inline ~NVXDat<NVXType::V2SoftMeta, V, MULT>() {};
        
        typedef NVX_IStruct<T, MULT, N2Meta_RefRowBase<T>> RefRow;
        typedef RefRow Value;
        union {
            struct {
                RefRow x;
                RefRow y;
            };
            struct {
                RefRow X;
                RefRow Y;
            };
            struct {
                unsigned char rawX[sizeof(T)-1];
                unsigned char rawMeta0;
                unsigned char rawY[sizeof(T)-1];
                unsigned char rawMeta1;
            };
            T raw[size];
            RefRow rows[size];
        };
        
        inline static RefRow getMetaID(const Self *obj) {
            return RefRow((((unsigned short) obj->rawMeta1) << 8) | obj->rawMeta0);
        }

        //template<typename V>
        inline static RefRow setMetaID(Self *obj, const RefRow& v) {
            obj->rawMeta0 = (v.template to<unsigned short>() & ((unsigned short) 0xFF));
            obj->rawMeta1 = ((v.template to<unsigned short>() >> 8) & ((unsigned short) 0xFF));
            return v;
        }
        
        inline const Pseudo<RefRow, 
        NVXDat<NVXType::V2SoftMeta, V, MULT>, 
        &(NVXDat<NVXType::V2SoftMeta, V, MULT>::getMetaID), 
        &(NVXDat<NVXType::V2SoftMeta, V, MULT>::setMetaID)> 
        Z() {
            return this;//automatic conversion ^_^
        }
        inline typename V::Type extDebug() const { return getMetaID(this).template to<typename V::Type>(); };
        
#ifdef NVX_GLM_INTEROP
        typedef glm::vec2 FPVec;
#endif
};
template<typename V, NVX_Mult_T MULT> 
constexpr std::array<typename V::Type, 2> NVXDat<NVXType::V2SoftMeta, V, MULT>::Masks;

typedef NVX<NVXType::V2SoftMeta, NVX_Default_Type, NVX_Default_Precision > N2Meta;
typedef NVX<NVXType::V2SoftMeta, NVX_Default_Type, 0 > N2MetaRaw;
};

#endif /* N2META_H */

