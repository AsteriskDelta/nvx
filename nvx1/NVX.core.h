#ifndef NVX_CORE_H
#define NVX_CORE_H
#include <ARKE/ARKEBase.h>
#include <ARKE/BitMask.h>
#include <typeinfo>

//If selected, NVX vectors will be entirely interoperable with GLM vectors- assignment, operations, even functions. 
#define NVX_GLM_INTEROP

//Defines .str() methods for NVXV/NVXI types, as well as stream operators
#define NVX_STR_CASTS

//If enabled, auto-initializes any created vectors/values to 0s
#define NVX_ZERO_INIT

//If true, meta fields attached to NVX objects will be preserved by otherwise ambiguous arithmetic operators
//Specifically, the first argument to an operator will determine the metadata of the output- ie, A:M1 + B:M2 = (A+B):M1
//#define NVX_ARITH_PRESERVES_METADATA
//TODO: Metadata interpolation feature? Not sure how useful this would be to anyone...

//The integral type for default vectors/values
#define NVX_DEFAULT_BASE_TYPE signed long long

//Default number of integers mapped to a single "real" value- powers of 2 are faster, but if seen numerically, their decimal forms may seem strange to people
#define NVX_DEFAULT_PRECISION ((NVX_DEFAULT_BASE_TYPE)4096)

//Factor by which precision is increased by generic highp and lowp calls- may be a function of MULT
//WARNING: NOT AUTO-PARENTHESIZED- you may use order of operations to change the sequence, or change factor ordering to avoid underflow
#define NVX_PRECISION_MULT (NVX_DEFAULT_PRECISION)

//Debug printouts- these are not the droids you're looking for (probably)
//#define NVX_DEBUG

#include "NVX.defines.h"

namespace nvx {
    //namespace std = ::std;
      //Because parent namespace resolution, and let's face it: you only hate it because you didn't think of it first ;)
    namespace $=::nvx;
    template<typename T, T MULT, typename BASE> struct NVX_IStruct;
    template<int D_ID, typename V, NVX_Mult_T MULT = (256)> class NVX;
    
    template<typename T> 
    struct base_value {
        typedef typename std::conditional<(std::is_base_of<NVX_T, T>::value || std::is_base_of<NVX_IRoot, T>::value), typename T::Value, T>::type type;
        typedef type Type;
    };
    template<typename TA, typename TB> 
    struct choose_nvxv {
        typedef typename std::conditional<(std::is_base_of<NVX_T, TB>::value), TB, TA>::type type;
        typedef type Type;
    };
    template<typename TA, typename TB> 
    struct choose_nvxi {
        typedef typename std::conditional<(std::is_base_of<NVX_IRoot, TB>::value), TB, TA>::type type;
        typedef type Type;
    };
    
    template<typename TA, typename TB> 
    using best_vector = typename choose_nvxv<TA,TB>::type::template MVec<std::max(TA::Multiplier, TB::Multiplier)>;
    template<typename TA, typename TB> 
    using best_scalar = typename choose_nvxi<TA,TB>::type::template MRow<std::max(TA::Multiplier, TB::Multiplier)>;
    
    template<typename T, NVX_Mult_T MULT = NVX_Default_Precision> 
    using NIT = NVX_IStruct<T, MULT>;
};

//Pull in macros for operators
#include "NVXI.arith.h"

namespace nvx {

template<typename T, T MULT, typename BASE>
struct NVX_IStruct : public BASE, public NVX_IRoot {
    static constexpr T Multiplier = MULT;
    typedef T Type;
    typedef NVX_IStruct<T,MULT,BASE> Self;
    typedef NVX_IStruct GenSelf;
    typedef BASE Base;
    
    typedef NVX_IStruct<T,0,BASE> Raw;
    typedef NVX_IStruct<T, MULT, BASE> Row;
    typedef Row Value;
    
    typedef NVX_IStruct<T, MULT*NVX_PLEVEL_MULT, BASE> HighpRow;
    typedef NVX_IStruct<T, std::max(static_cast<decltype(MULT/NVX_PLEVEL_MULT)>(1), MULT/NVX_PLEVEL_MULT), BASE> LowpRow;
    template<typename TB> using BestRow = NVX_IStruct<T, std::max(Multiplier, TB::Multiplier), BASE>;
    template<typename TB> using WorstRow = NVX_IStruct<T, std::min(Multiplier, TB::Multiplier), BASE>;
    template<NVX_Mult_T NM> using MRow = NVX_IStruct<T,  NM>;
    
    inline static constexpr Self Nil() {
        Self ret; ret.makeNil();
        return ret;
    }
    
    template<typename TARG_T> inline constexpr typename std::enable_if<std::is_integral<TARG_T>::value, TARG_T>::type to() const {
        NVX_DBG(std::cout << "INT TO base=" << (BASE::val / NVX_OpMult<TARG_T, Self>()) << " roundP="<<  (((BASE::val%NVX_OpMult<TARG_T, Self>()) >= NVX_OpMult<TARG_T, Self>()/2)? 1 : 0)<<"\n");
        return (BASE::val / NVX_OpMult<TARG_T, Self>()) + ((frac(*this) >= 0.5)? 1 : 0);//(((BASE::val%NVX_OpMult<TARG_T, Self>()) >= NVX_OpMult<TARG_T, Self>()/2)? 1 : 0);
    }
    template<typename TARG_T> inline constexpr typename std::enable_if<std::is_floating_point<TARG_T>::value, TARG_T>::type to() const {
        NVX_DBG(std::cout<< "FP TO\n");
        return TARG_T(double(BASE::val) / double(NVX_OpMult<TARG_T, Self>()));
    }
    template<typename TARG_T> inline constexpr typename std::enable_if<!std::is_base_of<NVX_IRoot, TARG_T>::value && std::is_integral<TARG_T>::value, Self&>::type from(const TARG_T& v) {
        NVX_DBG(std::cout << "FROM INT " << typeid(TARG_T).name() << " = " << v << "\n");
        BASE::val = (v * NVX_OpMult<TARG_T, Self>());
        return *this;
    }
    template<typename TARG_T> inline constexpr typename std::enable_if<!std::is_base_of<NVX_IRoot, TARG_T>::value && std::is_floating_point<TARG_T>::value, Self&>::type from(const TARG_T& v) {
        NVX_DBG(std::cout << "DBL FROM: " << double(v) << " MUL="<<double(NVX_OpMult<Self, TARG_T>())<<", RECIP="<< double(NVX_OpRecip<Self, TARG_T>()) <<"\n");
        BASE::val = T( ::round(double(v) * double(NVX_OpMult<TARG_T, Self>()) ) );
        return *this;
    }
    
    //Needed for bitfields, because they apparently have a blood feud with templates
    //TODO: Actual template recognition of bitfields, and appropriate handling? No type_traits for this, so it'll have to be something hacky and a little too clever
    /*template<typename TARG_T> inline typename std::enable_if<
    !std::is_integral<TARG_T>::value && !std::is_floating_point<TARG_T>::value && !std::is_base_of<NVX_IRoot, TARG_T>::value,
    Self&>::type from(const TARG_T& v) {
        BASE::val = Type(v);
        return *this;
    }*/
    /*template<typename TARG_T> inline typename std::enable_if<!std::is_integral<TARG_T>::value&&!std::is_floating_point<TARG_T>::value&&! std::is_base_of<NVX_IRoot, TARG_T>::value, TARG_T>::type to() const {
        return BASE::val;
    }*/
    template<typename TARG_T> inline constexpr typename std::enable_if< Base::HasMetadata && 
        std::is_base_of<NVX_IRoot, TARG_T>::value && std::is_base_of<Base, TARG_T>::value, void>::type
    fromMeta(const TARG_T& v) {
        BASE::cloneMetadata(static_cast<const typename TARG_T::Base&>(v));
    }
    template<typename TARG_T> inline constexpr typename std::enable_if<!Base::HasMetadata || !std::is_base_of<NVX_IRoot, TARG_T>::value, void>::type
    fromMeta(const TARG_T& v) {
        _unused(v);
    }
    template<typename TARG_T> inline constexpr typename std::enable_if<Base::HasMetadata && 
        std::is_base_of<NVX_IRoot, TARG_T>::value  && !std::is_base_of<Base, TARG_T>::value, void>::type
    fromMeta(const TARG_T& v) {
        _unused(v);
    }
    
    template<typename TARG_T> inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TARG_T>::value || std::is_base_of<NVX_IRoot&, TARG_T>::value, Self&>::type from(const TARG_T& v) {
        if(v.isNil()) {//Special case for nils on custom types
            BASE::makeNil();
            return *this;
        }
        BASE::val = (v.raw() * NVX_OpMult<TARG_T, Self>()) / NVX_OpMult<Self, TARG_T>();
        
        //Rounding, if needed
        if(::abs(TARG_T::Multiplier) > ::abs(Multiplier) &&  Multiplier != 0) {
            //constexpr evaluated at compile time, so guarding if is useless
            constexpr typename TARG_T::Type truncMult = (Multiplier == 0)? T(1) : (TARG_T::Multiplier / Multiplier);
            if((v.raw() % truncMult) >= truncMult/2) (BASE::val)++;//Round up
        }
        
        //Metadata if we have it- again, constexpr, so should be optimized out if unneeded
        this->fromMeta(v);
        
        return *this;
    }
   template<typename TARG_T> inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TARG_T>::value || std::is_base_of<NVX_IRoot&, TARG_T>::value || std::is_base_of<const NVX_IRoot&, TARG_T>::value, TARG_T>::type to() const {
       TARG_T ret;
       ret.from(*this);
       return ret;
    }
        
    inline NVX_IStruct<T,MULT,BASE>() : BASE() {};
    template<typename TARG_T> 
    inline constexpr NVX_IStruct<T,MULT,BASE>(const TARG_T& o, typename std::enable_if<std::is_base_of<NVX_IRoot, TARG_T>::value, void *const&>::type _icFlag = nullptr) {
         _unused(_icFlag);
        this->template from<TARG_T>(o);
    }
    template<typename TARG_T> 
    inline constexpr NVX_IStruct<T,MULT,BASE>(const TARG_T& o, typename std::enable_if<!std::is_base_of<NVX_IRoot, TARG_T>::value && (std::is_floating_point<TARG_T>::value || std::is_integral<TARG_T>::value), void *const&>::type _icFlag = nullptr) {
        _unused(_icFlag);
        this->template from<TARG_T>(o);
    }
    
    template<typename TARG_T>
    inline explicit constexpr operator TARG_T() const {
        return to<TARG_T>();
    }
    
    inline explicit operator bool() const {
        return !this->isNil();
    }
    
    template<typename TARG_T>
    inline constexpr Self& operator=(const TARG_T& o) {
         this->template from<TARG_T>(o);
         return *this;
    }
    
    inline constexpr T raw() const { return BASE::val; };
    template<class=void>//Prevent instantiation unless needed, since this won't be valid for all types
    inline T& rawref() { return BASE::val; };//Purposefully different names, this shouldn't be oft used
    
    inline constexpr void setRaw(const T& rv) {
        BASE::val = rv;
    }
    
    NVX_OPEQ_OP(+);
    NVX_OPEQ_OP(-);
    NVX_OPEQ_OP(*);
    NVX_OPEQ_OP(/);
    NVX_OPEQ_OP(%);
    NVX_BOOL_OP_FRIEND(<,  NVX_IRoot);
    NVX_BOOL_OP_FRIEND(>,  NVX_IRoot);
    NVX_BOOL_OP_FRIEND(<=, NVX_IRoot);
    NVX_BOOL_OP_FRIEND(>=, NVX_IRoot);
    NVX_BOOL_OP_FRIEND(==, NVX_IRoot);
    NVX_BOOL_OP_FRIEND(!=, NVX_IRoot);
    
    inline constexpr Self operator-() const {
        Self ret(*this);
        ret.val = -BASE::val;
        return ret;
    }
    inline constexpr Self& operator++() {
        (*this) += T(1);
        return *this;
    }
    inline constexpr  Self& operator++(int unu) {
        _unused(unu);
        (*this) += T(1);
        return *this;
    }
    inline constexpr Self& operator--() {
        (*this) -= T(1);
        return *this;
    }
    inline constexpr Self& operator--(int unu) {
        _unused(unu);
        (*this) -= T(1);
        return *this;
    }
    
#ifdef NVX_STR_CASTS
    inline std::string str() const;
#endif
};
template<typename T, T MULT, typename BASE>
constexpr T NVX_IStruct<T, MULT, BASE>::Multiplier;
NVXI_ARITH_OP(+, NVX_OpRecip);
NVXI_ARITH_OP(-, NVX_OpRecip);
NVXI_ARITH_OP(%, NVX_OpRecip);
//NVXI_ARITH_OP(*, NVX_OpRecipSq);
//Special implementation of division is done in NVXI.arith.h, to avoid possible over/underflows

NVXI_BOOL_OP(==);
NVXI_BOOL_OP(!=);
NVXI_BOOL_OP(<=);
NVXI_BOOL_OP(>=);
NVXI_BOOL_OP(<);
NVXI_BOOL_OP(>);

template<int D, typename V, NVX_Mult_T MULT> class NVXDat;

namespace NVXType {
    enum {
        vec2 = 2, V2 = vec2,
        vec3 = 3, V3 = vec3,
        vec4 = 4, V4 = vec4,
        
        vec2smeta = -2, V2SoftMeta = vec2smeta
    };
};

template<int D_ID, typename V, NVX_Mult_T MULT>
class NVX : public NVXDat<D_ID, V, MULT> {
public:
    typedef NVXDat<D_ID, V, MULT> Dat;
    typedef NVX<D_ID,V,MULT> Self;
    typedef NVX<D_ID,V,0> Raw;
    typedef NVX GenSelf;
    
    static constexpr unsigned int D = Dat::size;
    static constexpr unsigned int Size = Dat::size;
    static constexpr typename V::Type Multiplier = MULT;
    static constexpr int VecID = D_ID;
    
    typedef unsigned char Idx;
    typedef typename Dat::Value Value;
    
    typedef typename Dat::RefRow Row;
    typedef NVX_IStruct<typename V::Type, MULT*D*D*NVX_PLEVEL_MULT> HighpRow;
    typedef NVX_IStruct<typename V::Type, std::max(1ll, MULT/D/D/NVX_PLEVEL_MULT)> LowpRow;
    typedef NVX<D_ID, V, MULT*NVX_PLEVEL_MULT> HighpVec;
    typedef NVX<D_ID, V, std::max(1ll, MULT/NVX_PLEVEL_MULT)> LowpVec;
    
    template<typename TB> using BestVec = NVX<D_ID, V, std::max(Multiplier, TB::Multiplier)>;
    template<typename TB> using WorstVec = NVX<D_ID, V, std::min(Multiplier, TB::Multiplier)>;
    template<NVX_Mult_T NM> using MVec = NVX<D_ID, V, NM>;
    template<int NT, NVX_Mult_T NM = MULT> using DMVec = NVX<NT, V, NM>;
    
    inline static constexpr Self Nil() {
        Self ret;
        for(Idx i = 0; i < D; i++) ret.rows[i].makeNil();
        return ret;
    }
    
    template<typename T>
    inline constexpr void set(const Idx& idx, const T& val) { 
        Dat::rows[idx] = val; 
    };
    
    //Initialize to zero as expected by external libraries
#ifdef NVX_ZERO_INIT
    inline _unroll constexpr NVX() : NVXDat<D_ID, V, MULT>() {};
#else
    inline _unroll constexpr NVX() {};
#endif
    
    inline _unroll constexpr NVX(const GenSelf& orig) {
        for(Idx i = 0; i < D; i++) Dat::rows[i] = orig.rows[i];
    };
    template<typename T, typename = typename std::enable_if<( !std::is_base_of<NVX_T, T>::value  && !NVX_MATCH_GLM(T)), Self>::type>
    inline _unroll constexpr NVX(const T& initial) {
        for(Idx i = 0; i < D; i++) Dat::rows[i].template from<T>(initial);
    }
    
    template<typename T> inline constexpr typename std::enable_if<!std::is_base_of<NVX_T, T>::value && !NVX_MATCH_GLM(T), Idx>::type 
    argUnzipStore(const Idx i, const T& val) {
        //static_assert(i < D, "Too many arguments supplied to NVX vector constructor!");
        if(i >= D) return i+1;//throw std::logic_error( "Too many arguments supplied to NVX vector constructor!");
        NVX_DBG(std::cout << "argstore " << val << "\n");
        Dat::rows[i].template from(val);
        return i + 1;
    }
    template<typename T>
    inline constexpr typename std::enable_if<NVX_MATCH_GLM(T), Idx>::type argUnzipStore(const Idx i, const T& val) {
        for(Idx oi = 0; oi < val.length(); oi++) argUnzipStore(i + oi, val[oi]);
        return i + val.length();
    }
    template<typename T>
    inline constexpr typename std::enable_if<std::is_base_of<NVX_T, T>::value, Idx>::type argUnzipStore(const Idx i, const T& val) {
        for(typename T::Idx oi = 0; oi < T::D; oi++) argUnzipStore(i + oi, val[oi]);
        return i + T::D;
    }
    template<typename T>
    inline constexpr void argUnzip(const Idx i, const T& val) {
        argUnzipStore(i, val);
    }
    template<typename T, typename ...Args>
    inline constexpr void argUnzip(const Idx i, const T& val, const Args&... args) {
        argUnzip(argUnzipStore(i, val), args...);
    }
    
    template< typename ...Args>
    inline constexpr _unroll NVX( const Args& ...args) {
        this->argUnzip(0, args...);
    }
    
    template<typename ...Args>
    inline constexpr Self& operator=(const Args& ...args) {
        Self tmp(args...);
        std::swap(*this, tmp);
        return *this;
    }
    
    inline constexpr const Row& operator[](const Idx& idx) const {
        return Dat::rows[idx];
    }
    inline constexpr Row& operator[](const Idx& idx) {
        return const_cast<Row&>(const_cast<const Self*>(this)->operator[](idx));
    }
        
    template<typename TO_T> inline constexpr typename std::enable_if<!std::is_base_of<NVX_IRoot, TO_T>::value, TO_T>::type
    get(const Idx& idx) const {
        return this->operator[](idx).template to<TO_T>();
    }
    //Non-const and const
    template<typename TO_T> inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TO_T>::value, TO_T>::type
    get(const Idx& idx) {
        return this->operator[](idx);
    }
    template<typename TO_T> inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TO_T>::value, TO_T>::type
    get(const Idx& idx) const {
        return this->operator[](idx);
    }
    
    inline bool isNil() const {
        return Dat::rows[0].isNil();
    }
    inline explicit operator bool() const {
        return !this->isNil();
    }
    inline operator std::string() const {
        return str();
    }
    
    inline constexpr Self operator-() const {
        Self ret(*this);
        for(Idx i = 0; i < D; i++) ret[i] = -((*this)[i]);
        return ret;
    }
    
    NVX_OPEQ_OP(+);
    NVX_OPEQ_OP(-);
    NVX_OPEQ_OP(*);
    NVX_OPEQ_OP(/);
    NVX_OPEQ_OP(%);
    
    NVX_BOOL_OP_FRIEND(<,  NVX_T);
    NVX_BOOL_OP_FRIEND(>,  NVX_T);
    NVX_BOOL_OP_FRIEND(<=, NVX_T);
    NVX_BOOL_OP_FRIEND(>=, NVX_T);
    NVX_BOOL_OP_FRIEND(==, NVX_T);
    NVX_BOOL_OP_FRIEND(!=, NVX_T);
    
#ifdef NVX_GLM_INTEROP
    inline _unroll constexpr operator typename Dat::FPVec() const {
        typename Dat::FPVec ret;
        for(Idx i = 0; i < D; i++) ret[i] = this->get<float>(i);
        return ret;
    }
#endif

    //Provides overloads for generic raw accessors, ie, .rxx(), .ryy(), ..rzzref(), etc.
    NVXV_RAW_COMP_ACCESS(x, 0);
    NVXV_RAW_COMP_ACCESS(y, 1);
    NVXV_RAW_COMP_ACCESS(z, 2);
    NVXV_RAW_COMP_ACCESS(w, 3);
    
    //Swizzles
    //NVXV_SWIZZLES(2, x, y);
    //NVXV_SWIZZLES(3, x, y, z);
    //NVXV_SWIZZLES(4, x, y, z, w);
#ifdef NVX_STR_CASTS
    std::string str(bool isRaw = false) const;
#endif
protected:
};

NVXV_ARITH_OP(+);
NVXV_ARITH_OP(-);
NVXV_ARITH_OP(*);
NVXV_ARITH_OP(/);
NVXV_ARITH_OP(%);

NVXV_BOOL_OP_ALL(==);
NVXV_BOOL_OP_ONE(!=);
NVXV_BOOL_OP_ALL(<=);
NVXV_BOOL_OP_ALL(>=);
NVXV_BOOL_OP_ALL(<);
NVXV_BOOL_OP_ALL(>);
};

#include "NVX.core.impl.h"

#endif /* NVX_CORE_H */

