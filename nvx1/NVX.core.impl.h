#ifndef NVX_CORE_IMPL_H
#define NVX_CORE_IMPL_H

#define NVIF template<typename T, T MULT, typename BASE>
#define NVIC NVX_IStruct<T,MULT, BASE>
#define NVXF template<int D_ID, typename V,  NVX_Mult_T MULT>
#define NVXC NVX<D_ID,V,MULT>

namespace nvx {
#ifdef NVX_STR_CASTS
NVIF inline std::string NVIC::str() const {
    std::stringstream ss;
    const unsigned char precision = ceil(log10(double(Multiplier)));
    if(this->isNil()) ss << "nil";
    else ss << std::fixed << std::setprecision(precision) << this->template to<double>();
    return ss.str();
}

NVXF inline std::string NVXC::str(bool isRaw) const {
    std::stringstream ss; 
    if(D == D_ID) ss << "NV";
    else ss << "NV"<<D_ID<<":";
    ss << D;
    if(Multiplier == 0) ss << "raw";
    ss << "(";
    
    if(this->isNil()) {
        ss << "nil";
    } else if(isRaw) {
        ss << (typename base_value<V>::type::Type)((*this)[0].raw());//this->get<const V&>(0).raw();
        for(Idx i = 1; i < D; i++) ss << ", " << (typename base_value<V>::type::Type)((*this)[i].raw());//this->get(i);
    } else {
        ss << (*this)[0];
        for(Idx i = 1; i < D; i++) ss << ", " << (*this)[i];
    }
    if(Dat::extDebug() != 0) ss << " : " << Dat::extDebug();
    ss << ")";
    return ss.str();
}
#endif//NVX_STR_CASTS
};

#endif /* NVX_CORE_IMPL_H */

