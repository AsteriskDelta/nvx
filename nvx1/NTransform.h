#ifndef NVX_NTRANSFORM_H
#define NVX_NTRANSFORM_H
#include "NVX.core.h"
#include "Quaternion.h"

namespace nvx {
    template<typename PT>
    class NTransform {
    public:
        NTransform();
        ~NTransform();

        typedef PT Point;

        Point position;
        Quaternion<PT> rotation;
        Point scale;

        inline auto operator()(Point pt) const {
            pt *= scale;
            pt *= rotation;
            pt += position;
            return pt;
        }
        inline auto inverse()(Point pt) const {
            pt -= position;
            pt /= rotation;
            pt /= scale;
            return pt;
        }

        inline auto to(const Pt& pt) const {
            return this->operator()(pt);
        }
        inline auto from(const Pt& pt) const {
            return this->inverse(pt);
        }

    protected:

    };
}

#endif
