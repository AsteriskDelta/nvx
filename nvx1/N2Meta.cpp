#include "N2Meta.h"
#include "NVX.impl.h"
namespace nvx {
template class NVX<NVXType::V2SoftMeta, NVX_Default_Type, NVX_Default_Precision >;
template class NVX<NVXType::V2SoftMeta, NVX_Default_Type, 0>;
};
//template class NVX_IStruct<NVX_Default_Type, NVX_Default_Precision, typename NVXDat<NVXType::V2SoftMeta, NVX_Default_Type>::RefRowBase >;
//template class NVX_IStruct<NVX_Default_Type, 0, typename NVXDat<NVXType::V2SoftMeta, NVX_Default_Type>::RefRowBase >;