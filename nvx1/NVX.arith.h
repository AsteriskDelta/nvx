#ifndef NVXI_ARITH_H
#define NVXI_ARITH_H
#include <iostream>
//template<typename T>
//inline static void _bad_func(T&& parameter);

#define NVX_PPCAT_NX(A, B) A ## B

#ifdef NVX_ARITH_PRESERVES_METADATA
#define NVX_OP_CLONEMETA(var, A1, A2) var.fromMeta(A1)
#else
#define NVX_OP_CLONEMETA(var, A1, A2)
#endif

#define NVXI_BASE_OP(OP, INTER_TYPE, RET_1, RET_2, RET_3, TAIL_3, MEMB, PRERET_OP) \
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value && std::is_arithmetic<TB>::value && !std::is_base_of<NVX_IRoot, TB>::value && !std::is_base_of<NVX_T, TB>::value, RET_1>::type \
operator OP(const TA& a, const TB &b) {\
    NVX_DBG(std::cout << "CONV NVX OP integ, " << a << " OP " << b << "\n";);\
    typename std::remove_const<TA>::type conv; \
    conv.template from<TB>(b);\
    NVX_OP_CLONEMETA(conv, a, b);\
    return ((a OP conv));\
}\
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TB>::value && std::is_arithmetic<TA>::value &&!std::is_base_of<NVX_IRoot, TA>::value && !std::is_base_of<NVX_T, TA>::value, RET_2>::type \
operator OP(const TA& a, const TB &b) {\
    NVX_DBG(std::cout << "CONV integ OP NVX, " << a << " OP " << b << "\n";);\
    typename std::remove_const<TB>::type conv; \
    conv.template from<TA>(a);\
    NVX_OP_CLONEMETA(conv, b, a);\
    return ((conv OP b));\
}\
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value\
/*((TA::Multiplier >= TB::Multiplier && TB::Multiplier != 0) || (TA::Multiplier == 0 && TB::Multiplier != 0))*/, RET_3>::type \
operator OP(const TA& a, const TB &b) {\
    NVX_DBG(std::cout << " OP got " << static_cast<typename TA::template BestRow<TB>::Type>(a.raw() * NVX_OpMult<TA, typename TA::template BestRow<TB>>()) << " and " << static_cast<typename TA::template BestRow<TB>::Type>(b.raw() * NVX_OpMult<TB, typename TA::template BestRow<TB>>())  << " RECIP "<<static_cast<typename TA::template BestRow<TB>::Type>(NVX_OpRecip<TB, typename TA::template BestRow<TB>>())<<"\n");\
        RET_3 ret = 0;\
    ret MEMB = ( (\
        static_cast<INTER_TYPE>(a.raw() * NVX_OpMult<TA, typename TA::template BestRow<TB>>()) \
        OP \
        static_cast<INTER_TYPE>(b.raw() * NVX_OpMult<TB, typename TA::template BestRow<TB>>()) \
    ) TAIL_3);\
    PRERET_OP;\
    NVX_DBG(std::cout << "a\t" << a.raw() << " * " << NVX_OpMult<TA, TA>() << "\n"\
        << "b\t" << b.raw() << " * " << NVX_OpMult<TB, TA>() << "\nDIV "<< NVX_OpRecip<TA, TB>() << " for" << ", result= " << ret << "\n");\
    return ret;\
}

#define NVXI_ARITH_RECIP(RECIP_FN)  / (\
    static_cast<typename TA::template BestRow<TB>::Type>(RECIP_FN<TA, typename TA::template BestRow<TB>>()) \
)

#define NVXI_ARITH_INTER_T typename TA::template BestRow<TB>::Type
#define NVXI_BOOL_INTER_T typename TA::template WorstRow<TB>::Type

#define NVXI_ARITH_OP(OP, RECIP) NVXI_BASE_OP(OP,NVXI_ARITH_INTER_T,  TA, TB, typename TA::template BestRow<TB>, NVXI_ARITH_RECIP(RECIP), .val,  NVX_OP_CLONEMETA(ret, a, b))
#define NVXI_BOOL_OP(OP) NVXI_BASE_OP(OP,NVXI_BOOL_INTER_T, bool,bool,bool,,,)

//Specialized division implementation
namespace nvx {
#define RET_3 typename ::nvx::best_scalar<TA, TB>
template<typename TA, typename TB>
inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value && std::is_arithmetic<TB>::value && !std::is_base_of<NVX_IRoot, TB>::value && !std::is_base_of<NVX_T, TB>::value, typename choose_nvxi<TA,TB>::type>::type 
operator /(const TA& a, const TB &b) {
    typename std::remove_const<TA>::type conv; 
    conv.template from<TB>(b);
    //conv.fromMeta(a);
    return ((a / conv));
}
template<typename TA, typename TB>
inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TB>::value && std::is_arithmetic<TA>::value && !std::is_base_of<NVX_IRoot, TA>::value && !std::is_base_of<NVX_T, TA>::value, typename choose_nvxi<TA,TB>::type>::type 
operator /(const TA& a, const TB &b) {
    typename std::remove_const<TB>::type conv; 
    conv.template from<TA>(a);
    //conv.fromMeta(b);
    return ((conv / b));
}
template<typename TA, typename TB> inline constexpr typename std::enable_if<
    std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value, RET_3>::type 
operator /(const TA& a, const TB &b) {
    RET_3 ret;
    auto aT = static_cast<RET_3::Type>(a.raw() * NVX_OpMult<TA, RET_3>());
    auto bT = static_cast<RET_3::Type>(b.raw() * NVX_OpMult<TB, RET_3>());
    
    bool takeRecip = false;
    if(abs(bT) > abs(aT)) std::swap(aT, bT), takeRecip = true;
    
    if(bT != 0) {
        ret.val = aT * TA::template BestRow<TB>::Multiplier / bT;
        if(takeRecip && ret.val != 0) ret.val = (TA::template BestRow<TB>::Multiplier) * TA::template BestRow<TB>::Multiplier / ret.val;
        NVX_DBG(std::cout << "\tset div val to " << ret << " : " << ret.val << " from " << aT << "/"<<bT << " conv " << a.raw() << "/"<<b.raw()<<", takeRecip=" << takeRecip << " of " << ((TA::template BestRow<TB>::Multiplier)) << "\n");
    } else ret.val = 0;
    
    ret.val /= static_cast<typename TA::template BestRow<TB>::Type>(NVX_OpRecip<TB, typename TA::template BestRow<TB>>());
    //ret.fromMeta(a);
    return ret;
}

//Specialized mult
template<typename TA, typename TB>
inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TA>::value && std::is_arithmetic<TB>::value && !std::is_base_of<NVX_IRoot, TB>::value && !std::is_base_of<NVX_T, TB>::value, typename choose_nvxi<TA,TB>::type>::type 
operator *(const TA& a, const TB &b) {
    typename std::remove_const<TA>::type conv; 
    conv.template from<TB>(b);
    //conv.fromMeta(a);
    return ((a * conv));
}
template<typename TA, typename TB>
inline constexpr typename std::enable_if<std::is_base_of<NVX_IRoot, TB>::value && !std::is_base_of<NVX_IRoot, TA>::value && std::is_arithmetic<TA>::value && !std::is_base_of<NVX_T, TA>::value, typename choose_nvxi<TA,TB>::type>::type 
operator *(const TA& a, const TB &b) {
    typename std::remove_const<TB>::type conv; 
    conv.template from<TA>(a);
    //conv.fromMeta(b);
    return ((conv * b));
}
template<typename TA, typename TB> inline constexpr typename std::enable_if<
    std::is_base_of<NVX_IRoot, TA>::value && std::is_base_of<NVX_IRoot, TB>::value, RET_3>::type 
operator *(const TA& a, const TB &b) {
    RET_3 ret;
    auto aT = static_cast<RET_3::Type>(a.raw() * NVX_OpMult<TA, RET_3>());
    auto bT = static_cast<RET_3::Type>(b.raw() * NVX_OpMult<TB, RET_3>());
    
    /*std::cout << "aSrc=" << TA::Multiplier << ", bSrc=" << TB::Multiplier << "\n";
    std::cout << "aT = " << aT << ", bT = " << bT << "\n";
    
    std::cout << "aMult="<<NVX_OpMult<TA, RET_3>()<<", aRaw=" << a.raw() << "\n";
    std::cout << "bMult="<<NVX_OpMult<TB, RET_3>()<<", bRaw="<< b.raw() << "\n";
    std::cout << "RET_TMult="<<(::nvx::best_scalar<TA, TB>::Multiplier)<<"\n";*/
    
    constexpr RET_3::Type mRecip = ::std::max(((::nvx::best_scalar<TA, TB>::Multiplier)), static_cast<RET_3::Type>(1));
    ret.setRaw((aT * bT) / mRecip);
    //ret.fromMeta(a);
    return ret;
}
#undef RET_3
};

//Vector arith/ops
#define NVXV_RET_T typename ::nvx::best_vector<TA,TB>

#define NVXV_BASE_OP(OP, RET_T, RET_IDX, RET_ASSIGN, RET_DEFAULT, RET_PRESTATE) \
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<\
std::is_base_of<NVX_T, TA>::value && !std::is_base_of<NVX_T, TB>::value &&\
!std::is_base_of<NVX_IRoot, TB>::value, TA>::type \
operator OP(const TA& a, const TB &b) {\
    NVX_DBG(std::cout << "OPTRUNC_B " << a << " OP "<<b<<" \n");\
    return ((a OP TA(b)));\
}\
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<\
std::is_base_of<NVX_T, TB>::value && !std::is_base_of<NVX_T, TA>::value && \
!std::is_base_of<NVX_IRoot, TA>::value, TB>::type \
operator OP(const TA& a, const TB &b) {\
    NVX_DBG(std::cout << "OPTRUNC_A "<<a<<" OP " << b << "\n");\
    return (TB(a) OP (b));\
}\
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<\
std::is_base_of<NVX_T, TA>::value && !std::is_base_of<NVX_T, TB>::value && \
std::is_base_of<NVX_IRoot, TB>::value, RET_T>::type \
operator OP(const TA& a, const TB &b) {\
    NVX_DBG(std::cout << "OPCONV_B " << RET_T(a) << " OP " << RET_T(b) << "\n");\
    return ((RET_T(a) OP RET_T(b)));\
}\
template<typename TA, typename TB>\
inline constexpr typename std::enable_if<\
std::is_base_of<NVX_T, TB>::value && !std::is_base_of<NVX_T, TA>::value &&\
std::is_base_of<NVX_IRoot, TA>::value, RET_T>::type \
operator OP(const TA& a, const TB &b) {\
    NVX_DBG(std::cout << "OPCONV_A " << RET_T(a) << " OP " << RET_T(b) << "\n");\
    return (RET_T(a) OP RET_T(b));\
}\
template<typename TA, typename TB>\
inline _unroll constexpr typename std::enable_if<std::is_base_of<NVX_T, TA>::value && std::is_base_of<NVX_T, TB>::value, RET_T>::type \
operator OP(const TA& a, const TB &b) {\
    static_assert(TA::D == TB::D, "Operation on vectors of non-equal sizes requested");\
    NVX_DBG(std::cout << "OPVEC " << a << " OP " << b<< "\n");\
    RET_T ret RET_DEFAULT;\
    for(typename TA::Idx i = 0; i < TA::D; i++) ret RET_IDX RET_ASSIGN a[i] OP b[i];\
    RET_PRESTATE;\
    return ret;\
}

#define NVXV_ARITH_OP(OP) NVXV_BASE_OP(OP, NVXV_RET_T , [i], =,,)
#define NVXV_BOOL_OP_ALL(OP) NVXV_BASE_OP(OP, bool,,&=, = true,)
#define NVXV_BOOL_OP_ONE(OP) NVXV_BASE_OP(OP, bool,,|=, = false,)

#define NVX_BOOL_OP_FRIEND(OP, BASE) template<typename TA, typename TB>\
    inline constexpr friend typename std::enable_if<std::is_base_of<BASE, TA>::value && std::is_base_of<BASE, TB>::value, bool>::type \
    operator OP(const TA& a, const TB &b);

#define NVX_OPEQ_OP(OP) template<typename TARG_T> inline constexpr Self& operator NVX_PPCAT_NX(OP,=)(const TARG_T& o) {\
        NVX_DBG(std::cout << "assign-set OP got " << (*this) << " and " << o << " = " << ((*this) OP o) << ", convert o = " << Self(o) << "\n");\
        return ((*this) = (*this) OP o);\
    }
#endif /* NVXI_ARITH_H */

