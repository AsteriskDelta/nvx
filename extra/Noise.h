#ifndef NVX_NOISE_H
#define NVX_Noise_H

namespace nvx {
    namespace Noise {
        template<typename T>
        class Generator {
        public:
            inline Generator() {};
            inline virtual ~Generator() {};
            typedef T Value;

            Value min, max, cmod, div;
            uint64_t seed, offset;
            inline virtual void setSeed(uint64_t newSeed) {
                this->seed = newSeed;
                this->offset = newSeed;
            }
            inline virtual void setOffset(uint64_t off) {
                this->offset = off;
            }

            inline virtual void setRange(Value newMin, Value newMax, Value newDiv) {
                min = newMin; max = newMax;
                cmod = (max - min)*newDiv;
                div = newDiv;
            }
        };

        template<typename T>
        class IterativeGenerator : public Generator<T> {
        public:
            inline IterativeGenerator() {};
            inline virtual ~IterativeGenerator() {};
            typedef T Value;

            inline Value eval() {
                static constexpr uint64_t m = uint64_t(0x1) << 32, c = 12345, a = 1103515245;
                this->offset = (a * this->offset + c) % m;
                return Value(abs(Value(this->offset) % (this->cmod))) / this->div + this->min;
            }

        };

        template<typename T>
        class Perlin : public IterativeGenerator<typename T::Value> {
        public:
            typedef typename T::Value Value;
            uint16_t octaves;
            Value scale;

            inline Perlin() : octaves(8), scale(1.0) {};
            inline virtual ~Perlin() {};

            inline virtual void setScale(double scl) {
                scale = Value(scl);
            }

            inline virtual void setOctaves(uint16_t newOctaves) {
                octaves = newOctaves;
            }

            inline virtual Value operator()(const T pt) {
                Value ret = 0;
                Value ratioSum = 0;
                for(uint16_t octave = 0; octave < octaves; octave++) {
                    Value ratio = Value(1.0) - Value(octaves - octave) / (Value(octaves) + ratioSum);
                    ratioSum += ratio;
                    uint64_t baseSeed = this->seed;
                    //std::cout << "evaluate for octaves " << octave << "/"<<octaves<<", sz="<<decltype(pt)::Size<<"\n";
                    for(uint16_t i = 0; i < decltype(pt)::Size; i++) {
                        //std::cout << "\t\tevalseed " << i << "\n";
                        auto base = pt[i] * this->scale;
                        //std::cout << "\t\tbase=" << base << "\n";
                        auto mdiv = Value(octaves - octave);
                        //std::cout << "\t\tdiv="<<mdiv<<"\n";
                        auto res = base / mdiv;
                        //std::cout << "\t\t"<<uint64_t(double(res))<<"\n";
                        baseSeed += uint64_t(double(res));//uint64_t(res);
                        //baseSeed += uint64_t((pt[i] * this->scale) / Value(octaves - octave));
                        //std::cout << "\tbaseSeed = " << baseSeed << "\n";
                    }
                    //std::cout << "eval for seed " << baseSeed << "\n";
                    this->setOffset(baseSeed);
                    this->eval();
                    //std::cout << "encorporate with ratio " << ratio << "\n";
                    ret = ret * (ratio) + this->eval() * (Value(1.0) - ratio);
                }
                return ret;
            }
        protected:

        };
/*
        template<typename T>
        inline T Range(const T& min, const T& max) {

        }

        template<typename T>
        inline typename T::Value Perlin(const T& pt, const uint16_t octaves, uint64_t *seed) {
            T ret;
            for(uint16_t i = 0; i < octaves; i++) {
                ret += Range
            }
        }*/
    };
};

#endif
