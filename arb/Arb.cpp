#include "Arb.h"
#ifdef NOT_TODAY
namespace nvx {
    
    Arb::Arb() {
        raw.integer = __null;
        raw.type = Undefined;
    }
    
    /*Arb::Arb(const Self&& in) {
        raw.type = in.raw.type;
        switch(in.raw.type) {
            case Integer    : raw.integer = in.raw.integer.
            case Rational   :
            case Irrational :
            case Undefined  :
        }
    }*/
    
    Arb::Type Arb::type() {
        return raw.type;
    }
    
    void Arb::upConvert(Arb&& a, Arb& b) {
        if(a.type() != b.type()) {
            Type desiredType = std::max(a.type(), b.type());
            convertTo(a, desiredType);
            convertTo(b, desiredType);
        }
        
    }
    
    void Arb::convertTo(Arb& in, Arb::Type type) {
        if(in.type() == Arb::Type::Integer) {
            long num = in.raw.integer.fits_slong_p();
            if(type == Arb::Type::Rational) {
                in.raw.rational = mpq_class(num, 1);
            }
            if(type == Arb::Type::Irrational) {
                in.raw.irrational = mpf_class(num);
            }
        }
        if(in.type() == Arb::Type::Rational && type == Arb::Type::Irrational) {
            long num = in.raw.rational.get_num().fits_slong_p();
            long den = in.raw.rational.get_den().fits_slong_p();
            in.raw.irrational = mpf_class(static_cast<double>(num)/den);
        }
    }
    
    Arb Arb::operator+(Arb a, Arb b) {
        if(a.raw.type >= b.raw.type) {
            // create new Arb of type 'a' with a + "b converted to type 'a'" 
            // Arb ret = new Arb(a + b, a.raw.type)?
        } else {
            // create new Arb of type 'b' with "a converted to type 'b'" + b
        }
        
        // return new object
    }
        
            
};

#endif
