#define NVX_ARB_H
#ifndef NVX_ARB_H
#define NVX_ARB_H
#include "NVX.defines.h"
#include "NVX2.core.h"
#include <gmp.h>
#include <gmpxx.h>

namespace nvx {
    class Arb : public NVX_IRoot {
    public:
        typedef Arb Self;
        typedef __int128_t Int_t;
        
        Arb();
        ~Arb();
        
        //Creates integer
        template<typename INTEGER>
        inline Arb(const INTEGER in, const typename std::enable_if<std::is_integral<INTEGER>::value,INTEGER*>::type nll = nullptr) : raw() {
            ((void)nll);
            this->from<Int_t>(in);
        }
        
        //Creates irrational
        template<typename FLOAT>
        inline Arb(FLOAT in, typename std::enable_if<std::is_floating_point<FLOAT>::value,FLOAT*>::type nll = nullptr) : raw() {
            ((void)nll);
            this->from<long double>(in);
        }
        
        Arb(const Self&& in);
        
        //Constructs rational
        template<typename OTHER>
        inline Arb(OTHER in, typename std::enable_if<std::is_base_of<NVX_IRoot, OTHER>::value, OTHER*>::type nll = nullptr) : raw() {//Non-homogenous conversion copy
            ((void)nll);
            this->from<typename OTHER::Primative>(in.handle(), _constexpr_exp2(OTHER::MinorBits));
        }
        
        enum Type : uint8_t {
            Integer = 0,
            Rational = 1,
            Irrational = 2,
            Undefined = 3
        };
        
        const Arb& handle() const;
        Arb& handle();
        
        Type type() const; //Done
        
        Int_t numerator() const;
        Int_t denominator() const;
        
        bool superunital() const;
        bool isNil() const;
        
        operator double() const;
        explicit operator long double() const;
        explicit operator Int_t() const;
        explicit operator int64_t() const;
        
#ifdef NVX_STR_CASTS
        std::string typeStr() const;
        std::string str(bool isRaw = false) const;
#endif
        
        static Self Nil();
        
        Self operator-();
        Self& operator++();
        Self& operator++(int unu);
        Self& operator--();
        Self& operator--(int unu);
        
        
        Self& from(const Int_t&& num, const Int_t&& denom);//Creates rational
        Self& from(const Int_t&& integer);//Integer
        Self& from(const long double&& floating);//Irrational
        
        //The rest of the class from here is done
        
        template<typename T>
        inline constexpr T to() const {
            return static_cast<T>(*this);
        }
        //Creates irrational if floating-point, integer if integer, etc.
        template<typename T>
        inline constexpr Self& from(const T& val) {
            (*this) = val;
            return *this;
        }
        
        template<typename TARG_T>
        inline explicit constexpr operator TARG_T() const {
            return to<TARG_T>();
        }
        
        inline explicit operator bool() const {
            return !this->isNil();
        }
        
        NVX_OPEQ_OP(+);
        NVX_OPEQ_OP(-);
        NVX_OPEQ_OP(*);
        NVX_OPEQ_OP(/);
        NVX_OPEQ_OP(%);
        
        template<typename T>
        static inline Arb FromInferior(const T&& val) {
            return Arb(val);
        }
        
        inline const Type& type() const {
            return raw.type;
        }
        _mutate(type,(),());
    protected:
        struct Raw {
            union {
                mpz_class integer;
                mpq_class rational;
                mpf_class irrational;
            };
            
            Type type;
            
            Raw();
        } raw;
    };
    
    //Conversion forwarders, finished
    template<typename T> inline auto NI2Result(Arb &&a, const T&& b) {
        ((void)a); ((void)b);
        return Arb();
    }
    template<typename T> inline auto NI2Result(T &&a, Arb &&b) {
        ((void)a); ((void)b);
        return Arb();
    }
    template<typename T> inline auto NI2Intermediate(Arb &&a, const T&& b) {
        ((void)a); ((void)b);
        return Arb();
    }
    template<typename T> inline auto NI2Intermediate(T &&a, Arb &&b) {
        ((void)a); ((void)b);
        return Arb();
    }
    
    void upConvert(Arb&& a, const Type type);
    void convertTo(Arb& in, Type& type);
    
    //Implement these as one would expect
    Arb operator+(const Arb&& a, const Arb&& b);
    Arb operator-(const Arb&& a, const Arb&& b);
    Arb operator*(const Arb&& a, const Arb&& b);
    Arb operator/(const Arb&& a, const Arb&& b);
    Arb operator%(const Arb&& a, const Arb&& b);

    
    //Conversion templates- already done
    //Multiplication
    template<typename TA>
    inline Arb operator*(const TA&& a, const Arb&& b) {
        return Arb(a) * b;
    }
    template<typename TB>
    inline Arb operator*(const Arb&& a, const TB&& b) {
        return a * Arb(b);
    }
    //Division
    template<typename TA>
    inline Arb operator/(const TA&& a, const Arb&& b) {
        return Arb(a) / b;
    }
    template<typename TB>
    inline Arb operator/(const Arb&& a, const TB&& b) {
        return a / Arb(b);
    }
};
#endif

