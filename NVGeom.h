#ifndef NVX_GEOM_H
#define NVX_GEOM_H

#include "NVX.h"
#include "NParametric.h"
#include "NLine.h"
#include "NPolygonPt.h"
#include "Bounds.h"
#include "NPolygon.h"

#endif /* NVX_H */

