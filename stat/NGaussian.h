#ifndef NGAUSSIAN_H
#define NGAUSSIAN_H
#include "NVX.h"

//#define NVXS_DEBUG
#if defined(NVX_DEBUG) || defined(NVXS_DEBUG)
#define NVXSG_DBG(...) __VA_ARGS__
#else
#define NVXSG_DBG(...) 
#endif

#ifndef _alias
#define _alias_attr(FROM, TO, ATTR) \
template<typename ...Args>\
inline auto TO(Args&... args) ATTR { return FROM(args...); };
#define _alias(FROM, TO) _alias_attr(FROM, TO,)
#define _alias_tpl_attr(FROM, TO, ATTR, ...) template<typename ...Args>\
inline TO(Args&...) ATTR { return FROM<__VA_ARGS__>(args...); };
#define _alias_tpl(FROM,TO,...) _alias_tpl_attr(FROM,TO,,__VA_ARGS__)
#endif

namespace nvx {
    struct Gaussian_Root{};
    
    template<typename V>
    class Gaussian : public Gaussian_Root {
    public:
        typedef V Value;
        typedef Gaussian<V> Self;
        using value = Value;
        
        Gaussian() noexcept;
        Gaussian(const V& mean, const V& variance) noexcept;
        
        void set(const V& mean, const V& variance) noexcept;
        _alias_attr(set, update, noexcept);
        
        void setRange(const V& mean, const V& range) noexcept;
        
        inline void setMean(const V& nMean) noexcept {
            b = nMean;
        }
        
        V pdf(const V& x) const noexcept;
        _alias_attr(pdf, operator(), const noexcept);
        //_alias_attr(pdf, evaluate, const noexcept);
        
        V cdf(const V& x) const noexcept;
        
        //Returns the distance from the mean- ie, rel
        V invpdf(const V& p) const noexcept;
        V invstddev(const V& p) const noexcept;
        
        //Returns the area of overlap with another gaussian
        V intersect(const Self& o) const;
        //Returns the probability at which the two distributions have maximum overlap
        static V Vertex(const Self *a, const Self *b);
        inline V vertexWith(const Self *o) const {
            return Vertex(this, o);
        }
        
        inline const V& stddev() const noexcept {
            return c;
        }
        inline const V& mean() const noexcept {
            return b;
        }
        inline V variance() const noexcept {
            return var;
        }
        
        inline V relative(const V& x) const noexcept {
            return x - this->mean();
        }
        _alias_attr(relative, rel, const noexcept);
    protected:
        V a,b,c;
        V var;//Stored to avoid continually multiplying c^2
    };
};

#endif
