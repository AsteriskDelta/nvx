#include "NGaussian.h"
#include "NVX.h"
#define GAUSS_TPL template<typename V>
#define GAUSS Gaussian<V>
#include "NVX.func.h"

namespace nvx {
    GAUSS_TPL
    GAUSS::Gaussian() noexcept {
        
    }
    
    GAUSS_TPL
    GAUSS::Gaussian(const typename GAUSS::Value& mean, const typename GAUSS::Value& variance) noexcept {
        this->set(mean, variance);
    }
    
    GAUSS_TPL
    void GAUSS::set(const typename GAUSS::Value& mean, const typename GAUSS::Value& variance) noexcept {
        b = mean;
        c = sqrt(variance);
        a = Value(1) / ( c * Value(::sqrt(2 * M_PI)) );
        var = variance;
    }
    GAUSS_TPL
    void GAUSS::setRange(const typename GAUSS::Value& mean, const typename GAUSS::Value& range) noexcept {
        const V variance = V(1)/V(12) * sq(range);
        this->set(mean, variance);
    }
    
    GAUSS_TPL
    typename GAUSS::Value GAUSS::pdf(const typename GAUSS::Value& x) const noexcept {
        if(_unlikely(var == Value(0))) {
            if(x == b) return Value(1);
            else return Value(0);
        }
        return a * ::nvx::exp( - sq(this->rel(x)) / (2 * var) );
    }
    
    GAUSS_TPL
    typename GAUSS::Value GAUSS::cdf(const typename GAUSS::Value& x) const noexcept{
        if(_unlikely(var == Value(0))) {
            if(x == b) return Value(1);
            else return Value(0);
        }
        return (1 + ::nvx::erf( (this->rel(x)) / (c * Value(::sqrt(2.0))) ) ) / 2;
    }
        
    //Returns the distance from the mean- ie, rel
    GAUSS_TPL
    typename GAUSS::Value GAUSS::invstddev(const typename GAUSS::Value& p) const noexcept {
        return Value(::sqrt(2.0)) * inverf(p);
    }
    GAUSS_TPL
    typename GAUSS::Value GAUSS::invpdf(const typename GAUSS::Value& p) const noexcept {
        if(_unlikely(var == Value(0) && p == Value(1))) {
            return b;
        }
        return this->invstddev(p) * this->stddev();
    }
    
    
    //Incorrect CDF Eq, see my notebook. Corrected in C3AI child class
    GAUSS_TPL
    typename GAUSS::Value GAUSS::intersect(const GAUSS::Self& o) const {
        //std::cout << "intersect: " <<this->variance() << " =?= " << o.variance() << " -> " << (this->variance() == o.variance()) << "\n";
        if(_unlikely(var == Value(0) || o.var == Value(0))) {
            if(b == o.b) return Value(1);
            else return Value(0);
        }
        
        const Self *ag, *bg;
        if(this->mean() < o.mean()) {
            ag = this; bg = &o;
        } else {
            ag = &o; bg = this;
        }
        
        auto cp = Vertex(ag, bg);
        return max(Value(0), Value(1) - ag->cdf(cp) + bg->cdf(cp));
    }
    
    
    //a la https://stats.stackexchange.com/questions/103800/calculate-probability-area-under-the-overlapping-area-of-two-normal-distributi
    GAUSS_TPL
    typename GAUSS::Value GAUSS::Vertex(const GAUSS::Self *ag, const GAUSS::Self *bg) {
        //std::cout << "Vertex:"<< ag->variance() << " =?= " << bg->variance() << " -> " << (ag->variance() == bg->variance()) << "\n";
        if(_unlikely(ag->var == Value(0) || bg->var == Value(0))) {
            if(ag->b == bg->b) return ag->mean();
            else if(ag->b == Value(0)) return ag->mean();
            else return bg->mean();
        }
        
        if(ag->variance() == bg->variance()) {//Special case needed for equation below
            return (ag->mean() + bg->mean()) / V(2);
        }
        
        bool toggle = false;
        if(ag->mean() >= bg->mean()) {
            std::swap(ag,bg);
            toggle = true;
        }
        
        Value numerator = bg->mean() * ag->variance();
        const Value ll = log(ag->stddev() / bg->stddev());
        
        
        const Value sqr = ag->stddev() * sqrt(
                        sq(ag->mean() - bg->mean())
                        + Value(2) * (ag->variance() - bg->variance())
                        * ll
                  );
        numerator -= bg->stddev() * (ag->mean() * bg->stddev() + sqr);
        
        const Value denominator = ag->variance() - bg->variance();
        
        return numerator / denominator;
    }
    /*
    GAUSS_TPL
    typename GAUSS::Value GAUSS::Vertex(const GAUSS::Self *ag, const GAUSS::Self *bg) {
        const V A = V(-1) / ag->variance() + V(1)/bg->variance();
        const V B = V(2) * (-bg->mean()/bg->variance() + ag->mean()/ag->variance());
        const V C = sq(bg->mean())/bg->variance() - sq(ag->mean())/ag->variance() + log(ag->variance() / bg->variance());
        
        const V discriminant = sq(B) - V(4) * A * C;
        const V sqd = sqrt(discriminant);
        
        const V x1 = (-B + sqd) / (V(2)*A), x2 = (-B + sqd) / (V(2)*A);
        
        std::cout << "GAUSS::Vertex x1=" << x1 << ", x2=" << x2 << "\n";
        return x1;
        
    }*/
}
