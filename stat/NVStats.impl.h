#include "NVStats.h"

#include "NGaussian.impl.h"

#define NSTATS_IMPL(...) namespace nvx {\
    template class Gaussian<__VA_ARGS__>;\
};
