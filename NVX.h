#ifndef NVX_H
#define NVX_H

#ifndef _alias
#define _alias_attr(FROM, TO, ATTR) \
template<typename ...Args>\
inline auto TO(Args&... args) ATTR { return FROM(args...); };
#define _alias(FROM, TO) _alias_attr(FROM, TO,)
#define _alias_tpl_attr(FROM, TO, ATTR, ...) template<typename ...Args>\
inline TO(Args&...) ATTR { return FROM<__VA_ARGS__>(args...); };
#define _alias_tpl(FROM,TO,...) _alias_tpl_attr(FROM,TO,,__VA_ARGS__)
#endif

#include "NVX.defines.h"
#include "NVX2.core.h"

//#include "NI.h"
#include "N2.h"
//#include "N2Meta.h"
#include "N3.h"
#include "N4.h"

#include "NVX.func.h"
namespace nvx {
    
};

#endif /* NVX_H */

